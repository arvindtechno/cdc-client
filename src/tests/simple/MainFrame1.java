package tests.simple;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;

import javax.swing.JFrame;

import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.coretechies.Client.FileConstraint;
import org.coretechies.Client.SingleTon;
import org.coretechies.Client.Unity3dJoin;

public class MainFrame1 extends JDialog {
    private static final long serialVersionUID = -5570653778104813836L;
    private CefApp     cefApp_;
    private CefClient  client_;
    private CefBrowser browser_;
    private Component  browerUI_;
    private Thread t;
    public MainFrame1(String startURL1, File path, boolean useOSR, boolean isTransparent, JFrame owner){
        super(owner);
        System.out.println("hello");
        final String startURL = startURL1 + path.getAbsolutePath();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screen = toolkit.getScreenSize();
        setResizable(false);
        
//        setUndecorated(true);
        try{
            System.out.println("OPEN JDialog");
            String arch = System.getProperty("os.arch", "x86");
            if(arch.endsWith("32")){
                System.out.println("arc : "+arch);
                System.setProperty("java.library.path", System.getProperty("user.dir")+"win32/Debug");
            }
            System.out.println("Library Path : "+System.getProperty("java.library.path"));
            cefApp_ = CefApp.getInstance();

            client_ = cefApp_.createClient();

            browser_ = client_.createBrowser(startURL, useOSR, isTransparent);
            browerUI_ = browser_.getUIComponent();
            System.out.println("Parent : "+path.getParent());
            System.out.println("Parent : "+path.getParentFile());
//            browser_.loadURL(address_.getText());
            
//            Thread log = new Thread(new Runnable() {
//                
//                @Override
//                public void run() {
//                    try {
//                        File ff = new File("C:\\Users\\Tiwari\\AppData\\Local\\Temp\\UnityWebPlayer\\log");
//                        while(!ff.exists()){
//                            Thread.sleep(500);
//                        }
//                        System.out.println("rrrrrrrrrrrrrrrrrrrrr");
////                        Runtime.getRuntime().exec("attrib +H "+path.getParent()+"\\UnityWebPlayer\\log");
////                        Runtime.getRuntime().exec("attrib +H "+path.getParent()+"\\UnityWebPlayer");//UnityWebPlayer\log
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(MainFrame1.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            });
//            log.setDaemon(true);
//            log.start();
            t = new Thread(new Runnable() {

                @Override
                public void run() {
                    try{
                        SingleTon singleTon = SingleTon.getSingleTon();
                        while(singleTon.getUnityHtml()==null){
                            System.out.println("SLEEP......................");
                            Thread.sleep(500);
                        }
                        System.out.println("hello  "+singleTon.getUnityHtml().getAbsolutePath());
    //                    browser_.stopLoad();
    //                    while(browser_.getURL().equalsIgnoreCase(startURL))
                        Unity3dJoin join = Unity3dJoin.getUnity3dJoin();
                        singleTon.releaseFileLock();
                        FileConstraint.reWriteMoreToFile((join.getFileNumber())[0].getFile(), singleTon.getTempUnity());
                        do{
//                            FileConstraint.reWriteMoreToFile(null);
                            browser_.loadURL("file:///"+singleTon.getUnityHtml().getAbsolutePath());
                            Thread.sleep(1000);
                        }while(!browser_.isLoading());
                        Thread.sleep(500);
                        System.out.println("hhhhhhhhhhhhhhhhhhhhh");
                        while(browser_.isLoading()){
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(MainFrame1.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            System.out.println("Loading.....");
                        }System.out.println("loaded");
                        Thread.sleep(1000);
//                        Unity3dJoin join = Unity3dJoin.getUnity3dJoin();
                        FileConstraint.writeMoreToFile((join.getFileNumber())[0].getFile());
//                        ArrayList<Object> arrayList = FileConstraint.lockFile((join.getFileNumber())[0].getFile());
                        singleTon = SingleTon.getSingleTon();
                        singleTon.acquireFileLock((join.getFileNumber())[0].getFile());
//                        singleTon.setRandomAccessFileUnity((RandomAccessFile) arrayList.get(0));
//                        singleTon.setFileChannelUnity((FileChannel) arrayList.get(1));
//                        singleTon.setFileLockUnity((FileLock) arrayList.get(2));
                        
                        singleTon.getUnityHtml().delete();
                        path.delete();
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            });
            getContentPane().add(browerUI_, BorderLayout.CENTER);
            t.setDaemon(true);
            t.start();
            getContentPane().add(browerUI_, BorderLayout.CENTER);
        }catch(Exception e){
//            FileConstraint.lockFile(path);
            e.printStackTrace();
        }
        pack();
        setSize(screen.width,screen.height);
        setAlwaysOnTop(true);
        System.out.println("hello44444444444444444");
        setVisible(true);
        System.out.println("hello55555555555555555555555");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(t!=null){
                    System.out.println("t!=null");
                    t.stop();
                }
                dispose();
                System.out.println("Close");
//                path.delete();
//                SingleTon singleTon = SingleTon.getSingleTon();
//                singleTon.getUnityHtml().delete();
//                cefApp_.dispose();
//                System.exit(0);
//                Unity3dJoin join = Unity3dJoin.getUnity3dJoin();
//                FileNumber[] fileNumber = join.getFileNumber();
//                fileNumber[0].getFile().delete();
//                SingleTon singleTon = SingleTon.getSingleTon();
//                singleTon.getUnityHtml().delete();
//                path.delete();
//                setVisible(false);
//                path.delete();
            }
        });
        
        System.out.println("Close11");
//        path.delete();
//        SingleTon singleTon = SingleTon.getSingleTon();
//        singleTon.getUnityHtml().delete();
    }

  public static void main(String[] args) {
      
//	String path = "D:\\Unity 3D\\HTML Format\\HTML Format";
//        String file = "S.html";
//        
//	File f = new File(path,file);
//	System.out.println(f.getAbsolutePath());
//    new MainFrame1("file:///"+f.getAbsolutePath(), OS.isLinux(), false);
  }
}
