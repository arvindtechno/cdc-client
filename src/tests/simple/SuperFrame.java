/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests.simple;

import java.awt.BorderLayout;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.OS;
import org.cef.browser.CefBrowser;

/**
 *
 * @author Tiwari
 */
public class SuperFrame {

//    private final CefApp     cefApp_;
    public SuperFrame(String title) {
        String path = "D:\\Unity 3D\\HTML Format\\HTML Format";
        String file = "S.html";

        File f = new File(path,file);
        System.out.println(f.getAbsolutePath());
//        new MainFrame1("file:///"+f.getAbsolutePath(), OS.isLinux(), false);
        
    }
    
    public static void main(String[] args) {
      new SuperFrame("Unity 3D");
    }
    
    // Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

    public class MainFrame extends JPanel {
      private static final long serialVersionUID = -5570653778104813836L;
      private final JTextField address_;
      private final CefApp     cefApp_;
      private final CefClient  client_;
      private final CefBrowser browser_;
      private final Component  browerUI_;

      public MainFrame(String startURL, boolean useOSR, boolean isTransparent) {
        cefApp_ = CefApp.getInstance();
        client_ = cefApp_.createClient();
        browser_ = client_.createBrowser(startURL, useOSR, isTransparent);
        browerUI_ = browser_.getUIComponent();
        
        address_ = new JTextField(startURL, 100);
        address_.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            browser_.loadURL(address_.getText());
          }
        });
        add(address_, BorderLayout.NORTH);
        add(browerUI_, BorderLayout.CENTER);
      }
      
}
}
