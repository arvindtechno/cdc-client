package tests.simple;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainFrame11 extends JDialog {
  private static final long serialVersionUID = -5570653778104813836L;
  BufferedImage img;

  public MainFrame11(BufferedImage bufferedImage, JFrame owner) {
        super(owner);
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screen = toolkit.getScreenSize();
        
        img = bufferedImage;
        if(bufferedImage!=null){
            final SeeThroughComponent st = new SeeThroughComponent(img, screen.width, screen.height);
            add("Center", st);
            
//            JSlider opacitySlider = new JSlider(0, 100);
//            opacitySlider.addChangeListener(new ChangeListener() {
//                public void stateChanged(ChangeEvent e) {
//                    JSlider slider = (JSlider)e.getSource();
//                    st.setOpacity(slider.getValue()/100f);
//                    st.repaint();
//                };
//            });
//            Dimension size = st.getPreferredSize();
//            Dimension sliderSize = opacitySlider.getPreferredSize();
//            resize(size.width, size.height+sliderSize.height);
//            add("South", opacitySlider);
            
//            int w = img.getWidth(null);
//            int h = img.getHeight(null);
//            BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//            Graphics g = bi.getGraphics();
//            g.drawImage(img, 0, 0, null);
//            setOpacity(0.5f);
//            
//            JPanel jPanel = new JPanel();
//            ImageIcon icon = new ImageIcon(bufferedImage);
//            JLabel jLabel = new JLabel(icon);
//            jPanel.add(jLabel);
//            jPanel.repaint(); 
//            Dimension screen = getContentPane().getSize();
//            jPanel.setBounds(0,0,screen.width, screen.height);
//            jPanel.setBackground(Color.BLACK);
////            setContentPane(jPanel);
//            add(jPanel);
//            add(new JPanelWithBackground("sample.jpeg"));
        }else
            System.out.println("Error");
        pack();
        
        setSize(screen.width,screen.height);
//        setSize(1100,700);
        setAlwaysOnTop(true);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
              dispose();
            }
          }
        ); 
  }
//  
//  protected void paintComponent(Graphics g) {
////      super.paintComponent(g);
//      if (img != null) {
//          System.out.println("PaintComponent");
//            Dimension screen = getContentPane().getSize();
//         g.drawImage(img, 0, 0, screen.width, screen.height, this);
//      }
//   }
}

class SeeThroughComponent extends Component {
 
    private BufferedImage bi;
    float[] scales = { 1f, 1f, 1f, 0.5f };
    float[] offsets = new float[4];
    RescaleOp rop;
    int wid;
    int hei;
     
    public SeeThroughComponent(BufferedImage img, int width, int height) {
        wid = width;
        hei = height;
        int w = img.getWidth(null);
        int h = img.getHeight(null);
        bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics g = bi.getGraphics();
        g.drawImage(img, 0, 0, width, height, null);
//        setOpacity(0.5f);
    }
 
    public Dimension getPreferredSize() {
        return new Dimension(bi.getWidth(null), bi.getHeight(null));
    }
 
    public void setOpacity(float opacity) {
        scales[3] = opacity;
        rop = new RescaleOp(scales, offsets, null);
    }
 
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setColor(Color.white);
        g2d.fillRect(0,0, wid, hei);
        g2d.setColor(Color.black);
        g2d.setFont(new Font("Dialog", Font.BOLD, 24));
//        g2d.drawString("Java 2D is great!", 10, 80);
        g2d.drawImage(bi, rop, 0, 0);
    }
}