package tests.simple;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.coretechies.Client.FileConstraint;
import org.coretechies.Client.FileNumber;
import org.coretechies.Client.SWFJoin;
import org.coretechies.Client.SingleTon;

public class MainFrame111 extends JDialog {
    private static final long serialVersionUID = -5570653778104813836L;
    private JTextField address_ = null;
    private CefApp     cefApp_ = null;
    private CefClient  client_ = null;
    private CefBrowser browser_ = null;
    private Component  browerUI_ = null;
    RandomAccessFile raf;
    FileChannel channel;
    FileLock fileLock;
    private Thread t;
    public MainFrame111(String startURL, File file, boolean useOSR, boolean isTransparent, JFrame owner){
        super(owner);
        startURL = startURL + file.getAbsolutePath();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screen = toolkit.getScreenSize();
        setResizable(false);
//        setUndecorated(true);
        try{
            System.out.println("OPEN JDialog");
            String arch = System.getProperty("os.arch", "x86");
            if(arch.endsWith("32")){
                System.out.println("arc : "+arch);
                System.setProperty("java.library.path", System.getProperty("user.dir")+"win32/Debug");
            }
            cefApp_ = CefApp.getInstance();

            client_ = cefApp_.createClient();

            browser_ = client_.createBrowser(startURL, useOSR, isTransparent);
            browerUI_ = browser_.getUIComponent();
            address_ = new JTextField(startURL, 100);
            address_.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    browser_.loadURL(address_.getText());
                }
            });
            t = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
//                        browser_.loadURL(address_.getText());
                        SingleTon singleTon = SingleTon.getSingleTon();
                        while(singleTon.getSwfHtml()==null){
                            System.out.println("SLEEP......................");
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(MainFrame111.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        System.out.println("hellooo"+singleTon.getSwfHtml().getAbsolutePath());
                        singleTon = SingleTon.getSingleTon();
                        singleTon.releaseSWFFileLock();
                        SWFJoin sWFJoin = SWFJoin.getSWFJoin();
                        FileConstraint.reWriteMoreToFile((sWFJoin.getFileNumber())[0].getFile(), singleTon.getTempSWF());
//                    browser_.stopLoad();
//                        Thread.sleep(1000);
                        String url = browser_.getURL();
                        System.out.println("url.equals(browser_.getURL()) : "+url.equals(browser_.getURL()));
                        System.out.println(url+".equals("+browser_.getURL()+") : "+url.equals(browser_.getURL()));
                        
                        do{
                            System.out.println("url : "+browser_.getURL());
                            browser_.loadURL("file:///"+singleTon.getSwfHtml().getAbsolutePath());
                            System.out.println("url 1: "+browser_.getURL());
                            System.out.println("swf html file : "+singleTon.getSwfHtml());
                            System.out.println("swf html file : "+"file:///"+singleTon.getSwfHtml().getAbsolutePath().replace("\\", "/"));
                            Thread.sleep(1000);
//                            System.out.println("url.equals(\"file:///\"+singleTon.getSwfHtml().getAbsolutePath().replace(\"\\\\\", \"/\")");
//                            System.out.print(browser_.getURL()+".equals(file:///"+singleTon.getSwfHtml().getAbsolutePath().replace("\\", "/"));
//                            System.out.print(browser_.getURL().equals("file:///"+singleTon.getSwfHtml().getAbsolutePath().replace("\\", "/")));
//                            url = browser_.getURL();
                        }while(!browser_.getURL().equals("file:///"+singleTon.getSwfHtml().getAbsolutePath().replace("\\", "/")));
                        
                        System.out.println("url : "+browser_.getURL());
                        System.out.println("hhhhhhhhhhhhhhhhhhhhh");
//                        Thread.sleep(1000);
//                            System.out.println("browser_hasDocument() : "+browser_.hasDocument());
//                        while(!browser_.hasDocument()){
//                            System.out.println("browser_hasDocument() : "+browser_.hasDocument());
//                            browser_.loadURL("file:///"+singleTon.getSwfHtml().getAbsolutePath());
//                            Thread.sleep(1000);
//                        }
                        while(browser_.isLoading()){
                            Thread.sleep(500);
                            System.out.println("sssssssssss");
                        }System.out.println("loaded");
                        Thread.sleep(1000);
//                        SWFJoin sWFJoin = SWFJoin.getSWFJoin();
                        singleTon = SingleTon.getSingleTon();
                        FileConstraint.writeMoreToFile((sWFJoin.getFileNumber())[0].getFile());
                        singleTon.acquireSWFFileLock((sWFJoin.getFileNumber())[0].getFile());
//                        SWFJoin sWFJoin = SWFJoin.getSWFJoin();
//                        FileNumber[] fileNumber = sWFJoin.getFileNumber();
//                        fileNumber[0].getFile().delete();
                        singleTon.getSwfHtml().delete();
                        file.delete();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame111.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            getContentPane().add(browerUI_, BorderLayout.CENTER);
            t.setDaemon(true);
            t.start();
        }catch(Exception e){
            e.printStackTrace();
        }
        
        pack();
        setSize(screen.width,screen.height);
        setAlwaysOnTop(true);
        setVisible(true);
        System.out.println("hellooooooooooooo");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
//                cefApp_.dispose();
//                System.exit(0);
                if(t!=null){
                    System.out.println("t!=null");
                    t.stop();
                }
                System.out.println("closeeeeeeee-----------------");
//                setVisible(false);
//                hide();
            }
        });
//        SingleTon singleTon = SingleTon.getSingleTon();
//        singleTon.getSwfHtml().delete();
//        file.delete();
    }
}
