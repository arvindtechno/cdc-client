/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class AddLeaveResponse extends MessageResponse{
    
    private boolean addResponse;
    private int id;
    private boolean Interrupt;
    private String subject;
    private String lesson;
    private String topic;
    private String chapter;

    public boolean isAddResponse() {
        return addResponse;
    }

    public void setAddResponse(boolean addResponse) {
        this.addResponse = addResponse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isInterrupt() {
        return Interrupt;
    }

    public void setInterrupt(boolean Interrupt) {
        this.Interrupt = Interrupt;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }
    
}
