/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class WeeklyScheduleResponse extends MessageResponse{
    
    private ArrayList<LessonPlanning> lessonPlannings;

    public ArrayList<LessonPlanning> getLessonPlannings() {
        return lessonPlannings;
    }

    public void setLessonPlannings(ArrayList<LessonPlanning> lessonPlannings) {
        this.lessonPlannings = lessonPlannings;
    }

}
