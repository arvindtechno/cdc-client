/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class UpdatePasswordResponse extends MessageResponse{
    
    private int UpdatePasswordResponse;

    public int getUpdatePasswordResponse() {
        return UpdatePasswordResponse;
    }

    public void setUpdatePasswordResponse(int UpdatePasswordResponse) {
        this.UpdatePasswordResponse = UpdatePasswordResponse;
    }

}
