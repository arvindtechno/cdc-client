/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class SearchResponseData implements Serializable{
    private String subject; 
    private String chapter; 
    private String lesson; 
    private String topic; 
    private String curriculum; 
    private String Grade;
    private ArrayList<String> curriculumList;
    private ArrayList<String> gradeList;
    private ArrayList<String> subjectList;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String Grade) {
        this.Grade = Grade;
    }

    public ArrayList<String> getCurriculumList() {
        return curriculumList;
    }

    public void setCurriculumList(ArrayList<String> curriculumList) {
        this.curriculumList = curriculumList;
    }

    public ArrayList<String> getGradeList() {
        return gradeList;
    }

    public void setGradeList(ArrayList<String> gradeList) {
        this.gradeList = gradeList;
    }

    public ArrayList<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<String> subjectList) {
        this.subjectList = subjectList;
    }
}
