/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class MyLeavesResponse extends MessageResponse {
    
    private ArrayList<MyActivity> myLeaves;

    public ArrayList<MyActivity> getMyLeaves() {
        return myLeaves;
    }

    public void setMyLeaves(ArrayList<MyActivity> myLeaves) {
        this.myLeaves = myLeaves;
    }

}
