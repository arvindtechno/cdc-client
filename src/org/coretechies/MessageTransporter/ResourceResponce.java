/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class ResourceResponce extends MessageResponse{

    private ArrayList<String> curriculum;
    private ArrayList<String> grade;    

    public ArrayList<String> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(ArrayList<String> curriculum) {
        this.curriculum = curriculum;
    }

    public ArrayList<String> getGrade() {
        return grade;
    }

    public void setGrade(ArrayList<String> grade) {
        this.grade = grade;
    }

}