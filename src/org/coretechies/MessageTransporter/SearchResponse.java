/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class SearchResponse extends MessageResponse{
    private ArrayList<SearchResponseData> searchResponseDatas;
    private ArrayList<String> subject;
    private ArrayList<String> curriculum;
    private ArrayList<String> grade;

    public ArrayList<SearchResponseData> getSearchResponseDatas() {
        return searchResponseDatas;
    }

    public void setSearchResponseDatas(ArrayList<SearchResponseData> searchResponseDatas) {
        this.searchResponseDatas = searchResponseDatas;
    }

    public ArrayList<String> getSubject() {
        return subject;
    }

    public void setSubject(ArrayList<String> subject) {
        this.subject = subject;
    }

    public ArrayList<String> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(ArrayList<String> curriculum) {
        this.curriculum = curriculum;
    }

    public ArrayList<String> getGrade() {
        return grade;
    }

    public void setGrade(ArrayList<String> grade) {
        this.grade = grade;
    }
}
