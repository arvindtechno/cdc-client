/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class UpdateUserInfoResponse extends MessageResponse{
    
    private UpdateUserInfo updateUserInfo;
    private ArrayList<String> countryList;
    private ArrayList<String> stateList;
    private ArrayList<String> categoryList;
    private ArrayList<String> districtList;
    private ArrayList<String> schoolList;

    public UpdateUserInfo getUpdateUserInfo() {
        return updateUserInfo;
    }

    public void setUpdateUserInfo(UpdateUserInfo updateUserInfo) {
        this.updateUserInfo = updateUserInfo;
    }

    public ArrayList<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<String> countryList) {
        this.countryList = countryList;
    }

    public ArrayList<String> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<String> stateList) {
        this.stateList = stateList;
    }

    public ArrayList<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<String> categoryList) {
        this.categoryList = categoryList;
    }

    public ArrayList<String> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(ArrayList<String> districtList) {
        this.districtList = districtList;
    }

    public ArrayList<String> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(ArrayList<String> schoolList) {
        this.schoolList = schoolList;
    }
}
