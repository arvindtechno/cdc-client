/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class LessonPlanningResponse extends MessageResponse{
    
    ArrayList<LessonPlanning> lessonPlanningList;
    
    boolean response;

    public ArrayList<LessonPlanning> getLessonPlanningList() {
        return lessonPlanningList;
    }

    public void setLessonPlanningList(ArrayList<LessonPlanning> lessonPlanningList) {
        this.lessonPlanningList = lessonPlanningList;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }
    
}
