/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class LoginResponse extends MessageResponse{
    
    private int Response;
    private boolean checkLessonPlan;
    private String firstName;
    private String lastName;

    public int getResponse() {
        return Response;
    }

    public void setResponse(int Response) {
        this.Response = Response;
    }

    public boolean isCheckLessonPlan() {
        return checkLessonPlan;
    }

    public void setCheckLessonPlan(boolean checkLessonPlan) {
        this.checkLessonPlan = checkLessonPlan;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
}