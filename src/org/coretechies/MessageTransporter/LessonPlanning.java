/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Tiwari
 */
public class LessonPlanning implements Serializable{
    
    private int lesson_plan_id;
    private String Subject;
    private String lesson;
    private String topic;
    private Date classDate;

    public int getLesson_plan_id() {
        return lesson_plan_id;
    }

    public void setLesson_plan_id(int lesson_plan_id) {
        this.lesson_plan_id = lesson_plan_id;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String Subject) {
        this.Subject = Subject;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }
}
