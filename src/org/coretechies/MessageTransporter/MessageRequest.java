/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.io.Serializable;

/**
 *
 * @author Tiwari
 */
public abstract class MessageRequest implements Serializable{
    
    private String messageType;
    private String Hash;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getHash() {
        return Hash;
    }

    public void setHash(String Hash) {
        this.Hash = Hash;
    }
    
}
