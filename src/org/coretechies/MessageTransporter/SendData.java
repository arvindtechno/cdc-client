/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.io.Serializable;

/**
 *
 * @author Tiwari
 */
public class SendData implements Serializable{
    private byte[] bs = new byte[1024];
    private int sequence;

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }
    
    public byte[] getBs() {
        return bs;
    }

    public void setBs(byte[] bs) {
        this.bs = bs;
    }
    
}
