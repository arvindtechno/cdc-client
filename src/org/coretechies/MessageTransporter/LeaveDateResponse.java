/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tiwari
 */
public class LeaveDateResponse extends MessageResponse{
    
    private ArrayList<Date> leaveDateList;
    
    private ArrayList<Date> classDateList;
    
    public ArrayList<Date> getLeaveDateList() {
        return leaveDateList;
    }

    public void setLeaveDateList(ArrayList<Date> leaveDateList) {
        this.leaveDateList = leaveDateList;
    }

    public ArrayList<Date> getClassDateList() {
        return classDateList;
    }

    public void setClassDateList(ArrayList<Date> classDateList) {
        this.classDateList = classDateList;
    }
}
