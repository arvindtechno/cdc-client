/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class SchoolDistrictResponse extends MessageResponse{
    
    private ArrayList<String> schoolDistrictList;

    public ArrayList<String> getSchoolDistrictList() {
        return schoolDistrictList;
    }

    public void setSchoolDistrictList(ArrayList<String> schoolDistrictList) {
        this.schoolDistrictList = schoolDistrictList;
    }

}
