/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class CreateUserResponse extends MessageResponse {
    
    private ArrayList<String> countryList;    
    private ArrayList<String> stateList;    
    private ArrayList<String> schoolCategoryList;

    public ArrayList<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<String> countryList) {
        this.countryList = countryList;
    }

    public ArrayList<String> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<String> stateList) {
        this.stateList = stateList;
    }

    public ArrayList<String> getSchoolCategoryList() {
        return schoolCategoryList;
    }

    public void setSchoolCategoryList(ArrayList<String> schoolCategoryList) {
        this.schoolCategoryList = schoolCategoryList;
    }

    
}
