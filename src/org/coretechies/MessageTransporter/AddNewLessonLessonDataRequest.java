/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonLessonDataRequest extends AddNewLessonChapterDataRequest{
    
    private String Chapter;

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String Chapter) {
        this.Chapter = Chapter;
    }
    
}
