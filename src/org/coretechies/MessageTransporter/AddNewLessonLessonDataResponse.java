/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonLessonDataResponse extends MessageResponse{
    
    private ArrayList<String> lessonList;

    public ArrayList<String> getLessonList() {
        return lessonList;
    }

    public void setLessonList(ArrayList<String> lessonList) {
        this.lessonList = lessonList;
    }
}
