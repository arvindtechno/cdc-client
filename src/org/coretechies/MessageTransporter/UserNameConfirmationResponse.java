/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class UserNameConfirmationResponse extends MessageResponse{
    
    private boolean userNameVarify;

    public boolean isUserNameVarify() {
        return userNameVarify;
    }

    public void setUserNameVarify(boolean userNameVarify) {
        this.userNameVarify = userNameVarify;
    }

}
