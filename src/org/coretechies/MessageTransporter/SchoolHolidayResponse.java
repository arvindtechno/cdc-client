/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class SchoolHolidayResponse extends MessageResponse{

    private ArrayList<MyActivityHoliday> schoolHolidays;

    public ArrayList<MyActivityHoliday> getSchoolHolidays() {
        return schoolHolidays;
    }

    public void setSchoolHolidays(ArrayList<MyActivityHoliday> schoolHolidays) {
        this.schoolHolidays = schoolHolidays;
    }

}
