/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 *
 * @author Tiwari
 */
public class ClientSocket {
     
    Socket socket = null;
    Socket mp4socket = null;
    String str=null;
    private String IP=null;
    Queue<MessageRequest> requestQueue;
    Queue<MessageRequest> mp4requestQueue;
    static boolean bool;
    String Hash;
    Stage nextStage;
    int x;
    ImageView view;
    BufferedImage image;
    static ClientSocket clientSocket;
    SingleTon singleTon;
    
    String web;
    boolean isPlaying = false;
    
    static{
        clientSocket=null;    
    }

    public boolean isIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    private ClientSocket() {
       
    }
    public static ClientSocket getClientSocket(){
        if(clientSocket == null){
            clientSocket=new ClientSocket();
        }
        return clientSocket;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }
    
    public void createss(MessageRequest mr, String ip) throws InterruptedException {

        singleTon = SingleTon.getSingleTon();
        this.image = null;
        this.web = "";
        try {
            this.x=0;
            IP = ip;
            InetAddress add = InetAddress.getByName(ip);
            System.out.println("Yahooo"+add);
            socket = new Socket(add,5051);
            System.out.println("Client Connected to server : " + socket);
            System.out.println("Start Typing...");
            requestQueue = new LinkedList<>();
            mp4requestQueue = new LinkedList<>();
            requestQueue.add(mr);
            ClientWrite write = new ClientWrite(this);
            ClientRead read = new ClientRead(this);
            
            Thread tRead = new Thread(read);
            tRead.setDaemon(true);
            tRead.start();
            
            Thread tWrite = new Thread(write);
            tWrite.setDaemon(true);
            tWrite.start();
            
            while(!singleTon.isResponse()){
                Thread.sleep(500);
            }Thread.sleep(500);
        }
        catch(SocketException ioe){
            this.x=3;
            ioe.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static boolean isBool() {
        return bool;
    }

    public static void setBool(boolean bool) {
        ClientSocket.bool = bool;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public SingleTon getSingleTon() {
        return singleTon;
    }

    public void setSingleTon(SingleTon singleTon) {
        this.singleTon = singleTon;
    }

    public String getHash() {
        return Hash;
    }

    public void setHash(String Hash) {
        this.Hash = Hash;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Queue<MessageRequest> getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(Queue<MessageRequest> requestQueue) {
        this.requestQueue = requestQueue;
    }

    public Stage getNextStage() {
        return nextStage;
    }

    public void setNextStage(Stage nextStage) {
        this.nextStage = nextStage;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
    
    public ImageView getView() {
        return view;
    }

    public void setView(ImageView view) {
        this.view = view;
    }

    public Socket getMp4socket() {
        return mp4socket;
    }

    public void setMp4socket(Socket mp4socket) {
        this.mp4socket = mp4socket;
    }

    public Queue<MessageRequest> getMp4requestQueue() {
        return mp4requestQueue;
    }

    public void setMp4requestQueue(Queue<MessageRequest> mp4requestQueue) {
        this.mp4requestQueue = mp4requestQueue;
    }
    
}