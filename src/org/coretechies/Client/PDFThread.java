/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.EventQueue;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.JFrame;
import org.coretechies.MessageTransporter.Data;
import org.coretechies.forms.PDFViewer;
import org.coretechies.forms.PDFViewerWithOutManuBar;

/**
 *
 * @author Tiwari
 */
public class PDFThread extends JFrame implements Runnable{

    private BlockingQueue<Data> queueData;
    static PDFThread pDFThread;
    long size;
    byte[] newBs;
    boolean flage = true;
    
    static{
        pDFThread=null;
    }

    private PDFThread() {
       queueData = new LinkedBlockingQueue<Data>() ;
    }
    
    public static PDFThread getObject(){
        if(pDFThread == null){
            pDFThread=new PDFThread();
        }
        return pDFThread;
    }

    public BlockingQueue<Data> getQueueData() {
        return queueData;
    }

    public void setQueueData(BlockingQueue<Data> queueData) {
        this.queueData = queueData;
    }

    @Override
    public void run() {
        int store = 0;
        while(true){
            if(!queueData.isEmpty()){
                System.out.println("Size : "+getQueueData().peek().getSequenceNumber());
                Data data = getQueueData().poll();
                size = data.getSequenceNumber()+10240;
                byte[] bs = data.getBs();
                newBs =  new byte[(int)size];
                if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END")))
                    flage = false;
                break;
            }
        }
        while(true){
            if(!flage)
                break;
            while(true){
                if(!queueData.isEmpty())
                    break;
            }
            
            byte[] bs = getQueueData().poll().getBs();
            
            for(byte b : bs)
                newBs[store++] = b;
            
            if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))) break;
        }
                
        SingleTon singleTon = SingleTon.getSingleTon();
//        System.out.println("New Bs "+new String(newBs,0,newBs.length));
        singleTon.setPdfByte(newBs);
        singleTon.setResponse(true);

//        final ByteBuffer buf = ByteBuffer.allocate(newBs.length);
//        buf.put(newBs);
//        EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                PDFViewerWithOutManuBar dFViewer = new PDFViewerWithOutManuBar(true);
//                dFViewer.openPDFByteBuffer(buf);
//            }
//        });
        
//        System.out.println("image prints");
    }
    
}
