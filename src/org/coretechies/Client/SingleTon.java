/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Stage;
import org.cef.CefApp;
import org.coretechies.MessageTransporter.AddNewLesson;
import org.coretechies.MessageTransporter.LessonPlanning;
import org.coretechies.MessageTransporter.MyActivity;
import org.coretechies.MessageTransporter.MyActivityHoliday;
import org.coretechies.MessageTransporter.SearchResponseData;
import org.coretechies.MessageTransporter.UpdateUser;
import org.coretechies.MessageTransporter.UpdateUserInfo;

/**
 *
 * @author Tiwari
 */
public class SingleTon {

    static SingleTon singleTon;
    
    private String firstName;
    private String lastName;
    private File mediaFile;
    private File mediaFile3D;
    private Locale locale = new Locale("en", "EN");
    private ArrayList<String> curriculumList;
    private ArrayList<String> gradeList;
    private String selectedCurriculum;
    private String selectedGrade;
    private ArrayList<String> subjectList;
    private ArrayList<String> unitList;
    private ArrayList<String> chapterList;
    private ArrayList<String> lessonList;
    private ArrayList<String> countryList;
    private ArrayList<String> stateList;
    private ArrayList<String> schoolCategaryList;
    private ArrayList<String> schoolDistrictList;
    private ArrayList<String> schoolList;
    private boolean userNameVerification;
    private int changePasswordResponse;
    private int updatePasswordResponse;
    private ArrayList<Integer> scheduleList;
    private ArrayList<MyActivity> MyLeavesList;
    private ArrayList<MyActivityHoliday> schoolHolidayList;
    private ArrayList<LessonPlanning> lessonPlanningList;
    private ArrayList<String> addNewLessonSubjectList;
    private ArrayList<String> addNewLessonChapterList;
    private ArrayList<String> addNewLessonLessonList;
    private ArrayList<AddNewLesson> addNewLessonTopicList;
    private ArrayList<SearchResponseData> searchResponseDatas;
    private int id;
    private String holidayId;
    private ArrayList<UpdateUser> updateUserList;
    private String updateUserName;
    private String updateUserId;
    private int createUser;
    private UpdateUserInfo updateUserInfo;
    private boolean createUserResponse;
    private boolean lessonPlanResponse = false;
    private byte[] pdfByte;
    private ArrayList<String> searchSubjectList;
    private ArrayList<String> searchCurriculumList;
    private ArrayList<String> searchGradeList;
    private String searchTopic;
    private String searchLesson;
    private String searchChapter;
    private boolean search;
    private String searchText;
    private boolean fileDataResponce;
    private boolean updateResponse;
    private boolean userNameVerificationResponse;
    private boolean response;
    private ArrayList<Date> leaveDateList;
    private ArrayList<Date> classDateList;
    private boolean loginLessonPlanningResponce;
    private boolean mp4 = true;
    private boolean mp4player;
    private boolean mp43dplayer;
    private CefApp SWFCefApp;
    private Stage stage;
    private String fxmlPage;
    private boolean errorResponse;
    private boolean updateClassDayScheduleResponse;
    private boolean lessonPlanningErrorPane = false;
    private int logoutResponse = 0;
    private String subject;
    private String chapter;
    private String lesson;
    private String topic;
    private boolean inturrept;
    private ArrayList<Date> dateList;
    private ArrayList<String> dateStringList;
    private int mp4Packate=0;
    private int mp43dPackate=0;
    private File swfHtml;
    private File unityHtml;
    private long sizeSWF;
    private long sizeUnity3d;
    private RandomAccessFile randomAccessFileSWF;
    private FileLock fileLockSWF;
    private FileChannel fileChannelSWF;
    private RandomAccessFile randomAccessFileUnity;
    private FileLock fileLockUnity;
    private FileChannel fileChannelUnity;
    private byte[] tempUnity;
    private byte[] tempSWF;

    public void releaseFileLock() {
        if(fileLockUnity!=null && fileChannelUnity!=null && randomAccessFileUnity!=null){
            try {
                fileLockUnity.release();
                fileChannelUnity.close();
                randomAccessFileUnity.close();
            } catch (IOException ex) {
                System.out.println("release lock");
                Logger.getLogger(SingleTon.class.getName()).log(Level.SEVERE, null, ex);
            }
            fileLockUnity = null;
        }
    }

    public void acquireFileLock(File file) {
        System.out.println("file :  :::::: "+file);
        ArrayList<Object> arrayList = FileConstraint.lockFile(file);
        randomAccessFileUnity = (RandomAccessFile) arrayList.get(0);
        fileChannelUnity = (FileChannel) arrayList.get(1);
        fileLockUnity = (FileLock) arrayList.get(2);
    }
    
    public void releaseSWFFileLock() {
        if(fileLockSWF!=null && fileChannelSWF!=null && randomAccessFileSWF!=null){
            try {
                fileLockSWF.release();
                fileChannelSWF.close();
                randomAccessFileSWF.close();
            } catch (IOException ex) {
                System.out.println("release lock");
                Logger.getLogger(SingleTon.class.getName()).log(Level.SEVERE, null, ex);
            }
            fileLockSWF = null;
        }
    }

    public void acquireSWFFileLock(File file) {
        System.out.println("file swf:  :::::: "+file);
        ArrayList<Object> arrayList = FileConstraint.lockFile(file);
        randomAccessFileSWF = (RandomAccessFile) arrayList.get(0);
        fileChannelSWF = (FileChannel) arrayList.get(1);
        fileLockSWF = (FileLock) arrayList.get(2);
    }
    
    public byte[] getTempUnity() {
        return tempUnity;
    }

    public void setTempUnity(byte[] tempUnity) {
        this.tempUnity = tempUnity;
    }

    public byte[] getTempSWF() {
        return tempSWF;
    }

    public void setTempSWF(byte[] tempSWF) {
        this.tempSWF = tempSWF;
    }

    public RandomAccessFile getRandomAccessFileSWF() {
        return randomAccessFileSWF;
    }

    public void setRandomAccessFileSWF(RandomAccessFile randomAccessFileSWF) {
        this.randomAccessFileSWF = randomAccessFileSWF;
    }

    public FileLock getFileLockSWF() {
        return fileLockSWF;
    }

    public void setFileLockSWF(FileLock fileLockSWF) {
        this.fileLockSWF = fileLockSWF;
    }

    public FileChannel getFileChannelSWF() {
        return fileChannelSWF;
    }

    public void setFileChannelSWF(FileChannel fileChannelSWF) {
        this.fileChannelSWF = fileChannelSWF;
    }

    public RandomAccessFile getRandomAccessFileUnity() {
        return randomAccessFileUnity;
    }

    public void setRandomAccessFileUnity(RandomAccessFile randomAccessFileUnity) {
        this.randomAccessFileUnity = randomAccessFileUnity;
    }

    public FileLock getFileLockUnity() {
        return fileLockUnity;
    }

    public void setFileLockUnity(FileLock fileLockUnity) {
        this.fileLockUnity = fileLockUnity;
    }

    public FileChannel getFileChannelUnity() {
        return fileChannelUnity;
    }

    public void setFileChannelUnity(FileChannel fileChannelUnity) {
        this.fileChannelUnity = fileChannelUnity;
    }

    public long getSizeSWF() {
        return sizeSWF;
    }

    public void setSizeSWF(long sizeSWF) {
        this.sizeSWF = sizeSWF;
    }

    public long getSizeUnity3d() {
        return sizeUnity3d;
    }

    public void setSizeUnity3d(long sizeUnity3d) {
        this.sizeUnity3d = sizeUnity3d;
    }

    public File getSwfHtml() {
        return swfHtml;
    }

    public void setSwfHtml(File swfHtml) {
        this.swfHtml = swfHtml;
    }

    public File getUnityHtml() {
        return unityHtml;
    }

    public void setUnityHtml(File unityHtml) {
        this.unityHtml = unityHtml;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public boolean isLessonPlanResponse() {
        return lessonPlanResponse;
    }

    public File getMediaFile3D() {
        return mediaFile3D;
    }

    public void setMediaFile3D(File mediaFile3D) {
        this.mediaFile3D = mediaFile3D;
    }

    public void setLessonPlanResponse(boolean lessonPlanResponse) {
        this.lessonPlanResponse = lessonPlanResponse;
    }
    
    static{
        singleTon = null;
    }
    
    private SingleTon() {
    }
    
    public static SingleTon getSingleTon(){
        if(singleTon == null){
            singleTon = new SingleTon();
        }
        return singleTon;
    }

    public static void removeSingleTon(){
        singleTon = null;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public ArrayList<String> getCurriculumList() {
        return curriculumList;
    }

    public void setCurriculumList(ArrayList<String> curriculumList) {
        this.curriculumList = curriculumList;
    }

    public ArrayList<String> getGradeList() {
        return gradeList;
    }

    public void setGradeList(ArrayList<String> gradeList) {
        this.gradeList = gradeList;
    }

    public String getSelectedCurriculum() {
        return selectedCurriculum;
    }

    public void setSelectedCurriculum(String selectedCurriculum) {
        this.selectedCurriculum = selectedCurriculum;
    }

    public String getSelectedGrade() {
        return selectedGrade;
    }

    public void setSelectedGrade(String selectedGrade) {
        this.selectedGrade = selectedGrade;
    }

    public ArrayList<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<String> subjectList) {
        this.subjectList = subjectList;
    }

    public ArrayList<String> getUnitList() {
        return unitList;
    }

    public void setUnitList(ArrayList<String> unitList) {
        this.unitList = unitList;
    }

    public ArrayList<String> getChapterList() {
        return chapterList;
    }

    public void setChapterList(ArrayList<String> chapterList) {
        this.chapterList = chapterList;
    }

    public ArrayList<String> getLessonList() {
        return lessonList;
    }

    public void setLessonList(ArrayList<String> lessonList) {
        this.lessonList = lessonList;
    }

    public ArrayList<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<String> countryList) {
        this.countryList = countryList;
    }

    public ArrayList<String> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<String> stateList) {
        this.stateList = stateList;
    }

    public ArrayList<String> getSchoolCategaryList() {
        return schoolCategaryList;
    }

    public void setSchoolCategaryList(ArrayList<String> schoolCategaryList) {
        this.schoolCategaryList = schoolCategaryList;
    }

    public ArrayList<String> getSchoolDistrictList() {
        return schoolDistrictList;
    }

    public void setSchoolDistrictList(ArrayList<String> schoolDistrictList) {
        this.schoolDistrictList = schoolDistrictList;
    }

    public ArrayList<String> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(ArrayList<String> schoolList) {
        this.schoolList = schoolList;
    }

    public boolean isUserNameVerification() {
        return userNameVerification;
    }

    public void setUserNameVerification(boolean userNameVerification) {
        this.userNameVerification = userNameVerification;
    }

    public int getChangePasswordResponse() {
        return changePasswordResponse;
    }

    public void setChangePasswordResponse(int changePasswordResponse) {
        this.changePasswordResponse = changePasswordResponse;
    }

    public ArrayList<Integer> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(ArrayList<Integer> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public ArrayList<MyActivity> getMyLeavesList() {
        return MyLeavesList;
    }

    public void setMyLeavesList(ArrayList<MyActivity> MyLeavesList) {
        this.MyLeavesList = MyLeavesList;
    }

    public ArrayList<MyActivityHoliday> getSchoolHolidayList() {
        return schoolHolidayList;
    }

    public void setSchoolHolidayList(ArrayList<MyActivityHoliday> schoolHolidayList) {
        this.schoolHolidayList = schoolHolidayList;
    }
    public ArrayList<LessonPlanning> getLessonPlanningList() {
        return lessonPlanningList;
    }

    public void setLessonPlanningList(ArrayList<LessonPlanning> lessonPlanningList) {
        this.lessonPlanningList = lessonPlanningList;
    }

    public ArrayList<String> getAddNewLessonSubjectList() {
        return addNewLessonSubjectList;
    }

    public void setAddNewLessonSubjectList(ArrayList<String> addNewLessonSubjectList) {
        this.addNewLessonSubjectList = addNewLessonSubjectList;
    }

    public ArrayList<String> getAddNewLessonChapterList() {
        return addNewLessonChapterList;
    }

    public void setAddNewLessonChapterList(ArrayList<String> addNewLessonChapterList) {
        this.addNewLessonChapterList = addNewLessonChapterList;
    }

    public ArrayList<String> getAddNewLessonLessonList() {
        return addNewLessonLessonList;
    }

    public void setAddNewLessonLessonList(ArrayList<String> addNewLessonLessonList) {
        this.addNewLessonLessonList = addNewLessonLessonList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(String holidayId) {
        this.holidayId = holidayId;
    }

    public ArrayList<UpdateUser> getUpdateUserList() {
        return updateUserList;
    }

    public void setUpdateUserList(ArrayList<UpdateUser> updateUserList) {
        this.updateUserList = updateUserList;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public int getUpdatePasswordResponse() {
        return updatePasswordResponse;
    }

    public void setUpdatePasswordResponse(int updatePasswordResponse) {
        this.updatePasswordResponse = updatePasswordResponse;
    }

    public int getCreateUser() {
        return createUser;
    }

    public void setCreateUser(int createUser) {
        this.createUser = createUser;
    }

    public UpdateUserInfo getUpdateUserInfo() {
        return updateUserInfo;
    }

    public void setUpdateUserInfo(UpdateUserInfo updateUserInfo) {
        this.updateUserInfo = updateUserInfo;
    }

    public boolean isCreateUserResponse() {
        return createUserResponse;
    }

    public void setCreateUserResponse(boolean createUserResponse) {
        this.createUserResponse = createUserResponse;
    }

    public byte[] getPdfByte() {
        return pdfByte;
    }

    public void setPdfByte(byte[] pdfByte) {
        this.pdfByte = pdfByte;
    }

    public ArrayList<AddNewLesson> getAddNewLessonTopicList() {
        return addNewLessonTopicList;
    }

    public void setAddNewLessonTopicList(ArrayList<AddNewLesson> addNewLessonTopicList) {
        this.addNewLessonTopicList = addNewLessonTopicList;
    }

    public ArrayList<SearchResponseData> getSearchResponseDatas() {
        return searchResponseDatas;
    }

    public void setSearchResponseDatas(ArrayList<SearchResponseData> searchResponseDatas) {
        this.searchResponseDatas = searchResponseDatas;
    }

    public ArrayList<String> getSearchSubjectList() {
        return searchSubjectList;
    }

    public void setSearchSubjectList(ArrayList<String> searchSubjectList) {
        this.searchSubjectList = searchSubjectList;
    }

    public ArrayList<String> getSearchCurriculumList() {
        return searchCurriculumList;
    }

    public void setSearchCurriculumList(ArrayList<String> searchCurriculumList) {
        this.searchCurriculumList = searchCurriculumList;
    }

    public ArrayList<String> getSearchGradeList() {
        return searchGradeList;
    }

    public void setSearchGradeList(ArrayList<String> searchGradeList) {
        this.searchGradeList = searchGradeList;
    }

    public String getSearchTopic() {
        return searchTopic;
    }

    public void setSearchTopic(String searchTopic) {
        this.searchTopic = searchTopic;
    }

    public String getSearchLesson() {
        return searchLesson;
    }

    public void setSearchLesson(String searchLesson) {
        this.searchLesson = searchLesson;
    }

    public String getSearchChapter() {
        return searchChapter;
    }

    public void setSearchChapter(String searchChapter) {
        this.searchChapter = searchChapter;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public boolean isFileDataResponce() {
        return fileDataResponce;
    }

    public void setFileDataResponce(boolean fileDataResponce) {
        this.fileDataResponce = fileDataResponce;
    }

    public boolean isUpdateResponse() {
        return updateResponse;
    }

    public void setUpdateResponse(boolean updateResponse) {
        this.updateResponse = updateResponse;
    }

    public boolean isUserNameVerificationResponse() {
        return userNameVerificationResponse;
    }

    public void setUserNameVerificationResponse(boolean userNameVerificationResponse) {
        this.userNameVerificationResponse = userNameVerificationResponse;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public ArrayList<Date> getLeaveDateList() {
        return leaveDateList;
    }

    public void setLeaveDateList(ArrayList<Date> leaveDateList) {
        this.leaveDateList = leaveDateList;
    }

    public ArrayList<Date> getClassDateList() {
        return classDateList;
    }

    public void setClassDateList(ArrayList<Date> classDateList) {
        this.classDateList = classDateList;
    }

    public boolean isLoginLessonPlanningResponce() {
        return loginLessonPlanningResponce;
    }

    public void setLoginLessonPlanningResponce(boolean loginLessonPlanningResponce) {
        this.loginLessonPlanningResponce = loginLessonPlanningResponce;
    }

    public File getMediaFile() {
        return mediaFile;
    }

    public void setMediaFile(File mediaFile) {
        this.mediaFile = mediaFile;
    }

    public CefApp getSWFCefApp() {
        return SWFCefApp;
    }

    public void setSWFCefApp(CefApp SWFCefApp) {
        this.SWFCefApp = SWFCefApp;
    }

    public boolean isMp4() {
        return mp4;
    }

    public void setMp4(boolean mp4) {
        this.mp4 = mp4;
    }

    public boolean isMp4player() {
        return mp4player;
    }

    public void setMp4player(boolean mp4player) {
        this.mp4player = mp4player;
    }

    public boolean isMp43dplayer() {
        return mp43dplayer;
    }

    public void setMp43dplayer(boolean mp43dplayer) {
        this.mp43dplayer = mp43dplayer;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getFxmlPage() {
        return fxmlPage;
    }

    public void setFxmlPage(String fxmlPage) {
        this.fxmlPage = fxmlPage;
    }

    public boolean isErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(boolean errorResponse) {
        this.errorResponse = errorResponse;
    }

    public boolean isUpdateClassDayScheduleResponse() {
        return updateClassDayScheduleResponse;
    }

    public void setUpdateClassDayScheduleResponse(boolean updateClassDayScheduleResponse) {
        this.updateClassDayScheduleResponse = updateClassDayScheduleResponse;
    }

    public boolean isLessonPlanningErrorPane() {
        return lessonPlanningErrorPane;
    }

    public void setLessonPlanningErrorPane(boolean lessonPlanningErrorPane) {
        this.lessonPlanningErrorPane = lessonPlanningErrorPane;
    }

    public int getLogoutResponse() {
        return logoutResponse;
    }

    public void setLogoutResponse(int logoutResponse) {
        this.logoutResponse = logoutResponse;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public boolean isInturrept() {
        return inturrept;
    }

    public void setInturrept(boolean inturrept) {
        this.inturrept = inturrept;
    }

    public ArrayList<Date> getDateList() {
        return dateList;
    }

    public void setDateList(ArrayList<Date> dateList) {
        this.dateList = dateList;
    }

    public ArrayList<String> getDateStringList() {
        return dateStringList;
    }

    public void setDateStringList(ArrayList<String> dateStringList) {
        this.dateStringList = dateStringList;
    }

    public int getMp4Packate() {
        return mp4Packate;
    }

    public void setMp4Packate(int mp4Packate) {
        this.mp4Packate = mp4Packate;
    }

    public int getMp43dPackate() {
        return mp43dPackate;
    }

    public void setMp43dPackate(int mp43dPackate) {
        this.mp43dPackate = mp43dPackate;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
}
