/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.coretechies.forms.SubjController;
import org.coretechies.forms.controller.SubjectController;

/**
 *
 * @author Tiwari
 */
public class DecryptionQueueUnity3D extends Observable implements Runnable{

    BlockingQueue<byte[]> encryptedData;
    
    static DecryptionQueueUnity3D decryption;
    
    static{
        decryption=null;
    }

    private DecryptionQueueUnity3D() {
       encryptedData = new LinkedBlockingQueue<byte[]>() ;
    }
    
    public static DecryptionQueueUnity3D getDecryption(){
        if(decryption == null){
            decryption=new DecryptionQueueUnity3D();
        }
        return decryption;
    }

    public BlockingQueue<byte[]> getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(BlockingQueue<byte[]> encryptedData) {
        this.encryptedData = encryptedData;
    }


    @Override
    public void run() {
        File f = null;
        File f1 = null;
        FileOutputStream fileOutputStream1 = null;
        BufferedOutputStream stream;

        byte[] bs;
        f = new File("temp");
        f.mkdir();
        f1 = new File(f,"temp.unity3d");
        try {
            while(true){
                
                while(true){
                    ClientSocket clientSocket = ClientSocket.getClientSocket();
//                    System.out.print("While");
//                    synchronized(clientSocket){
//                        boolean isPlaying = clientSocket.isIsPlaying();
                        if(!encryptedData.isEmpty())// && !isPlaying)
                            break;
//                    }
                }
//                SubjectController.setMp(false);
                
                System.out.println("PPPPPPP");
                byte[] newBs = getEncryptedData().poll();
                if(new String(newBs,0,newBs.length).equalsIgnoreCase("End")) break;
                if(!f1.exists()){
                    fileOutputStream1 = new FileOutputStream(f1);
                }else{
                    fileOutputStream1 = new FileOutputStream(f1, true);
                }stream = new BufferedOutputStream(fileOutputStream1);
                
                stream.write(newBs,0, newBs.length-1);
                
                fileOutputStream1.flush();
                stream.flush();
                
                stream.close();
                
                ClientSocket clientSocket = ClientSocket.getClientSocket();
//                synchronized(clientSocket){
//                    clientSocket.setIsPlaying(true);
//                }
//                System.out.print("Play");
//                setChanged();
//                notifyObservers();
//                SubjectController.setMp(true);
            }
        } catch (IOException ex) {
            System.out.println("Exception");
            ex.printStackTrace();
            Logger.getLogger(DecryptionQueueUnity3D.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                if(fileOutputStream1!=null){
                    fileOutputStream1.close();
                };
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
//                    if(f1.exists())
//                        f1.delete();
//                    if(f.exists())
//                        f.delete();
        }
    }
}
