/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.cef.OS;
import org.coretechies.MessageTransporter.Data;
import tests.simple.MainFrame1;

/**
 *
 * @author Tiwari
 */
public class Unity3dThread extends JFrame implements Runnable{

    private BlockingQueue<Data> queueData;
    static Unity3dThread imageThread;
    boolean running = true;
    int num;
    int height = 0;
    int width = 0;
    long size;
    byte[] newBs;
    
    static{
        imageThread=null;
    }

    public Unity3dThread(int num) {
        this.num = num;
        queueData = new LinkedBlockingQueue<Data>();
    }

    public BlockingQueue<Data> getQueueData() {
        return queueData;
    }

    public void setQueueData(BlockingQueue<Data> queueData) {
        this.queueData = queueData;
    }

    @Override
    public void run() {
        try {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screen = toolkit.getScreenSize();
            height = screen.height -30;
            width = screen.width -10;
            
            final File f = File.createTempFile("tmp", ".unity3d");
            FileConstraint.file(f.getAbsolutePath());
            System.out.println("File path: "+f.getAbsolutePath());
            System.out.println("Parent File path: "+f.getParentFile());
//            Runtime.getRuntime().exec("attrib +H "+f);
//            f.deleteOnExit();
            FileOutputStream fileOutputStream1 = null;
            BufferedOutputStream stream;
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Unity3dThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            fileOutputStream1 = new FileOutputStream(f);
            stream = new BufferedOutputStream(fileOutputStream1);
            try{
                int store = 0;
                while(true){
                    while(true){
                        if(!queueData.isEmpty())
                            break;
                    }

                    byte[] bs = getQueueData().poll().getBs();

                    if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END")))
                        break;
                    stream.write(bs,0, bs.length);
                }
                Unity3dJoin unity3dJoin = Unity3dJoin.getUnity3dJoin();
                FileNumber fileNumber = new FileNumber();
                fileNumber.setFile(f);
                fileNumber.setNum(num);
                unity3dJoin.setFileNumber(fileNumber, num);
            } catch (IOException ex) {
                System.out.println("Exception");
                ex.printStackTrace();
                Logger.getLogger(DecryptionQueueUnity3D.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                try {
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            System.out.println("image prints");
        } catch (IOException ex) {
            Logger.getLogger(Unity3dThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
