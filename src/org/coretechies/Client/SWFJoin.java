/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import org.cef.OS;
import tests.simple.MainFrame111;

/**
 *
 * @author Tiwari
 */
public class SWFJoin extends JFrame implements Runnable{
    
    private BlockingQueue<FileNumber> fileNumbers;
    static SWFJoin swfJoin;
    int height = 0;
    int width = 0;
    private FileNumber[] fileNumber = new FileNumber[20];
    MainFrame111 mainFrame;
    boolean bool=false;

    public FileNumber[] getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(FileNumber fileNumber, int num) {
        this.fileNumber[num] = fileNumber;
    }

    static{
        swfJoin = null;
    }
    
    private SWFJoin() {
        fileNumbers = new LinkedBlockingQueue<>();
    }
    
    public static SWFJoin getSWFJoin(){
        if(swfJoin == null){
            swfJoin = new SWFJoin();
        }
        return swfJoin;
    }

    public BlockingQueue<FileNumber> getFileNumbers() {
        return fileNumbers;
    }

    public void setFileNumbers(BlockingQueue<FileNumber> fileNumbers) {
        this.fileNumbers = fileNumbers;
    }

    @Override
    public void run() {
//        final File f = fileNumber[0].getFile();
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("bol1 = "+bool);
//                    if(bool){
//                    System.out.println("bol = "+bool);
////                        mainFrame.reURL();
//                        if(mainFrame!=null){
//                            mainFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
//                            Thread.sleep(500);
////                            mainFrame.setAlwaysOnTop( true );
//                            mainFrame.setVisible(true);
////                            mainFrame.setLocationByPlatform( true );
//                            //hide();.setVisible(true);
//    //                        mainFrame.toFront();//setAlwaysOnTop(true);//setVisible(true);
//                        }
//                    }else{
                        bool = true;
                        SingleTon singleTon = SingleTon.getSingleTon();
                        singleTon.setSwfHtml(null);
                        final File file = File.createTempFile("tmp", ".html");
                        FileConstraint.file(file.getAbsolutePath());
                        System.out.println("File path: "+file.getAbsolutePath());
    //                    Runtime.getRuntime().exec("attrib +H "+file);
//                        file.deleteOnExit();
                        createFileLoading(file);

                        mainFrame = new MainFrame111("file:///", file, OS.isLinux(), false, SWFJoin.this);
//                        fileNumber[0].getFile().delete();
//                    }
                } catch (IOException ex) {
                    Logger.getLogger(SWFJoin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        t.setDaemon(true);
        t.start();
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        try {
            if(!bool){
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screen = toolkit.getScreenSize();
                height = screen.height -35;
                width = screen.width -10;
                for(int i=0; i<20; i++){
                    if(fileNumber[i]==null){
                        System.out.println("i for null := "+i);
                        i=-1;
                    }
                }
                FileOutputStream fileOutputStream = null;
                FileInputStream fileInputStream = null;
                BufferedOutputStream stream = null;

                for(int i=0; i<20; i++){
                    System.out.println("File : "+i+" "+fileNumber[i]);
                }

                try {
                    fileOutputStream = new FileOutputStream(fileNumber[0].getFile(), true);
                    stream = new BufferedOutputStream(fileOutputStream);
                    byte[] buf = new byte[524288];
                    int len;

                    for(int i=1; i<20; i++){
                        fileInputStream = new FileInputStream(fileNumber[i].getFile());
                        while ((len = fileInputStream.read(buf)) > 0) {
                            stream.write(buf, 0, len);
                        }
                        fileInputStream.close();
                        fileNumber[i].getFile().delete();
                    }
                    stream.close();
                    
                    fileInputStream = new FileInputStream(fileNumber[0].getFile());
                    byte[] tempbs = new byte[130];
                    fileInputStream.read(tempbs);
                    SingleTon singleTon = SingleTon.getSingleTon();
                    singleTon.setTempSWF(tempbs);
                    singleTon = SingleTon.getSingleTon();
                    System.out.println("bs :  "+new String(singleTon.getTempSWF(), 0, singleTon.getTempSWF().length));
                    FileConstraint.writeMoreToFile(fileNumber[0].getFile());
                    singleTon = SingleTon.getSingleTon();
                    singleTon.acquireSWFFileLock(fileNumber[0].getFile());
                    
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(SWFJoin.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SWFJoin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            File file = File.createTempFile("tmp", ".html");
            FileConstraint.file(file.getAbsolutePath());
            System.out.println("File path: "+file.getAbsolutePath());
            Runtime.getRuntime().exec("attrib +H "+file);
            file.deleteOnExit();
            createFileSwf(file, fileNumber[0].getFile());
            SingleTon singleTon = SingleTon.getSingleTon();
            singleTon.setSwfHtml(file);
//            final File f = fileNumber[0].getFile();
//            Thread t = new Thread(new Runnable() {
//                public void run() {
//                    MainFrame111 mainFrame = new MainFrame111("file:///", file, OS.isLinux(), false, SWFJoin.this);
////                    mainFrame.dispose();
//                    f.delete();
//                    file.delete();
//                }
//            });
//            t.setDaemon(true);
//            t.start();
            System.out.println("image prints");

        } catch (IOException ex) {
            Logger.getLogger(SWFJoin.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void createFileSwf(File file, File f) {
        String content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "	<head>\n" +
        "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                        "	</head>\n" +
        "	<body style = \"margin:0px\">\n" +
        "		<div class=\"content\">\n" +
        "			<div id=\"unityPlayer\">\n"
                        + "<object width=\'"+width+"\' height=\'"+height+"\' data=\""+f+"\"></object>" +

                                "</div>\n" +
        "		</div>\n" +
        "	</body>\n" +
        "</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

            if (!file.exists()) {
                    file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
    
     private void createFileLoading(File file) {
        String content = "<html>\n" +
                        "<head>\n" +
                        "<title>Test Page</title>\n" +
                        "<style>\n" +
                        "label{\n" +
                        "	width:100px;\n"
                        + "font-size:72px;\n"
                        + "font-weight:bold;\n"
                        + "color:#888;\n" +
                        "	height:100px;\n" +
                        "	position:fixed;\n" +
                        "	left:40%;\n" +
                        "	top:40%;\n" +
                        "}\n" +
                        "</style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "<label> loading...</label>\n" +
                        "</body>\n" +
                        "</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

            if (!file.exists()) {
                    file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
}
