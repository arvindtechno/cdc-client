/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

/**
 *
 * @author Tiwari
 */
public enum MessageType {
    
    AUTO(Byte.parseByte("1"));
    
//    MANUAL(Byte.parseByte(2+""));
//    MANUAL(2);
    
    private final Byte MessageTypeValue;

    private MessageType(Byte MessageTypeValue) {
        this.MessageTypeValue = MessageTypeValue;
    }

    public Byte getMessageTypeValue() {
        return MessageTypeValue;
    }
    
    private static MessageType parse(Byte messageTypeValue) {
        MessageType messageType = null;
        for (MessageType item : MessageType.values()) {
            if (item.getMessageTypeValue().equals(messageTypeValue)) {
                messageType = item;
                break;
            }
        }
        return messageType;
    }
}