/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import org.coretechies.MessageTransporter.Data;
import org.coretechies.MessageTransporter.MessageResponse;
import tests.simple.MainFrame11;

/**
 *
 * @author Tiwari
 */
public class ImageThread extends JFrame implements Runnable{

    private BlockingQueue<Data> queueData;
    static ImageThread imageThread;
    long size;
    byte[] newBs;
    
    static{
        imageThread=null;
    }

    private ImageThread() {
       queueData = new LinkedBlockingQueue<Data>() ;
    }
    
    public static ImageThread getObject(){
        if(imageThread == null){
            imageThread=new ImageThread();
        }
        return imageThread;
    }

    public BlockingQueue<Data> getQueueData() {
        return queueData;
    }

    public void setQueueData(BlockingQueue<Data> queueData) {
        this.queueData = queueData;
    }

    @Override
    public void run() {
        try {
            int store = 0;  
            while(true){
                if(!queueData.isEmpty()){
                    size = getQueueData().poll().getSequenceNumber()+10240;
                    newBs =  new byte[(int)size];
                    break;
                }
            }
            
            while(true){
                while(true){
                    if(!queueData.isEmpty())
                        break;
                }
                
                byte[] bs = getQueueData().poll().getBs();
                
                for(byte b : bs)
                    newBs[store++] = b;
                
                if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))) break;
            }
            final BufferedImage img1 = ImageIO.read(new ByteArrayInputStream(newBs));
            
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new MainFrame11(img1, ImageThread.this);
                }
            });
            System.out.println("image prints");
        } catch (IOException ex) {
            Logger.getLogger(ImageThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
