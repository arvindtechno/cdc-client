/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 *
 * @author Tiwari
 */
public class ClientWrite implements Runnable{
    
    private Socket socket;
    
    ObjectOutputStream oout;
    ClientSocket clientSocket;
    SingleTon singleTon;
    

    public ClientWrite(ClientSocket clientsocket) {
        this.clientSocket = clientsocket;
        this.socket=clientSocket.getSocket();
         
        ObjectOutputStream oout = null;
    }
   

    @Override
    public void run() {
        MessageRequest request;
        ClientSocket cs =ClientSocket.getClientSocket();
        ClientSocket.setBool(true);
        
        try {
            oout = new ObjectOutputStream(socket.getOutputStream());
            while(true){ 
                Queue<MessageRequest> queue =clientSocket.getRequestQueue();
                while(queue.isEmpty()){
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientWrite.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                singleTon = SingleTon.getSingleTon();
                singleTon.setResponse(false);
                request = queue.poll();
                oout.writeObject(request);
                System.out.println("ClientWrite");
                oout.flush();
                System.out.println("flushed");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientWrite.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
}