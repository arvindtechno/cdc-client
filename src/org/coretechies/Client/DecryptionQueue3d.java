/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiwari
 */
public class DecryptionQueue3d implements Runnable{

    BlockingQueue<byte[]> encryptedData;
    static boolean checkFile3D;
    private boolean end = false;
    private FileLock fileLock;
    private FileChannel channel;
    private RandomAccessFile file;
    static DecryptionQueue3d decryption;
    SingleTon singleTon;
    private long size;
    static{
        decryption=null;
        checkFile3D = false;
    }

    private DecryptionQueue3d() {
       encryptedData = new LinkedBlockingQueue<byte[]>() ;
    }
    
    public static DecryptionQueue3d getDecryption(){
        if(decryption == null){
            decryption=new DecryptionQueue3d();
        }
        return decryption;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public static void closeDecryption() {
        decryption = null;
    }

    public FileLock getFileLock() {
        return fileLock;
    }

    public void setFileLock() {
        try {
            fileLock.release();
            channel.close();
            file.close();
        } catch (IOException ex) {
            System.out.println("release lock");
            Logger.getLogger(DecryptionQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public BlockingQueue<byte[]> getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(BlockingQueue<byte[]> encryptedData) {
        this.encryptedData = encryptedData;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
    ArrayList<byte[]> arrayList;

    public static boolean isCheckFile3D() {
        return checkFile3D;
    }

    public static void setCheckFile3D(boolean checkFile3D) {
        DecryptionQueue3d.checkFile3D = checkFile3D;
    }

    @Override
    public void run() {
        try {

            System.out.println("TRYYYYYYY");
            FileOutputStream fileOutputStream1 = null;
            BufferedOutputStream stream;
            File f1 = File.createTempFile("tmp", ".mp4");
            FileConstraint.file(f1.getAbsolutePath());
            singleTon = SingleTon.getSingleTon();
            singleTon.setMediaFile3D(f1);
            System.out.println("File path: "+f1.getAbsolutePath());
            Runtime.getRuntime().exec("attrib +H "+f1);
            System.out.println("trChechFile");
            f1.deleteOnExit();
            checkFile3D = false;
            try {
                while(true){
                    while(true){
                        if(!encryptedData.isEmpty())
                            break;
                    }
                    
                    byte[] newBs = getEncryptedData().peek();                    
//                    System.out.println("length : "+getEncryptedData().size());
                    if(!(new String(newBs,0,newBs.length).trim().equalsIgnoreCase("End")))
                        if(getEncryptedData().size() < size*2)
                            continue;
                    
                    if(!f1.exists())
                        fileOutputStream1 = new FileOutputStream(f1);
                    else
                        fileOutputStream1 = new FileOutputStream(f1, true);
                    
                    stream = new BufferedOutputStream(fileOutputStream1);
                    
                    synchronized(getEncryptedData()){
                        while((newBs=getEncryptedData().poll())!=null){
                            try{
                                if(new String(newBs,0,newBs.length).trim().equalsIgnoreCase("End")){
                                    singleTon.setMp43dPackate(-1);
                                    Thread.currentThread().stop();
                                    break;
                                }
                            }catch(Exception e){
                                e.printStackTrace();
                            }                            
                            stream.write(newBs,0, newBs.length);
                            stream.flush();
                        }
                    }
                    stream.close();
                    singleTon.setMp43dPackate(singleTon.getMp43dPackate()+1);
                    checkFile3D = true;
                    if(singleTon.getMp43dPackate()==2){
                        FileConstraint.writeMoreToFile(f1);
                        ArrayList<Object> arrayList = FileConstraint.lockFile(f1);
                        file = (RandomAccessFile) arrayList.get(0);
                        channel = (FileChannel) arrayList.get(1);
                        fileLock = (FileLock) arrayList.get(2);
                    }
                    System.out.println("TRUEEEEEEEEEE");
                }
            } catch (Exception ex) {
                System.out.println("Exception");
                ex.printStackTrace();
            } finally{
                try {
                    if(fileOutputStream1!=null){
                        fileOutputStream1.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            singleTon.setMp4Packate(-1);
        } catch (IOException ex) {
            Logger.getLogger(DecryptionQueue3d.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
