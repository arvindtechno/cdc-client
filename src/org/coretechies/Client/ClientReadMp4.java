/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.coretechies.MessageTransporter.*;

/**
 *
 * @author Tiwari
 */
public class ClientReadMp4 extends JFrame implements Runnable{
    
    private SingleTon singleTon;
    private Socket socket;
//    private MediaDataRequest mediaDataRequest;
    ClientSocket clientSocket;
    private Unity3dThread thread;
    String Path;
    String str;
    ClientWriteMp4 write;
    public ClientReadMp4(Socket socket, ClientWriteMp4 write) {
//        synchronized(mediaDataRequest){
        this.write = write;
            this.socket=socket;
//        }
    }

    public ClientReadMp4() {
    }

    @Override
    public void run() {
        ObjectInputStream ois = null;
        singleTon = SingleTon.getSingleTon();
        try {
            boolean mp4Flage = false;
            boolean mp43dFlage = false;
//            System.out.println("Client Mp4");
            ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            while(true){
                MessageResponse response = (MessageResponse)ois.readObject();
                String mType = response.getMessageType();
//                System.out.println("MType : "+mType);
                if(mType.equals("MediaDataResponse")){
                    System.out.println("sleepin Data Response");
                    MediaDataResponse dataResponse =(MediaDataResponse) response;
                    Path = dataResponse.getPath();
                }else if(mType.equalsIgnoreCase("MediaDatamp4")){
                    DecryptionQueue decryptionQueue = DecryptionQueue.getDecryption();
                    Data data = (Data)response;
                    if(!mp4Flage){
                        data = (Data)response;
                        decryptionQueue = DecryptionQueue.getDecryption();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        Thread decryptionQueueThread = new Thread(decryptionQueue);
                        decryptionQueueThread.setDaemon(true);
                        decryptionQueueThread.start();
                        byte[] bs =new byte[127];
                        data = (Data)response;
                        bs = data.getBs();
                        data = (Data)response;
                        response = (MessageResponse)ois.readObject();
                    }
                    byte[] bs = new byte[524288];
                    int i =0;
                    do{
                        data = (Data)response;
                        bs = data.getBs();
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))||bs.length==0){
                            queue.add("End".getBytes());
                            decryptionQueue.setEnd(true);
                            mp4Flage = false;
                            break;
                        }
                        if((new String(bs,0,bs.length).trim().endsWith("continue"))||bs.length==0){
                            mp4Flage = true;
                            break;
                        }
                        queue.add(bs);
                        
                        singleTon = SingleTon.getSingleTon();
                        if(i==1000){
                            clientSocket = ClientSocket.getClientSocket();
                            Queue<MessageRequest> queue1 = clientSocket.getMp4requestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("mp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            write.setRequestQueue(continueRequest);
                            i=0;
                        }
                        i++;
                    }while((response = (MessageResponse)ois.readObject())!=null);
                    
                    if(!mp4Flage){
                        System.out.println("mp4flage");
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        queue.add("End".getBytes());
                        queue.add(bs);
                    }
                    System.out.println("image prints");
                    
                }else if(mType.equalsIgnoreCase("MediaData3dmp4")){
                    DecryptionQueue3d decryptionQueue = DecryptionQueue3d.getDecryption();
                    Data data = (Data)response;
                    if(!mp43dFlage){
                        decryptionQueue = DecryptionQueue3d.getDecryption();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        Thread decryptionQueueThread = new Thread(decryptionQueue);
                        decryptionQueueThread.setDaemon(true);
                        decryptionQueueThread.start();
                        byte[] bs =new byte[127];
                        data = (Data)response;
                        bs = data.getBs();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        data = (Data)response;
                        response = (MessageResponse)ois.readObject();
                    }byte[] bs = new byte[524288];
                    int i =0;
                    do{
                        data = (Data)response;
                        bs = data.getBs();
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))||bs.length==0){
                            decryptionQueue.setEnd(true);
                            queue.add("End".getBytes());
                            mp43dFlage = false;
                            break;
                        }
                        if((new String(bs,0,bs.length).trim().endsWith("continue"))||bs.length==0){
                            mp43dFlage = true;
                            break;
                        }
                        queue.add(bs);
                        
                        singleTon = SingleTon.getSingleTon();
                        if(i==1000){
                            System.out.println("Continnnnnnnn");
                            clientSocket = ClientSocket.getClientSocket();
                            Queue<MessageRequest> queue1 = clientSocket.getMp4requestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("3dmp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            write.setRequestQueue(continueRequest);
                        }
                        i++;
                    }while((response = (MessageResponse)ois.readObject())!=null);
                    if(!mp43dFlage){
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        queue.add("End".getBytes());
                        queue.add(bs);
                    }
                    System.out.println("image prints");
                }
                singleTon = SingleTon.getSingleTon();   
                singleTon.setResponse(true);
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("IOException");
            Logger.getLogger(ClientReadMp4.class.getName()).log(Level.SEVERE, null, ex);
        }catch(NullPointerException npe){
            PrintWriter out = null;
            try {
                out = new PrintWriter(socket.getOutputStream(),true);
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                boolean flag = true;
                while(flag)
                {
                    str = in.readLine();
                    if(!str.equalsIgnoreCase("END"))
                    {
                        out.println(str);
                    }
                    else
                    {
                        flag=false;
                        out.println("END");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ClientReadMp4.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Inner IOException");
            } finally {
                out.close();
            }
        }
    }
}