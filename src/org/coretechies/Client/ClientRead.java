/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import org.coretechies.MessageTransporter.*;
import org.coretechies.server.Decryption;
import org.coretechies.server.KeyGeneration;

/**
 *
 * @author Tiwari
 */
public class ClientRead extends JFrame implements Runnable{
    
    private SingleTon singleTon;
    private Socket socket;
    private String Hash;
    ClientSocket clientSocket;
    String Path;
    String str;
    boolean mp4Flage = false;
    boolean mp43dFlage = false;
    
    public ClientRead(ClientSocket clientsocket) {
        this.clientSocket = clientsocket;
        this.socket=clientSocket.getSocket();
    }

    public ClientRead() {
    }

    @Override
    public void run() {
        ObjectInputStream ois = null;
        BufferedImage Imagestored;
        Image img;
        singleTon = SingleTon.getSingleTon();
        try {
            ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            while(true){
                MessageResponse response = (MessageResponse)ois.readObject();
                
                String mType = response.getMessageType();
                System.out.println("a/"+response.getMessageType());
                System.out.println("a/"+response.getHash());

                if(mType.equals("LoginResponse")){
                    
                    LoginResponse loginResponse = (LoginResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setResponse(true);
                    if(loginResponse.getResponse() == 1){
                        clientSocket.setX(1);
                        Hash = loginResponse.getHash();
                        clientSocket.setHash(Hash);
                        singleTon.setFirstName(loginResponse.getFirstName());
                        singleTon.setLastName(loginResponse.getLastName());
                        singleTon.setLoginLessonPlanningResponce(loginResponse.isCheckLessonPlan());
                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        ResourceRequest request = new ResourceRequest();
                        request.setHash(Hash);
                        request.setIdType(0);
                        request.setMessageType("Resource");
                        queue.add(request);
                    }else{
                        System.out.println("Client Read");
                        clientSocket.setX(loginResponse.getResponse());
                        socket.close();
                        break;
                    }
                }else if(mType.equals("ResourceResponse")){
                    System.out.println("hello");
                    ResourceResponce responce = (ResourceResponce) response;
                    System.out.println("hash>"+responce.getHash());
                    System.out.println("typees>"+responce.getMessageType());
                    ArrayList<String> curriculumList = responce.getCurriculum();
                    ArrayList<String> gradeList = responce.getGrade();
                    singleTon.setCurriculumList(curriculumList);
                    singleTon.setGradeList(gradeList);
                    
                }else if(mType.equals("DataResponse")){
                    System.out.println("sleepin Data Response");
                    DataResponse dataResponse =(DataResponse) response;
                    System.out.println("DATA HASH "+dataResponse.getHash());
                    System.out.println("M TYPE    "+dataResponse.getMessageType());
                    singleTon.setSubjectList(dataResponse.getSubjectList());
                    System.out.println("Subject List : "+singleTon.getSubjectList().toString());
                    
                }else if(mType.equals("SubjectDataResponse")){
                    System.out.println("AAAAAAAAAAAsleepin Data Response");
                    SubjectDataResponse subjectDataResponse =(SubjectDataResponse) response;
                    System.out.println(""+subjectDataResponse.getHash());
                    System.out.println(""+subjectDataResponse.getMessageType());
                    singleTon.setUnitList(subjectDataResponse.getUnitList());
                    System.out.println("Subject List : "+singleTon.getUnitList().toString());
                    
                }else if(mType.equals("ChapterDataResponse")){
                    System.out.println("chapter Data Response");
                    ChapterResponse chapterResponse =(ChapterResponse) response;
                    System.out.println(""+chapterResponse.getHash());
                    System.out.println(""+chapterResponse.getMessageType());
                    singleTon.setChapterList(chapterResponse.getChapterList());
                    System.out.println("Subject Listttttttttttttt : "+singleTon.getChapterList().toString());
                    
                }else if(mType.equals("LessonDataResponse")){
                    System.out.println("Lesson Data Response");
                    LessonResponse lessonResponse =(LessonResponse) response;
                    System.out.println(""+lessonResponse.getHash());
                    System.out.println(""+lessonResponse.getMessageType());
                    singleTon.setLessonList(lessonResponse.getLessonList());
                    System.out.println("Subject List : "+singleTon.getLessonList().toString());
                    
                }else if(mType.equals("CreateUserResponse")){
                    System.out.println("CreateUserResponse");
                    CreateUserResponse createUserResponse = (CreateUserResponse) response;
                    singleTon.setCountryList(createUserResponse.getCountryList());
                    singleTon.setStateList(createUserResponse.getStateList());
                    singleTon.setSchoolCategaryList(createUserResponse.getSchoolCategoryList());
                    System.out.println("Subject List : "+singleTon.getCountryList().toString());
                    System.out.println("Subject List : "+singleTon.getStateList().toString());
                    System.out.println("Subject List : "+singleTon.getSchoolCategaryList().toString());
                    
                }else if(mType.equals("CreateUserSubmitResponse")){
                    System.out.println("CreateUserSubmitResponse");
                    CreateUserSubmitResponse createUserSubmitResponse = (CreateUserSubmitResponse) response;
                    singleTon.setCreateUserResponse(createUserSubmitResponse.isResponse());
                    
                }else if(mType.equals("StateResponse")){
                    System.out.println("StateResponse");
                    StateResponse stateResponse = (StateResponse) response;
                    singleTon.setStateList(stateResponse.getStateList());
                    System.out.println("Subject List : "+singleTon.getCountryList().toString());
                    System.out.println("Subject List : "+singleTon.getStateList().toString());
                    
                }else if(mType.equals("SchoolDistrictResponse")){
                    System.out.println("SchoolDistrictResponse");
                    SchoolDistrictResponse schoolDistrictResponse = (SchoolDistrictResponse) response;
                    singleTon.setSchoolDistrictList(schoolDistrictResponse.getSchoolDistrictList());
                    System.out.println("Subject List : "+singleTon.getSchoolDistrictList().toString());
                    
                }else if(mType.equals("SchoolResponse")){
                    System.out.println("SchoolResponse");
                    SchoolResponse schoolResponse = (SchoolResponse) response;
                    singleTon.setSchoolList(schoolResponse.getSchoolList());
                    System.out.println("Subject List : "+singleTon.getSchoolList().toString());
                    
                }else if(mType.equals("UserVerificationResponse")){
                    System.out.println("UserVerificationResponse");
                    UserNameVerificationResponse userNameVerificationResponse = (UserNameVerificationResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUserNameVerification(userNameVerificationResponse.isVerify());
                    singleTon.setResponse(true);
                    System.out.println("Subject List : "+singleTon.isUserNameVerification());

                }else if(mType.equals("ChangePasswordResponse")){
                    System.out.println("ChangePasswordResponse");
                    ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) response;
                    singleTon.setChangePasswordResponse(changePasswordResponse.getChangePasswordResponse());
                    System.out.println("Subject List : "+singleTon.getChangePasswordResponse());
                    
                }else if(mType.equals("UpdatePasswordResponse")){
                    System.out.println("UpdatePasswordResponse");
                    UpdatePasswordResponse updatePasswordResponse = (UpdatePasswordResponse) response;
                    singleTon.setUpdatePasswordResponse(updatePasswordResponse.getUpdatePasswordResponse());
                    System.out.println("Subject List : "+singleTon.getUpdatePasswordResponse());
                    
                }else if(mType.equals("ClassDayScheduleResponse")){
                    System.out.println("ClassDayScheduleResponse");
                    ClassDayScheduleResponse classDayScheduleResponse = (ClassDayScheduleResponse) response;
                    singleTon.setScheduleList(classDayScheduleResponse.getScheduleList());
                    System.out.println("Subject List : "+singleTon.getScheduleList().toString());
                    
                }else if(mType.equals("MyLeavesResponse")){
                    System.out.println("MyLeavesResponse");
                    MyLeavesResponse myLeavesResponse = (MyLeavesResponse) response;
                    singleTon.setMyLeavesList(myLeavesResponse.getMyLeaves());
                    
                }else if(mType.equals("SchoolHolidayResponse")){
                    System.out.println("SchoolHolidayResponse");
                    SchoolHolidayResponse schoolHolidayResponse = (SchoolHolidayResponse) response;
                    singleTon.setSchoolHolidayList(schoolHolidayResponse.getSchoolHolidays());
                    
                }else if(mType.equals("UpdateClassDayScheduleResponse")){
                    System.out.println("UpdateClassDayScheduleResponse");
                    UpdateClassDayScheduleResponse classDayScheduleResponse = (UpdateClassDayScheduleResponse) response;
                    singleTon.setUpdateClassDayScheduleResponse(classDayScheduleResponse.isResponse());
                    
                }else if(mType.equals("DeleteLessonPlanResponse")){
                    System.out.println("DeleteLessonPlanResponse");
                    DeleteLessonPlanResponse deleteLessonPlanResponse = (DeleteLessonPlanResponse) response;
                    singleTon.setResponse(deleteLessonPlanResponse.isResponse());
                    singleTon.setLessonPlanResponse(true);
                    
                }else if(mType.equals("LessonPlanningResponse")){
                    System.out.println("LessonPlanningResponse");
                    LessonPlanningResponse lessonPlanningResponse = (LessonPlanningResponse) response;
                    singleTon.setErrorResponse(lessonPlanningResponse.isResponse());
                    singleTon.setLessonPlanningList(null);
                    singleTon.setLessonPlanningList(lessonPlanningResponse.getLessonPlanningList());
                    singleTon.setLessonPlanResponse(true);
                    
                }else if(mType.equals("LessonDateResponse")){
                    LessonDateResponse lessonDateResponse = (LessonDateResponse) response;
                    singleTon.setLessonPlanningList(null);
                    singleTon.setResponse(true);
                    singleTon.setLessonPlanningList(lessonDateResponse.getLessonPlanningList());
                    
                }else if(mType.equals("MonthlyScheduleResponse")){
                    System.out.println("MonthlyScheduleResponse");
                    MonthlyScheduleResponse monthlyScheduleResponse = (MonthlyScheduleResponse) response;
                    singleTon.setLessonPlanningList(null);
                    singleTon.setLessonPlanningList(monthlyScheduleResponse.getLessonPlannings());
                    singleTon.setLessonPlanResponse(true);
                    
                }else if(mType.equals("WeeklyScheduleResponse")){
                    System.out.println("WeeklyScheduleResponse");
                    WeeklyScheduleResponse weeklyScheduleResponse = (WeeklyScheduleResponse) response;
                    singleTon.setLessonPlanningList(null);
                    singleTon.setLessonPlanningList(weeklyScheduleResponse.getLessonPlannings());
                    singleTon.setLessonPlanResponse(true);

                }else if(mType.equals("SearchDataResponse")){
                    System.out.println("SearchDataResponse");
                    SearchResponse searchResponse = (SearchResponse) response;
                    singleTon.setSearchResponseDatas(searchResponse.getSearchResponseDatas());
                    singleTon.setLessonPlanResponse(true);
                    singleTon.setSearchSubjectList(searchResponse.getSubject());
                    singleTon.setSearchGradeList(searchResponse.getGrade());
                    singleTon.setSearchCurriculumList(searchResponse.getCurriculum());

                }else if(mType.equals("SearchSortedDataRequest")){
                    System.out.println("SearchSortedDataRequest");
                    SearchSortedResponse searchSortedResponse = (SearchSortedResponse) response;
                    singleTon.setSearchResponseDatas(searchSortedResponse.getSearchResponseDatas());
                    singleTon.setLessonPlanResponse(true);
                    singleTon.setSearchSubjectList(searchSortedResponse.getSubject());
                    singleTon.setSearchGradeList(searchSortedResponse.getGrade());
                    singleTon.setSearchCurriculumList(searchSortedResponse.getCurriculum());

                }else if(mType.equals("AddSchoolHolidayResponse")){
                    AddSchoolHolidayResponse addSchoolHolidayResponse = (AddSchoolHolidayResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    
                    if(addSchoolHolidayResponse.isInterrupt()){
                        singleTon.setSubject(addSchoolHolidayResponse.getSubject());
                        singleTon.setChapter(addSchoolHolidayResponse.getChapter());
                        singleTon.setLesson(addSchoolHolidayResponse.getLesson());
                        singleTon.setTopic(addSchoolHolidayResponse.getTopic());
                    }else
                        singleTon.setHolidayId(addSchoolHolidayResponse.getId());
                    
                    singleTon.setInturrept(addSchoolHolidayResponse.isInterrupt());
                    singleTon.setResponse(true);
                    
                }else if(mType.equals("AddLeaveResponse")){
                    AddLeaveResponse addLeaveResponse = (AddLeaveResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    
                    if(addLeaveResponse.isInterrupt()){
                        singleTon.setSubject(addLeaveResponse.getSubject());
                        singleTon.setChapter(addLeaveResponse.getChapter());
                        singleTon.setLesson(addLeaveResponse.getLesson());
                        singleTon.setTopic(addLeaveResponse.getTopic());
                    }else
                        singleTon.setId(addLeaveResponse.getId());
                    
                    singleTon.setInturrept(addLeaveResponse.isInterrupt());
                    singleTon.setResponse(true);
                    
                }else if(mType.equals("UpdateLeaveResponse")){
                    UpdateLeaveResponse updateLeaveResponse = (UpdateLeaveResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUpdateResponse(updateLeaveResponse.isResponse());
                    
                    if(updateLeaveResponse.isInterrupt()){
                        singleTon.setSubject(updateLeaveResponse.getSubject());
                        singleTon.setChapter(updateLeaveResponse.getChapter());
                        singleTon.setLesson(updateLeaveResponse.getLesson());
                        singleTon.setTopic(updateLeaveResponse.getTopic());
                    }
                    singleTon.setInturrept(updateLeaveResponse.isInterrupt());
                    singleTon.setResponse(true);
                    
                }else if(mType.equals("UpdateSchoolHolidayResponse")){
                    UpdateSchoolHolidayResponse updateSchoolHolidayResponse = (UpdateSchoolHolidayResponse) response;
                    System.out.println("Subject List : "+updateSchoolHolidayResponse.isResponse());
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUpdateResponse(updateSchoolHolidayResponse.isResponse());
                    if(updateSchoolHolidayResponse.isInterrupt()){
                        singleTon.setSubject(updateSchoolHolidayResponse.getSubject());
                        singleTon.setChapter(updateSchoolHolidayResponse.getChapter());
                        singleTon.setLesson(updateSchoolHolidayResponse.getLesson());
                        singleTon.setTopic(updateSchoolHolidayResponse.getTopic());
                    }
                    singleTon.setInturrept(updateSchoolHolidayResponse.isInterrupt());
                    singleTon.setResponse(true);
                    
                }else if(mType.equals("UpdateUserResponse")){
                    System.out.println("UpdateUserResponse");
                    UpdateUserResponse updateUserResponse = (UpdateUserResponse) response;
                    
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUpdateUserList(updateUserResponse.getUpdateUsers());
                    System.out.println("Subject List : "+updateUserResponse.getUpdateUsers());

//                }else if(mType.equals("UserNameConfirmationResponse")){
//                    System.out.println("UserNameConfirmationResponse");
//                    UserNameConfirmationResponse userNameConfirmationResponse = (UserNameConfirmationResponse) response;
//                    
//                    singleTon = SingleTon.getSingleTon();
//                    System.out.println("Subject List : "+userNameConfirmationResponse.isUserNameVarify());

                }else if(mType.equals("UpdateUserInfoResponse")){
                    System.out.println("UpdateUserInfoResponse");
                    UpdateUserInfoResponse updateUserInfoResponse = (UpdateUserInfoResponse) response;
                    
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUpdateUserInfo(updateUserInfoResponse.getUpdateUserInfo());
                    singleTon.setCountryList(updateUserInfoResponse.getCountryList());
                    singleTon.setStateList(updateUserInfoResponse.getStateList());
                    singleTon.setSchoolDistrictList(updateUserInfoResponse.getDistrictList());
                    singleTon.setSchoolList(updateUserInfoResponse.getSchoolList());
                    singleTon.setSchoolCategaryList(updateUserInfoResponse.getCategoryList());
                    
                    System.out.println("Subject List : "+updateUserInfoResponse.getUpdateUserInfo());
                    
                }else if(mType.equals("AddNewLessonSubjectResponse")){
                    System.out.println("AddNewLessonSubjectResponse");
                    AddNewLessonSubjectDataResponse addNewLessonSubjectDataResponse = (AddNewLessonSubjectDataResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    
                    singleTon.setAddNewLessonSubjectList(addNewLessonSubjectDataResponse.getSubjectList());
                    
                    System.out.println("Subject List : "+addNewLessonSubjectDataResponse.getSubjectList());

                }else if(mType.equals("AddNewLessonChapterDataResponse")){
                    System.out.println("AddNewLessonChapterDataResponse");
                    AddNewLessonChapterDataResponse addNewLessonChapterDataResponse = (AddNewLessonChapterDataResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    
                    singleTon.setAddNewLessonChapterList(addNewLessonChapterDataResponse.getChapterList());
                    singleTon.setScheduleList(addNewLessonChapterDataResponse.getClassDayScheduleList());
                    singleTon.setDateList(addNewLessonChapterDataResponse.getDateList());
                    
                    System.out.println("Subject List : "+addNewLessonChapterDataResponse.getChapterList());

                }else if(mType.equals("AddNewLessonLessonDataResponse")){
                    System.out.println("AddNewLessonLessonDataResponse");
                    AddNewLessonLessonDataResponse addNewLessonLessonDataResponse = (AddNewLessonLessonDataResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    
                    singleTon.setAddNewLessonLessonList(addNewLessonLessonDataResponse.getLessonList());
                    
                    System.out.println("Subject List : "+addNewLessonLessonDataResponse.getLessonList());

                }else if(mType.equals("AddNewLessonTopicDataResponse")){
                    System.out.println("AddNewLessonTopicDataResponse");
                    AddNewLessonTopicDataResponse addNewLessonTopicDataResponse = (AddNewLessonTopicDataResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setLeaveDateList(addNewLessonTopicDataResponse.getLeaveDates());
                    singleTon.setResponse(true);
                    singleTon.setAddNewLessonTopicList(addNewLessonTopicDataResponse.getTopicList());
                    
                    System.out.println("Subject List : "+addNewLessonTopicDataResponse.getTopicList());

                }else if(mType.equals("UpdateLessonPlanResponse")){
                    System.out.println("UpdateLessonPlanResponse");
                    UpdateLessonPlanResponse updateLessonPlanResponse = (UpdateLessonPlanResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setResponse(true);
                    singleTon.setUpdateResponse(updateLessonPlanResponse.isResponse());
                    System.out.println("Subject List : "+updateLessonPlanResponse.isResponse());
                    
                }else if(mType.equals("LogOutResponse")){
                    System.out.println("LogOutResponse");
                    socket.close();
                    LogOutRespnse logOutRespnse = (LogOutRespnse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setResponse(true);
                    singleTon.setLogoutResponse(logOutRespnse.getResponse());
                    singleTon.removeSingleTon();
                    mp4Flage = false;
                    mp43dFlage = false;
                    break;
                    
                }else if(mType.equals("CreateNewLessonResponse")){
                    System.out.println("CreateNewLessonResponse");
                    CreateNewLessonResponse createNewLessonResponse = (CreateNewLessonResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setResponse(true);
                    singleTon.setDateStringList(createNewLessonResponse.getDateList());
                    System.out.println("Subject List : "+singleTon.getDateStringList().toString());
                    System.out.println("Subject List11111 : "+createNewLessonResponse.getDateList().toString());

                }else if(mType.equals("LeaveDateResponse")){
                    System.out.println("LeaveDateResponse");
                    LeaveDateResponse leaveDateResponse = (LeaveDateResponse) response;
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setLeaveDateList(leaveDateResponse.getLeaveDateList());
                    singleTon.setClassDateList(leaveDateResponse.getClassDateList());

                }else if(mType.equals("MediaDataResponse")){
                    System.out.println("sleepin Data Response");
                    MediaDataResponse dataResponse =(MediaDataResponse) response;
                    System.out.println(""+dataResponse.getHash());
                    System.out.println(""+dataResponse.getMessageType());
                    System.out.println(""+dataResponse.getPath());
                    Path = dataResponse.getPath();//.substring(dataResponse.getPath().indexOf('.')+1);

                }else if(mType.equalsIgnoreCase("MediaDatajpg")){
                    
                    ImageThread imageThread = ImageThread.getObject();
                    BlockingQueue<Data> queue = imageThread.getQueueData();
                    queue.add((Data)response);
                    
                }else if(mType.equals("MediaDatapdf")){
                
                    System.out.println("Media Data");
                    PDFThread pDFThread = PDFThread.getObject();
                    BlockingQueue<Data> queue = pDFThread.getQueueData();
                    queue.add((Data)response);
                    System.out.println("image prints");
                    
                }else if(mType.equalsIgnoreCase("123MediaDatahtml")){
         
                    int store = 0;
                    Data data = (Data)response;
                    long size = data.getSequenceNumber()+10240;
                    byte[] bs =new byte[1024];
                    byte[] newBs = new byte[(int)size];
                    int total = 0;
                    int i=1;
                    InputStream is;
                    do{
                        System.out.println("do");
                        i++;
                        data = (Data)response;
                        bs = data.getBs();
                        total += bs.length;
                        is = new ByteArrayInputStream(bs);

                        for(byte b : bs)
                            newBs[store++] = b;
                    
                        if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))) {
                            System.out.println("break");
                            break;
                        }
                    
                        response = null;
                        ois.skipBytes(total);
                    }while((response = (MessageResponse)ois.readObject())!=null);

                    KeyGeneration keyGeneration = new KeyGeneration();

                    Decryption decryption = new Decryption();
                    ByteArrayInputStream fileInputStream = new ByteArrayInputStream(newBs);
                    newBs = decryption.decrypt(newBs, fileInputStream, keyGeneration.key);
                    
                    clientSocket.setWeb(new String(newBs, 0, newBs.length));
                    System.out.println("image prints");
                    
                }else if(mType.equalsIgnoreCase("MediaDatamp4")){
                    DecryptionQueue decryptionQueue = DecryptionQueue.getDecryption();
                    Data data = (Data)response;
                    if(!mp4Flage){
                        data = (Data)response;
                        decryptionQueue = DecryptionQueue.getDecryption();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        Thread decryptionQueueThread = new Thread(decryptionQueue);
                        decryptionQueueThread.setDaemon(true);
                        decryptionQueueThread.start();
                        byte[] bs =new byte[127];
                        data = (Data)response;
                        bs = data.getBs();
                        data = (Data)response;
                        response = (MessageResponse)ois.readObject();
                    }byte[] bs = new byte[1024];
                    int i =0;
                    do{
                        data = (Data)response;
                        bs = data.getBs();
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))||bs.length==0){
                            queue.add("End".getBytes());
                            decryptionQueue.setEnd(true);
                            mp4Flage = false;
                            break;
                        }
                        if((new String(bs,0,bs.length).trim().endsWith("continue"))||bs.length==0){
                            mp4Flage = true;
                            break;
                        }
                        queue.add(bs);
                        
                        singleTon = SingleTon.getSingleTon();
                        if(i==1000 && singleTon.isMp4player() && singleTon.isMp4()){
                            Queue<MessageRequest> queue1 = clientSocket.getRequestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("mp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            queue1.add(continueRequest);
                            i=0;
                        }
                        if(i==1000 && singleTon.isMp4player() && !singleTon.isMp4()){
                            Queue<MessageRequest> queue1 = clientSocket.getRequestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("3dmp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            queue1.add(continueRequest);
                            i=0;
                        }
                        i++;
                    }while((response = (MessageResponse)ois.readObject())!=null);
                    if(!mp4Flage){
                        System.out.println("mp4flage");
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        queue.add("End".getBytes());
                        queue.add(bs);
                    }
                    System.out.println("image prints");
                    
                }else if(mType.equalsIgnoreCase("MediaData3dmp4")){
                    DecryptionQueue3d decryptionQueue = DecryptionQueue3d.getDecryption();
                    Data data = (Data)response;
                    if(!mp43dFlage){
                        decryptionQueue = DecryptionQueue3d.getDecryption();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        Thread decryptionQueueThread = new Thread(decryptionQueue);
                        decryptionQueueThread.setDaemon(true);
                        decryptionQueueThread.start();
                        byte[] bs =new byte[127];
                        data = (Data)response;
                        bs = data.getBs();
                        decryptionQueue.setSize(data.getSequenceNumber());
                        data = (Data)response;
                        response = (MessageResponse)ois.readObject();
                    }byte[] bs = new byte[1024];
                    int i =0;
                    do{
                        data = (Data)response;
                        bs = data.getBs();
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END"))||bs.length==0){
                            decryptionQueue.setEnd(true);
                            queue.add("End".getBytes());
                            mp43dFlage = false;
                            break;
                        }
                        if((new String(bs,0,bs.length).trim().endsWith("continue"))||bs.length==0){
                            mp43dFlage = true;
                            break;
                        }
                        queue.add(bs);
                        
                        singleTon = SingleTon.getSingleTon();
                        if(i==1000 && singleTon.isMp43dplayer() && !singleTon.isMp4()){
                            Queue<MessageRequest> queue1 = clientSocket.getRequestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("3dmp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            queue1.add(continueRequest);
                        }
                        if(i==1000 && singleTon.isMp43dplayer() && singleTon.isMp4()){
                            Queue<MessageRequest> queue1 = clientSocket.getRequestQueue();
                            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
                            continueRequest.setHash(clientSocket.getHash());
                            continueRequest.setFileType("mp4");
                            continueRequest.setMessageType("MediaDataContinueRequest");
                            queue1.add(continueRequest);
                        }
                        i++;
                    }while((response = (MessageResponse)ois.readObject())!=null);
                    if(!mp43dFlage){
                        BlockingQueue<byte[]> queue = decryptionQueue.getEncryptedData();
                        queue.add("End".getBytes());
                        queue.add(bs);
                    }
                    System.out.println("image prints");
                    
                }else if(mType.equalsIgnoreCase("MediaDataunity3d")){
//                    
//                    Unity3dThread pDFThread = Unity3dThread.getObject();
//                    BlockingQueue<Data> queue = pDFThread.getQueueData();
//                    queue.add((Data)response);
//                    
                }else if(mType.equalsIgnoreCase("MediaDataswf")){
                    
//                    SWFThread sWFThread = SWFThread.getObject();
//                    BlockingQueue<Data> queue = sWFThread.getQueueData();
//                    queue.add((Data)response);                    
//                    System.out.println("image prints");
                }
                singleTon = SingleTon.getSingleTon();   
                singleTon.setResponse(true);
            }            
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("IOException");
            Logger.getLogger(ClientRead.class.getName()).log(Level.SEVERE, null, ex);
        }catch(NullPointerException npe){
            System.out.println("NULL POINTER EXCEPTION");
            PrintWriter out = null;
            try {
                out = new PrintWriter(socket.getOutputStream(),true);
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                boolean flag = true;
                while(flag)
                {
                    str = in.readLine();
                    if(!str.equalsIgnoreCase("END"))
                    {
                        out.println(str);
                    }
                    else
                    {
                        flag=false;
                        out.println("END");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ClientRead.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Inner IOException");
            } finally {
                out.close();
            }
        }
        Thread.currentThread().stop();
    }
    
    public Image getJavaFXImage(byte[] rawPixels, int width, int height) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write((RenderedImage) createBufferedImage(rawPixels, width, height), "png", out);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ClientRead.class.getName()).log(Level.SEVERE, null, ex);
        }
        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        return new javafx.scene.image.Image(in);
    }

    private BufferedImage createBufferedImage(byte[] pixels, int width, int height) {
        SampleModel sm = getIndexSampleModel(width, height);
        DataBuffer db = new DataBufferByte(pixels, width*height, 0);
        WritableRaster raster = Raster.createWritableRaster(sm, db, null);
        IndexColorModel cm = getDefaultColorModel();
        BufferedImage image = new BufferedImage(cm, raster, false, null);
        return image;
    }

    private SampleModel getIndexSampleModel(int width, int height) {
        IndexColorModel icm = getDefaultColorModel();
        WritableRaster wr = icm.createCompatibleWritableRaster(1, 1);
        SampleModel sampleModel = wr.getSampleModel();
        sampleModel = sampleModel.createCompatibleSampleModel(width, height);
        return sampleModel;
    }

    private IndexColorModel getDefaultColorModel() {
        byte[] r = new byte[256];
        byte[] g = new byte[256];
        byte[] b = new byte[256];
        for(int i=0; i<256; i++) {
                r[i]=(byte)i;
                g[i]=(byte)i;
                b[i]=(byte)i;
        }
        IndexColorModel defaultColorModel = new IndexColorModel(8, 256, r, g, b);
        return defaultColorModel;
    }

    public void findMinAndMax(short[] pixels, int width, int height) {
        int size = width*height;
        int value;
        int min = 65535;
        int max = 0;
        for (int i=0; i<size; i++) {
                value = pixels[i]&0xffff;
                if (value<min)
                        min = value;
                if (value>max)
                        max = value;
        }
    }

    private void createFile() {
        File file = new File("temp\\temp.html");
        String content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"	<head>\n" +
"		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
"		<title>Unity Web Player | Measuring the Mass and Weight of Matter</title>\n" +
"		<script type='text/javascript' src='https://ssl-webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/jquery.min.js'></script>\n" +
"		<script type=\"text/javascript\">\n" +
"		<!--\n" +
"		var unityObjectUrl = \"http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js\";\n" +
"		if (document.location.protocol == 'https:')\n" +
"			unityObjectUrl = unityObjectUrl.replace(\"http://\", \"https://ssl-\");\n" +
"		document.write('<script type=\"text\\/javascript\" src=\"' + unityObjectUrl + '\"><\\/script>');\n" +
"		-->\n" +
"		</script>\n" +
"		<script type=\"text/javascript\">\n" +
"		<!--\n" +
"			var config = {\n" +
"				width: 1000,\n" +
"				height: 560,\n" +
"				params: { enableDebugging:\"0\" }\n" +
"\n" +
"			};\n" +
"			var u = new UnityObject2(config);\n" +
"\n" +
"			jQuery(function() {\n" +
"\n" +
"				var $missingScreen = jQuery(\"#unityPlayer\").find(\".missing\");\n" +
"				var $brokenScreen = jQuery(\"#unityPlayer\").find(\".broken\");\n" +
"				$missingScreen.hide();\n" +
"				$brokenScreen.hide();\n" +
"\n" +
"				u.observeProgress(function (progress) {\n" +
"					switch(progress.pluginStatus) {\n" +
"						case \"broken\":\n" +
"							$brokenScreen.find(\"a\").click(function (e) {\n" +
"								e.stopPropagation();\n" +
"								e.preventDefault();\n" +
"								u.installPlugin();\n" +
"								return false;\n" +
"							});\n" +
"							$brokenScreen.show();\n" +
"						break;\n" +
"						case \"missing\":\n" +
"							$missingScreen.find(\"a\").click(function (e) {\n" +
"								e.stopPropagation();\n" +
"								e.preventDefault();\n" +
"								u.installPlugin();\n" +
"								return false;\n" +
"							});\n" +
"							$missingScreen.show();\n" +
"						break;\n" +
"						case \"installed\":\n" +
"							$missingScreen.remove();\n" +
"						break;\n" +
"						case \"first\":\n" +
"						break;\n" +
"					}\n" +
"				});\n" +
"				u.initPlugin(jQuery(\"#unityPlayer\")[0], \"temp.unity3d\");\n" +
"			});\n" +
"		-->\n" +
"		</script>\n" +
"		<style type=\"text/css\">\n" +
"		<!--\n" +
"		body {\n" +
"			font-family: Helvetica, Verdana, Arial, sans-serif;\n" +
"			background-color: white;\n" +
"			color: black;\n" +
"			text-align: center;\n" +
"		}\n" +
"		a:link, a:visited {\n" +
"			color: #000;\n" +
"		}\n" +
"		a:active, a:hover {\n" +
"			color: #666;\n" +
"		}\n" +
"		p.header {\n" +
"			font-size: small;\n" +
"		}\n" +
"		p.header span {\n" +
"			font-weight: bold;\n" +
"		}\n" +
"		p.footer {\n" +
"			font-size: x-small;\n" +
"		}\n" +
"		div.content {\n" +
"			margin: auto;\n" +
"			width: 1000px;\n" +
"		}\n" +
"		div.broken,\n" +
"		div.missing {\n" +
"			margin: auto;\n" +
"			position: relative;\n" +
"			top: 50%;\n" +
"			width: 193px;\n" +
"		}\n" +
"		div.broken a,\n" +
"		div.missing a {\n" +
"			height: 63px;\n" +
"			position: relative;\n" +
"			top: -31px;\n" +
"		}\n" +
"		div.broken img,\n" +
"		div.missing img {\n" +
"			border-width: 0px;\n" +
"		}\n" +
"		div.broken {\n" +
"			display: none;\n" +
"		}\n" +
"		div#unityPlayer {\n" +
"			cursor: default;\n" +
"			height: 560px;\n" +
"			width: 1000px;\n" +
"		}\n" +
"		-->\n" +
"		</style>\n" +
"	</head>\n" +
"	<body>\n" +
"		<p class=\"header\"><span>Unity Web Player | </span>Measuring the Mass and Weight of Matter</p>\n" +
"		<div class=\"content\">\n" +
"			<div id=\"unityPlayer\">\n" +
"				<div class=\"missing\">\n" +
"					<a href=\"http://unity3d.com/webplayer/\" title=\"Unity Web Player. Install now!\">\n" +
"						<img alt=\"Unity Web Player. Install now!\" src=\"http://webplayer.unity3d.com/installation/getunity.png\" width=\"193\" height=\"63\" />\n" +
"					</a>\n" +
"				</div>\n" +
"				<div class=\"broken\">\n" +
"					<a href=\"http://unity3d.com/webplayer/\" title=\"Unity Web Player. Install now! Restart your browser after install.\">\n" +
"						<img alt=\"Unity Web Player. Install now! Restart your browser after install.\" src=\"http://webplayer.unity3d.com/installation/getunityrestart.png\" width=\"193\" height=\"63\" />\n" +
"					</a>\n" +
"				</div>\n" +
"			</div>\n" +
"		</div>\n" +
"		<p class=\"footer\">&laquo; created with <a href=\"http://unity3d.com/unity/\" title=\"Go to unity3d.com\">Unity</a> &raquo;</p>\n" +
"	</body>\n" +
"</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

                // if file doesn't exists, then create it
                if (!file.exists()) {
                        file.createNewFile();
                }

                // get the content in bytes
                byte[] contentInBytes = content.getBytes();

                fop.write(contentInBytes);
                fop.flush();
                fop.close();

                System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }

    private void createFileSwf() {
        File file = new File("temp\\tempswf.html");
        String content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "	<head>\n" +
        "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                        "	</head>\n" +
        "	<body>\n" +
        "		<p class=\"header\"><span>Unity Web Player | </span>Measuring the Mass and Weight of Matter</p>\n" +
        "		<div class=\"content\">\n" +
        "			<div id=\"unityPlayer\">\n"
                        + "<object width=\"600\" height=\"700\" data=\"temp.swf\"></object>" +

                                "</div>\n" +
        "		</div>\n" +
        "	</body>\n" +
        "</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

                // if file doesn't exists, then create it
                if (!file.exists()) {
                        file.createNewFile();
                }

                // get the content in bytes
                byte[] contentInBytes = content.getBytes();

                fop.write(contentInBytes);
                fop.flush();
                fop.close();

                System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
    
    private void createUnity3d(byte[] newBs){
        File f = null;
        File f1 = null;
        FileOutputStream fileOutputStream1 = null;
        BufferedOutputStream stream;

        f = new File("temp");
        f.mkdir();
        f1 = new File(f,"temp.unity3d");
        try {
            
            if(!f1.exists()){
                fileOutputStream1 = new FileOutputStream(f1);
            }else{
                fileOutputStream1 = new FileOutputStream(f1, true);
            }stream = new BufferedOutputStream(fileOutputStream1);
                
            if(newBs.length!=1024)
                System.out.println("Length "+newBs.length);
            
            stream.write(newBs,0, newBs.length);
                
            fileOutputStream1.flush();
            stream.flush();
                
            stream.close();
            
        } catch (IOException ex) {
            System.out.println("Exception");
            ex.printStackTrace();
            Logger.getLogger(DecryptionQueueUnity3D.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                if(fileOutputStream1!=null){
                    fileOutputStream1.close();
                };
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void createSWF(byte[] newBs){
        File f = null;
        File f1 = null;
        FileOutputStream fileOutputStream1 = null;
        BufferedOutputStream stream;

        byte[] bs;
        f = new File("temp");
        f.mkdir();
        f1 = new File(f,"temp.swf");
        try {
            if(!f1.exists()){
                fileOutputStream1 = new FileOutputStream(f1);
            }else{
                fileOutputStream1 = new FileOutputStream(f1, true);
            }stream = new BufferedOutputStream(fileOutputStream1);

            stream.write(newBs,0, newBs.length);

            fileOutputStream1.flush();
            stream.flush();

            stream.close();

        } catch (IOException ex) {
            System.out.println("Exception");
            ex.printStackTrace();
            Logger.getLogger(DecryptionQueueSwf.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                if(fileOutputStream1!=null){
                    fileOutputStream1.close();
                };
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}