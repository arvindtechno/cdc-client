/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 *
 * @author Tiwari
 */
public class ClientWriteMp4 implements Runnable{
    
    private Socket socket;
    
    ObjectOutputStream oout;
    ClientSocket clientSocket;
    SingleTon singleTon;
    Socket mp4socket = null;
    public Queue<MessageRequest> requestQueue;    

    public ClientWriteMp4(Socket socket, MessageRequest messageRequest) {
        this.socket=socket;
        requestQueue = new LinkedList<>();
        ObjectOutputStream oout = null;
        requestQueue.add(messageRequest);
    }

    public Queue<MessageRequest> getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(MessageRequest messageRequest) {
        this.requestQueue.add(messageRequest);
    }
    
    @Override
    public void run() {
        MessageRequest request;
//        ClientSocket cs =ClientSocket.getClientSocket();
//        ClientSocket.setBool(true);
        
        try {
            clientSocket = ClientSocket.getClientSocket();
            oout = new ObjectOutputStream(socket.getOutputStream());
            while(true){
//                Queue<MessageRequest> queue = requestQueue;
                while(requestQueue.isEmpty()){
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientWriteMp4.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                singleTon = SingleTon.getSingleTon();
                singleTon.setResponse(false);
                request = requestQueue.poll();
                oout.writeObject(request);
                System.out.println("ClientWrite");
                oout.flush();
                System.out.println("flushed");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientWriteMp4.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
}