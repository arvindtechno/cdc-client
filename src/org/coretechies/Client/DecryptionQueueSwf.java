/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.coretechies.forms.SubjController;
import org.coretechies.forms.controller.SubjectController;

/**
 *
 * @author Tiwari
 */
public class DecryptionQueueSwf extends Observable implements Runnable{

    BlockingQueue<byte[]> encryptedData;
    
    static DecryptionQueueSwf decryption;
    
    static{
        decryption=null;
    }

    private DecryptionQueueSwf() {
       encryptedData = new LinkedBlockingQueue<byte[]>() ;
    }
    
    public static DecryptionQueueSwf getDecryption(){
        if(decryption == null){
            decryption=new DecryptionQueueSwf();
        }
        return decryption;
    }

    public BlockingQueue<byte[]> getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(BlockingQueue<byte[]> encryptedData) {
        this.encryptedData = encryptedData;
    }


    @Override
    public void run() {
        try {
            File f = null;
            File f1 = null;
            FileOutputStream fileOutputStream1 = null;
            BufferedOutputStream stream;
            
            byte[] bs;
//            f = new File("temp");
//            f.mkdir();
//            f1 = new File(f,"temp.swf");
            f = File.createTempFile("tmp", ".txt");
            
            // prints absolute path
            System.out.println("File path: "+f.getAbsolutePath());
            Runtime.getRuntime().exec("attrib +H "+f);
            try {
                while(true){

                    while(true){
                        ClientSocket clientSocket = ClientSocket.getClientSocket();
//                    System.out.print("While");
//                    synchronized(clientSocket){
                        boolean isPlaying = clientSocket.isIsPlaying();
                        if(!encryptedData.isEmpty())// && !isPlaying)
                            break;
//                    }
                    }
//                SubjectController.setMp(false);
                    
                    byte[] newBs = getEncryptedData().poll();
                    if(new String(newBs,0,newBs.length).equalsIgnoreCase("End")) break;
                    if(!f1.exists()){
                        fileOutputStream1 = new FileOutputStream(f1);
                    }else{
                        fileOutputStream1 = new FileOutputStream(f1, true);
                    }stream = new BufferedOutputStream(fileOutputStream1);
                    
                    stream.write(newBs,0, newBs.length-1);
                    
                    fileOutputStream1.flush();
                    stream.flush();
                    
                    stream.close();
                    
//                    ClientSocket clientSocket = ClientSocket.getClientSocket();
//                synchronized(clientSocket){
//                    clientSocket.setIsPlaying(true);
//                }
//                System.out.print("Play");
//                setChanged();
//                notifyObservers();
//                SubjectController.setMp(true);
                }
            } catch (IOException ex) {
                System.out.println("Exception");
                ex.printStackTrace();
                Logger.getLogger(DecryptionQueueSwf.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                try {
                    if(fileOutputStream1!=null){
                        fileOutputStream1.close();
                    };
                } catch (IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
//                    if(f1.exists())
//                        f1.delete();
//                    if(f.exists())
//                        f.delete();
            }
        } catch (IOException ex) {
            Logger.getLogger(DecryptionQueueSwf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
