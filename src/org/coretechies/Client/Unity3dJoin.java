/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import org.cef.OS;
import tests.simple.MainFrame1;

/**
 *
 * @author Tiwari
 */
public class Unity3dJoin extends JFrame implements Runnable{
    
    private BlockingQueue<FileNumber> fileNumbers;
    static Unity3dJoin unity3dJoin;
    int height = 0;
    int width = 0;
    private FileNumber[] fileNumber = new FileNumber[20];
    MainFrame1 mainFrame;
    boolean bool=false;

    public FileNumber[] getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(FileNumber fileNumber, int num) {
        this.fileNumber[num] = fileNumber;
    }

    static{
        unity3dJoin = null;
    }
    
    private Unity3dJoin() {
        fileNumbers = new LinkedBlockingQueue<>();
    }
    
    public static Unity3dJoin getUnity3dJoin(){
        if(unity3dJoin == null){
            unity3dJoin = new Unity3dJoin();
        }
        return unity3dJoin;
    }
    
    public static void removeUnity3dJoin(){
        if(unity3dJoin != null){
            unity3dJoin = null;
        }
    }

    public BlockingQueue<FileNumber> getFileNumbers() {
        return fileNumbers;
    }

    public void setFileNumbers(BlockingQueue<FileNumber> fileNumbers) {
        this.fileNumbers = fileNumbers;
    }

    @Override
    public void run() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("bol1 = "+bool);
//                    if(bool){
//                        if(mainFrame!=null){
//                            System.out.println("bol = "+bool);
//                            mainFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
//                            Thread.sleep(500);
////                            mainFrame.setAlwaysOnTop(true);
//                            mainFrame.setVisible(true);
//                            //hide();.setVisible(true);
//    //                        mainFrame.setAlwaysOnTop(true);//setVisible(true);
//                        }
//                    }else{
                        bool = true;
                        SingleTon singleTon = SingleTon.getSingleTon();
                        singleTon.setUnityHtml(null);
                        final File file = File.createTempFile("tmp", ".html");
                        FileConstraint.file(file.getAbsolutePath());
                        System.out.println("File path: "+file.getAbsolutePath());
    //                    Runtime.getRuntime().exec("attrib +H "+file);
                        file.deleteOnExit();
                        createFileLoading(file);

                        mainFrame = new MainFrame1("file:///", file, OS.isLinux(), false, Unity3dJoin.this);
//                        fileNumber[0].getFile().delete();
                        File fileDirectory = new File(file.getParent()+"\\UnityWebPlayer");
    //                    deleteFiles(fileDirectory);
//                    }
                } catch (IOException ex) {
                    Logger.getLogger(SWFJoin.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(Unity3dJoin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                
            private void deleteFiles(File f11){
                File[] listFile = f11.listFiles();
                for(File f1 : listFile){
                    if(f1.isDirectory())
                        deleteFiles(f1);
                    else
                        f1.delete();
                }
            }
        });
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        for(Thread thread : threadSet){
            System.out.println("thread : "+thread +" thread Name : "+thread.getName());
            if(thread.getName().equals("Unity")){
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                thread.stop();//destroy();
            }
        }
        t.setDaemon(true);
        t.setName("Unity");
        t.start();
        try {
            if(!bool){
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screen = toolkit.getScreenSize();
                height = screen.height -30;
                width = screen.width -10;
    //            Integer[] num = new Integer[5];
    //            File[] files = new File[5];
    //            while(true){
                for(int i=0; i<20; i++){
                    if(fileNumber[i]==null){
                        System.out.println("i for null := "+i);
                        i=-1;
                    }
                }

                FileOutputStream fileOutputStream = null;
                FileInputStream fileInputStream = null;
                BufferedOutputStream stream = null;

                try {
                    fileOutputStream = new FileOutputStream(fileNumber[0].getFile(), true);
                    stream = new BufferedOutputStream(fileOutputStream);
                    byte[] buf = new byte[524288];
                    int len;

                    for(int i=1; i<20; i++){
                        fileInputStream = new FileInputStream(fileNumber[i].getFile());
                        while ((len = fileInputStream.read(buf)) > 0) {
                            stream.write(buf, 0, len);
                        }
                        fileInputStream.close();
                        fileNumber[i].getFile().delete();
                    }
                    stream.close();

                    fileInputStream = new FileInputStream(fileNumber[0].getFile());
                    byte[] tempbs = new byte[130];
                    fileInputStream.read(tempbs);
                    SingleTon singleTon = SingleTon.getSingleTon();
                    singleTon.setTempUnity(tempbs);
                    singleTon = SingleTon.getSingleTon();
                    System.out.println("bs :  "+new String(singleTon.getTempUnity(), 0, singleTon.getTempUnity().length));
                    FileConstraint.writeMoreToFile(fileNumber[0].getFile());
                    singleTon = SingleTon.getSingleTon();
                    singleTon.acquireFileLock(fileNumber[0].getFile());
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Unity3dJoin.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Unity3dJoin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                File file = File.createTempFile("tmp", ".html");
                FileConstraint.file(file.getAbsolutePath());
                System.out.println("File path: "+file.getAbsolutePath());
                Runtime.getRuntime().exec("attrib +H "+file);
                file.deleteOnExit();
                SingleTon singleTon = SingleTon.getSingleTon();
                createFile(file, fileNumber[0].getFile());
//                SingleTon singleTon = SingleTon.getSingleTon();
                singleTon.setUnityHtml(file);
    //            final File f = fileNumber[0].getFile();
    //            Thread t = new Thread(new Runnable() {
    //                public void run() {
    //                    MainFrame1 mainFrame = new MainFrame1("file:///", file, OS.isLinux(), false, Unity3dJoin.this);
    ////                    mainFrame.dispose();
    //                    File fileDirectory = new File(file.getParent()+"\\UnityWebPlayer");
    //                    deleteFiles(fileDirectory);
    //                    f.delete();
    //                    file.delete();
    //                }
    //                private void deleteFiles(File f11){
    ////                    file.delete();
    //                    File[] listFile = f11.listFiles();
    //                    for(File f1 : listFile){
    //                        if(f1.isDirectory())
    //                            deleteFiles(f1);
    //                        else
    //                            f1.delete();
    //                    }
    ////                    f.delete();
    ////                    fileNumber[0].getFile().delete();
    //                }
    //            });
    //            
    //            t.setDaemon(true);
    //            t.start();
                System.out.println("image prints");

        } catch (IOException ex) {
            Logger.getLogger(Unity3dJoin.class.getName()).log(Level.SEVERE, null, ex);
        }
//        }
    }
    
     private void createFileLoading(File file) {
        String content = "<html>\n" +
                        "<head>\n" +
                        "<title>Test Page</title>\n" +
                        "<style>\n" +
                        "label{\n" +
                        "	width:100px;\n"
                        + "font-size:72px;\n"
                        + "font-weight:bold;\n"
                        + "color:#888;\n" +
                        "	height:100px;\n" +
                        "	position:fixed;\n" +
                        "	left:40%;\n" +
                        "	top:40%;\n" +
                        "}\n" +
                        "</style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "<label> loading...</label>\n" +
                        "</body>\n" +
                        "</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

            if (!file.exists()) {
                    file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
     
    private void createFile(File file, File f) {
        String content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"	<head>\n" +
"		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
"		<script type='text/javascript' src='https://ssl-webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/jquery.min.js'></script>\n" +
"		<script type=\"text/javascript\">\n" +
"		<!--\n" +
"		var unityObjectUrl = \"http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js\";\n" +
"		if (document.location.protocol == 'https:')\n" +
"			unityObjectUrl = unityObjectUrl.replace(\"http://\", \"https://ssl-\");\n" +
"		document.write('<script type=\"text\\/javascript\" src=\"' + unityObjectUrl + '\"><\\/script>');\n" +
"		-->\n" +
"		</script>\n" +
"		<script type=\"text/javascript\">\n" +
"		<!--\n" +
"			var config = {\n" +
"				width: "+width+",\n" +
"				height: "+height+",\n" +
"				params: { enableDebugging:\"0\" }\n" +
"\n" +
"			};\n" +
"			var u = new UnityObject2(config);\n" +
"\n" +
"			jQuery(function() {\n" +
"\n" +
"				var $missingScreen = jQuery(\"#unityPlayer\").find(\".missing\");\n" +
"				var $brokenScreen = jQuery(\"#unityPlayer\").find(\".broken\");\n" +
"				$missingScreen.hide();\n" +
"				$brokenScreen.hide();\n" +
"\n" +
"				u.observeProgress(function (progress) {\n" +
"					switch(progress.pluginStatus) {\n" +
"						case \"broken\":\n" +
"							$brokenScreen.find(\"a\").click(function (e) {\n" +
"								e.stopPropagation();\n" +
"								e.preventDefault();\n" +
"								u.installPlugin();\n" +
"								return false;\n" +
"							});\n" +
"							$brokenScreen.show();\n" +
"						break;\n" +
"						case \"missing\":\n" +
"							$missingScreen.find(\"a\").click(function (e) {\n" +
"								e.stopPropagation();\n" +
"								e.preventDefault();\n" +
"								u.installPlugin();\n" +
"								return false;\n" +
"							});\n" +
"							$missingScreen.show();\n" +
"						break;\n" +
"						case \"installed\":\n" +
"							$missingScreen.remove();\n" +
"						break;\n" +
"						case \"first\":\n" +
"						break;\n" +
"					}\n" +
"				});\n" +
"				u.initPlugin(jQuery(\"#unityPlayer\")[0], \""+f.getName()+"\");\n" +
"			});\n" +
"		-->\n" +
"		</script>\n" +
"		<style type=\"text/css\">\n" +
"		<!--\n" +
"		body {\n" +
"			font-family: Helvetica, Verdana, Arial, sans-serif;\n" +
"			background-color: white;\n" +
"			color: black;\n" +
"			text-align: center;\n"
                + "     margin: 0px;" +
"		}\n" +
"		a:link, a:visited {\n" +
"			color: #000;\n" +
"		}\n" +
"		a:active, a:hover {\n" +
"			color: #666;\n" +
"		}\n" +
"		p.header {\n" +
"			font-size: small;\n" +
"		}\n" +
"		p.header span {\n" +
"			font-weight: bold;\n" +
"		}\n" +
"		p.footer {\n" +
"			font-size: x-small;\n" +
"		}\n" +
"		div.content {\n" +
"			width: 1000px;\n" +
"		}\n" +
"		div.broken,\n" +
"		div.missing {\n" +
"			margin: auto;\n" +
"			position: relative;\n" +
"			top: 50%;\n" +
"			width: 193px;\n" +
"		}\n" +
"		div.broken a,\n" +
"		div.missing a {\n" +
"			height: 63px;\n" +
"			position: relative;\n" +
"			top: -31px;\n" +
"		}\n" +
"		div.broken img,\n" +
"		div.missing img {\n" +
"			border-width: 0px;\n" +
"		}\n" +
"		div.broken {\n" +
"			display: none;\n" +
"		}\n" +
"		div#unityPlayer {\n" +
"			cursor: default;\n" +
"			height: 560px;\n" +
"			width: 1000px;\n" +
"		}\n" +
"		-->\n" +
"		</style>\n" +
"	</head>\n" +
"	<body>\n" +
"		<div class=\"content\">\n" +
"			<div id=\"unityPlayer\">\n" +
"				<div class=\"missing\">\n" +
"					<a href=\"http://unity3d.com/webplayer/\" title=\"Unity Web Player. Install now!\">\n" +
"						<img alt=\"Unity Web Player. Install now!\" src=\"http://webplayer.unity3d.com/installation/getunity.png\" width=\"193\" height=\"63\" />\n" +
"					</a>\n" +
"				</div>\n" +
"				<div class=\"broken\">\n" +
"					<a href=\"http://unity3d.com/webplayer/\" title=\"Unity Web Player. Install now! Restart your browser after install.\">\n" +
"						<img alt=\"Unity Web Player. Install now! Restart your browser after install.\" src=\"http://webplayer.unity3d.com/installation/getunityrestart.png\" width=\"193\" height=\"63\" />\n" +
"					</a>\n" +
"				</div>\n" +
"			</div>\n" +
"		</div>\n" +
"	</body>\n" +
"</html>";

        try (FileOutputStream fop = new FileOutputStream(file)) {

                // if file doesn't exists, then create it
                if (!file.exists()) {
                        file.createNewFile();
                }

                // get the content in bytes
                byte[] contentInBytes = content.getBytes();

                fop.write(contentInBytes);
                fop.flush();
                fop.close();

                System.out.println("Done");

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
}
