/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import org.coretechies.MessageTransporter.LoginRequest;
import org.coretechies.MessageTransporter.ResourceRequest;
import org.coretechies.server.SHA;


/**
 *
 * @author Tiwari
 */
public class Controller {
    
    public LoginRequest getUserField(String userName, String password){
        LoginRequest loginRequest =new LoginRequest();
        loginRequest.setUserName(userName);;
        loginRequest.setMessageType("Login");

        SHA sh = new SHA();
        loginRequest.setHash(sh.getSha(userName+password));

        return loginRequest;
    }

    public ResourceRequest sendResourceRequest(String hash){
        
        ResourceRequest resourceRequest = new ResourceRequest();
        
        resourceRequest.setHash(hash);;
        resourceRequest.setIdType(0);
        resourceRequest.setMessageType("Resource");
        return resourceRequest;
    }
    
}
