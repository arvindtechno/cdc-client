/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.forms.controller.SubjectController;

/**
 *
 * @author Tiwari
 */
public class DecryptionQueue implements Runnable{

    BlockingQueue<byte[]> encryptedData;
    static boolean checkFile;
    private boolean end = false;
    private FileLock fileLock;
    private FileChannel channel;
    private RandomAccessFile file;
    static DecryptionQueue decryption;
    private long size;
    SingleTon singleTon;
    private boolean first = true;
    static{
        decryption=null;
        checkFile = false;
    }
    
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public FileLock getFileLock() {
        return fileLock;
    }

    public void setFileLock() {
        if(fileLock!=null && channel!=null && file!=null){
            try {
                fileLock.release();
                channel.close();
                file.close();
            } catch (IOException ex) {
                System.out.println("release lock");
                Logger.getLogger(DecryptionQueue.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    private DecryptionQueue() {
       encryptedData = new LinkedBlockingQueue<byte[]>() ;
    }
    
    public static DecryptionQueue getDecryption(){
        if(decryption == null){
            decryption=new DecryptionQueue();
        }
        return decryption;
    }
    
    public static void closeDecryption(){
        decryption = null;
    }

    public BlockingQueue<byte[]> getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(BlockingQueue<byte[]> encryptedData) {
        this.encryptedData = encryptedData;
    }
    ArrayList<byte[]> arrayList;

    public static boolean isCheckFile() {
        return checkFile;
    }

    public static void setCheckFile(boolean checkFile) {
        DecryptionQueue.checkFile = checkFile;
    }

    @Override
    public void run() {
        try {
            FileOutputStream fileOutputStream1 = null;
            BufferedOutputStream stream = null;
            
            File f1 = File.createTempFile("tmp", ".mp4");
            FileConstraint.file(f1.getAbsolutePath());
            singleTon = SingleTon.getSingleTon();
            singleTon.setMediaFile(f1);
            System.out.println("File path: "+f1.getAbsolutePath());
            Runtime.getRuntime().exec("attrib +H "+f1);
            f1.deleteOnExit();
            try {
                checkFile = false;
                while(true){
                    while(true){
                        if(!encryptedData.isEmpty())
                            break;
                    }
                    
                    byte[] newBs = getEncryptedData().peek();
                    singleTon = SingleTon.getSingleTon();
                    if(!end){
                        if(getEncryptedData().size() < size*2)
                            continue;
                        System.out.println("end");
                    }
//                    if(first){
//                        if(!SubjectController.getMp2dStatus()){
                            System.out.println("writeeeeeeeeeeeeeeee");
                            if(!f1.exists())
                                fileOutputStream1 = new FileOutputStream(f1);
                            else
                                fileOutputStream1 = new FileOutputStream(f1, true);

                            stream = new BufferedOutputStream(fileOutputStream1);
                            synchronized(getEncryptedData()){
                                while((newBs=getEncryptedData().poll())!=null){
                                    try{
                                        if(new String(newBs,0,newBs.length).trim().equalsIgnoreCase("End")){
                                            singleTon.setMp4Packate(-1);
                                            Thread.currentThread().stop();
                                            break;
                                        }
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }

                                    stream.write(newBs,0, newBs.length);
                                    stream.flush();
                                }
                            }
                            stream.close();
                            singleTon.setMp4Packate(singleTon.getMp4Packate()+1);
                            checkFile = true;
                            first = false;
                            if(singleTon.getMp4Packate()==2){
                                FileConstraint.writeMoreToFile(f1);
                                ArrayList<Object> arrayList = FileConstraint.lockFile(f1);
                                file = (RandomAccessFile) arrayList.get(0);
                                channel = (FileChannel) arrayList.get(1);
                                fileLock = (FileLock) arrayList.get(2);
                            }
//                            SubjectController.setMp2dStatus();
//                        }
//                    }else{
//                        System.out.println("writeeiiiiiiiiiiiiiiiii");
//                        if(!f1.exists())
//                            fileOutputStream1 = new FileOutputStream(f1);
//                        else
//                            fileOutputStream1 = new FileOutputStream(f1, true);
//
//                        stream = new BufferedOutputStream(fileOutputStream1);
//                        synchronized(getEncryptedData()){
//                            while((newBs=getEncryptedData().poll())!=null){
//                                try{
//                                    if(new String(newBs,0,newBs.length).trim().equalsIgnoreCase("End")){
//                                        singleTon.setMp4Packate(-1);
//                                        Thread.currentThread().stop();
//                                        break;
//                                    }
//                                }catch(Exception e){
//                                    e.printStackTrace();
//                                }
//
//                                stream.write(newBs,0, newBs.length);
//                                stream.flush();
//                            }
//                        }
//                        stream.close();
//                        singleTon.setMp4Packate(singleTon.getMp4Packate()+1);
//                        checkFile = true;
//                        if(singleTon.getMp4Packate()==2){
//                            FileConstraint.writeMoreToFile(f1);
//                            ArrayList<Object> arrayList = FileConstraint.lockFile(f1);
//                            file = (RandomAccessFile) arrayList.get(0);
//                            channel = (FileChannel) arrayList.get(1);
//                            fileLock = (FileLock) arrayList.get(2);
//                        }
//                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally{
                try {
                    if(fileOutputStream1!=null){
                        fileOutputStream1.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            singleTon.setMp4Packate(-1);
        } catch (IOException ex) {
            Logger.getLogger(DecryptionQueue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
