/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Client;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javax.swing.JFrame;
import org.coretechies.MessageTransporter.*;

/**
 *
 * @author Tiwari
 */
public class ClientReadUnity3d extends JFrame implements Runnable{
    
    private SingleTon singleTon;
    private Socket socket;
    private MediaDataRequest mediaDataRequest;
    ClientSocket clientSocket;
    private Unity3dThread thread;
    String Path;
    String str;
    
    public ClientReadUnity3d(Socket socket, MediaDataRequest mediaDataRequest, int num) {
        synchronized(mediaDataRequest){
            this.socket=socket;
            this.mediaDataRequest = mediaDataRequest;
            this.mediaDataRequest.setNum(num);
        }
    }

    public ClientReadUnity3d() {
    }

    @Override
    public void run() {
        ObjectInputStream ois = null;
        singleTon = SingleTon.getSingleTon();
        try {
            
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(mediaDataRequest);
            objectOutputStream.flush();
            
            ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            while(true){
                MessageResponse response = (MessageResponse)ois.readObject();
                String mType = response.getMessageType();
                if(mType.equals("MediaDataResponse")){
                    System.out.println("sleepin Data Response");
                    MediaDataResponse dataResponse =(MediaDataResponse) response;
                    Path = dataResponse.getPath();
                    thread = new Unity3dThread(mediaDataRequest.getNum());
                    
                    Thread unity3dThread = new Thread(thread);
                    unity3dThread.setDaemon(true);
                    unity3dThread.start();
                }else if(mType.equalsIgnoreCase("MediaDataunity3d")){
                    
                    BlockingQueue<Data> queue = thread.getQueueData();
                    byte[] bs = ((Data)response).getBs();
                    queue.add((Data)response);
                    if((new String(bs,0,bs.length).trim().equalsIgnoreCase("END")))
                        break;
                }
                singleTon = SingleTon.getSingleTon();   
                singleTon.setResponse(true);
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("IOException");
            Logger.getLogger(ClientReadUnity3d.class.getName()).log(Level.SEVERE, null, ex);
        }catch(NullPointerException npe){
            PrintWriter out = null;
            try {
                out = new PrintWriter(socket.getOutputStream(),true);
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                boolean flag = true;
                while(flag)
                {
                    str = in.readLine();
                    if(!str.equalsIgnoreCase("END"))
                    {
                        out.println(str);
                    }
                    else
                    {
                        flag=false;
                        out.println("END");
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ClientReadUnity3d.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Inner IOException");
            } finally {
                out.close();
            }
        }
    }
}