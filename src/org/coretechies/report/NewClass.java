/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.report;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Tiwari
 */
public class NewClass {
    private void jbtSelectActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
        String dateNew = " 23:59:59";
        String startDate = " 00:00:00";
        
        try
        {
            
            Map params = new HashMap();
            params.put("EndDate", dateNew);  
            params.put("StartDate", startDate);  
            
            InputStream st = getClass().getResourceAsStream("/XML/IssueList.jrxml");
            JasperDesign jasperDesign = JRXmlLoader.load(st);
            
            JRDesignQuery newQuery = new JRDesignQuery();
            newQuery.setText("");
            jasperDesign.setQuery(newQuery);
            
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,  new JREmptyDataSource());

            JasperViewer viewer = new JasperViewer(jasperPrint); 
            JasperViewer.viewReport(jasperPrint,false);
        }
        catch (JRException ex)
        {
        }
    }                                         

}
