/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.ChangePasswordRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdatePasswordRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class UpdateController implements Initializable {
    @FXML
    private PasswordField oldPassword;
    @FXML
    private PasswordField newPassword;
    @FXML
    private PasswordField retypePassword;
    @FXML
    private CheckBox showReminders;
    @FXML
    private Button btnSubmit;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private Label errorRetypePassword;
    @FXML
    private Label errorOldPassword;
    @FXML
    private Label errorNewPassword;
    @FXML
    private TextField username;
    private SingleTon singleTon;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Pane paneInsideBG;
    @FXML
    private Label errorUserName;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader1;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelCNG;
    @FXML
    private Text userName;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pageSwitch = new PageSwitch();
        btnSubmit.setGraphic(new ImageView("file:///C:/Users/user/Downloads/user_ffffff_32.png"));
        
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        while(singleTon.getUpdateUserName()==null&&singleTon.getUpdateUserId()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(UpdateController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        username.setText(singleTon.getUpdateUserName());
        username.setEditable(false);
        btnSubmit.setId(singleTon.getUpdateUserId());
        
        newPassword.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }
        });
        
        retypePassword.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }

        });
    }    

    @FXML
    private void submit(ActionEvent event) {
        
        if(oldPassword.getText().equals(""))
            errorOldPassword.setText("Enter Current Password");
        if(newPassword.getText().equals(""))
            errorNewPassword.setText("Enter New Password");
        if(retypePassword.getText().equals(""))
            errorRetypePassword.setText("Enter New Password");
        
        if(!(oldPassword.getText().equals("") && newPassword.getText().equals("") && retypePassword.getText().equals(""))){
            if(newPassword.getText().equals(retypePassword.getText())){
                ClientSocket clientSocket =ClientSocket.getClientSocket();

                Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                System.out.println("<<<<<<<<<<<<<<<<");
                UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
                updatePasswordRequest.setHash(clientSocket.getHash());
                updatePasswordRequest.setOldPassword(oldPassword.getText());
                updatePasswordRequest.setNewPassword(newPassword.getText());
                updatePasswordRequest.setId(btnSubmit.getId());

                updatePasswordRequest.setMessageType("UpdatePasswordRequest");
                queue.add(updatePasswordRequest);

                Stage stage = (Stage) btnBack.getScene().getWindow();
                pageSwitch.nextScreen(stage, "Settings.fxml");

            }else
                passwordMatch();
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }
    
    private void passwordMatch() {
        if(!newPassword.getText().equals(retypePassword.getText()))
            errorRetypePassword.setText("Passwords do not match! Retype the password");
        else
            errorRetypePassword.setText("");
        errorRetypePassword.setStyle("-fx-text-fill:red;");
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
