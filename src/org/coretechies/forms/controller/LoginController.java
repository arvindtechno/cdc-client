/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import org.coretechies.forms.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.Controller;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class LoginController implements Initializable {
    @FXML
    private TextField userName;
    @FXML
    private TextField password;
    @FXML
    private Button enter;
    static Boolean nextscreen = false;
    Stage stage;
    ClientSocket clientSocket;
    private TextField serverip;
     @FXML
    private Label lusername;
    @FXML
    private Label lpassword;
    @FXML
    private Button toEnglish;
    @FXML
    private Button toEspanol;
    @FXML
    private Button toChineese;
    @FXML
    private Button toFrancais;
    private Label lip;
    @FXML
    private Label luserlogin;
    @FXML
    private StackPane log;
    @FXML
    private Pane loginpane;
    
    public LoginController(ClientSocket clientSocket) {
        this.clientSocket = clientSocket;
    }

    
    public LoginController() {
        
        
    }
    /**
     * Initializes the controller class.
     */
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        enter.setCursor(Cursor.HAND);
        toFrancais.setCursor(Cursor.HAND);
        toEspanol.setCursor(Cursor.HAND);
        toEnglish.setCursor(Cursor.HAND);
        toChineese.setCursor(Cursor.HAND);
        userName.setText("core");
        password.setText("core");
        
//        loginpane.clipProperty();
    }    

    @FXML
    private void userName(ActionEvent event) {
        
    }

    @FXML
    private void password(ActionEvent event) {
        
    }
    

    @FXML
    private void login(ActionEvent event) throws InterruptedException {
//            String UsernameS;
//            String passwordS;
//            String serveripS;
//            
//            UserfieldData ufd= new UserData();
//            UsernameS=ufd.getUserName();
//            passwordS=ufd.getPassword();
//            serveripS=ufd.getserverip();
//            
//            Controller c =new Controller();
//            MessageRequest mr= c.getUserField(UsernameS, passwordS);
//            stage = (Stage) enter.getScene().getWindow();
//            ClientSocket cs = ClientSocket.getClientSocket();
//            cs.createss(mr,stage,serveripS);
//            System.out.println("bScene");
        
        authenticateLogin();
        

    }
    public Scene loadCurr(){
        try {
            CurriculumAndGradeController cc = new CurriculumAndGradeController(clientSocket);
            StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("CurriculumAndGrade"
                    + ".fxml"));
            Scene scene = new Scene(page);
            
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void nextScreen(){
        
     System.out.println("curriculum");
        Stage stage = (Stage) enter.getScene().getWindow();
        System.out.println("scene");

        try {
             StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("search120"
                    + ".fxml"));
             Scene scene = new Scene(page);
           stage.setScene(scene);
            stage.setTitle("CDC");
            stage.show();

        } catch (Exception ex) {
             Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @FXML
    private void english(ActionEvent event) {
        lusername.setText("User Name");
        lpassword.setText("Password");
        lip.setText("Server I.P.");
        luserlogin.setText("User Login");
    }

    @FXML
    private void espanol(ActionEvent event) {
        lusername.setText("Nombre de Usuario");
        lpassword.setText("Contraseña");
        lip.setText("I.P. servidor");
        luserlogin.setText("Login de Usuario");
    }

    @FXML
    private void chineese(ActionEvent event) {
        lusername.setText("用户名");
        lpassword.setText("密码");
        lip.setText("服务器I.P.");
        luserlogin.setText("用户登录");
    }

    @FXML
    private void francais(ActionEvent event) {
        lusername.setText("Nom d'utilisateur");
        lpassword.setText("mot de passe");
        lip.setText("serveur I.P.");
        luserlogin.setText("login utilisateur");
    }

    
    @FXML
    private void logg(javafx.scene.input.KeyEvent event) {
       String code = event.getCode().toString();
        System.out.println(event.getCode());
        if(code.equals("ENTER")){
        try {
            authenticateLogin();
        } catch (InterruptedException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
    }

    
    
    private boolean isEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    public void setbool(boolean b) {
        LoginController.nextscreen=b;
        System.out.println("boole<<>>"+b);
        System.out.println("next"+nextscreen);
    }

    public Scene iview() {
         try {

             StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("simple"
                    + ".fxml"));
             ImageView imageView = clientSocket.getView();
             page.getChildren().add(imageView);
            Scene scene = new Scene(page);
            
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    
    private final class UserData implements UserfieldDataip {

        @Override
        public String getUserName() {
            if (userName == null || isEmpty(userName.getText())) {
                return null;
            }
            return userName.getText();
        }

        @Override
        public String getPassword() {
            if (password == null || isEmpty(password.getText())) {
                return null;
            }
            return password.getText();
        }

        @Override
        public String getserverip() {
            if (serverip == null || isEmpty(serverip.getText())) {
                return null;
            }
            return serverip.getText();
        }
        
    }
    
    
    public void authenticateLogin() throws InterruptedException{
        String UsernameS;
        String passwordS;
        String serveripS;

        UserfieldDataip ufd= new UserData();
        UsernameS=ufd.getUserName();
        passwordS=ufd.getPassword();
        serveripS=ufd.getserverip();

        Controller c =new Controller();
        MessageRequest mr= c.getUserField(UsernameS, passwordS);
        stage = (Stage) enter.getScene().getWindow();
        ClientSocket cs = ClientSocket.getClientSocket();
//        cs.createss(mr,stage,serveripS);
        System.out.println("bScene");
    }
}
