/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.InputStream;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.DeleteLessonPlanRequest;
import org.coretechies.MessageTransporter.LessonDateRequest;
import org.coretechies.MessageTransporter.LessonPlanning;
import org.coretechies.MessageTransporter.LessonPlanningRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MonthlyScheduleRequest;
import org.coretechies.MessageTransporter.UpdateLessonPlanRequest;
import org.coretechies.MessageTransporter.WeeklyScheduleRequest;
import org.coretechies.forms.controller.java.LessonScheduleView;

/**
 * FXML Controller class
 *
 * @author user
 */
public class LessonScheduleController implements Initializable {
    @FXML
    private Button btnPrint;
    @FXML
    private Button btnAddNewLesson;
    @FXML
    private Button btnPastSchedule;
    @FXML
    private Button btnUpcomingSchedule;
    @FXML
    private Button btnMonthlySchedule;
    @FXML
    private Button btnWeeklySchedule;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateto;
    @FXML
    private Pane panePrintSchedule;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private TableView<LessonScheduleView> lessonScheduleTable;
    @FXML
    private TableColumn colSubject;
    @FXML
    private TableColumn colLesson;
    @FXML
    private TableColumn colTopic;
    @FXML
    private TableColumn colClassDate;
    @FXML
    private TableColumn colUpdate;
    @FXML
    private TableColumn colDelete;
    ObservableList<LessonScheduleView> data;
    private Label labelSchedule;
    @FXML
    private Pane paneSelectWeeklySchedule;
    @FXML
    private DatePicker selectWeeklySchedule;
    @FXML
    private Button btnSubmitWeeklySchedule;
    @FXML
    private Label labelWeeklySchedule;
    @FXML
    private DatePicker selectMonthlySchedule;
    @FXML
    private Button btnSubmitMonthlySchedule1;
    @FXML
    private Label labelMonthlySchedule;
    
    SingleTon singleTon;
    @FXML
    private Pane paneUpdateSchedule;
    @FXML
    private DatePicker selectUpdatedDate;
    @FXML
    private Button btnSubmitUpdate;
    @FXML
    private Label labelMonthlySchedule1;

    private LessonScheduleView lessonScheduleView;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Button btnSubmitPrint;
    @FXML
    private Pane paneSelectMonthlySchedule;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Button btnClosePanePrint;
    @FXML
    private Button btnClosePaneSelectWeek;
    @FXML
    private Button btnClosePaneSelectMonth;
    @FXML
    private Button btnClosePaneUpdateScheduleDate;
    @FXML
    private HBox ERRORHbox;
    @FXML
    private Text textErrorInfo;
    @FXML
    private Text textMessage;
    @FXML
    private Button btnCloseError;
    @FXML
    private Text textDate;
    @FXML
    private RadioButton radioButtonCurrentSchedule;
    @FXML
    private RadioButton radioButtonScheduleDate;
    @FXML
    private Text userName;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        dateFrom.setDisable(true);
        dateto.setDisable(true);
        
        ToggleGroup group = new ToggleGroup();
        radioButtonCurrentSchedule.setToggleGroup(group);
        radioButtonScheduleDate.setToggleGroup(group);
                
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
        pageSwitch = new PageSwitch();
        paneSelectMonthlySchedule.setScaleX(0);
        paneSelectMonthlySchedule.setScaleY(0);
        paneSelectWeeklySchedule.setScaleX(0);
        paneSelectWeeklySchedule.setScaleY(0);
        paneUpdateSchedule.setScaleX(0);
        paneUpdateSchedule.setScaleY(0);
        panePrintSchedule.setScaleX(0);
        panePrintSchedule.setScaleY(0);
        colSubject.setCellValueFactory(
            new PropertyValueFactory<LessonScheduleView,String>("subject")
        );
        colLesson.setCellValueFactory(
            new PropertyValueFactory<LessonScheduleView,String>("lesson")
        );
        colTopic.setCellValueFactory(
            new PropertyValueFactory<LessonScheduleView,String>("topic")
        );
        colClassDate.setCellValueFactory(
            new PropertyValueFactory<LessonScheduleView,String>("classDate")
        );
        
        colUpdate.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<LessonScheduleView, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<LessonScheduleView, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            }
        );
        colUpdate.setCellFactory(
            new Callback<TableColumn<LessonScheduleView, Boolean>, TableCell<LessonScheduleView, Boolean>>() {
                @Override
                public TableCell<LessonScheduleView, Boolean> call(TableColumn<LessonScheduleView, Boolean> param) {
                     return new CreateButtonUpdate();
                }
            }
        );
        colDelete.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<LessonScheduleView, Boolean>, ObservableValue<Boolean>>() {
 
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<LessonScheduleView, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            }
        );
        colDelete.setCellFactory(new Callback<TableColumn<LessonScheduleView, Boolean>, TableCell<LessonScheduleView, Boolean>>() {

            @Override
            public TableCell<LessonScheduleView, Boolean> call(TableColumn<LessonScheduleView, Boolean> param) {
                 return new CreateButtonDelete();
            }
        });

        data = FXCollections.observableArrayList();
        lessonScheduleTable.setItems(data);
        
        singleTon = SingleTon.getSingleTon();
        while(singleTon.isLessonPlanResponse()==false){
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("At Lesson Schedule Controller");
        int size = singleTon.getLessonPlanningList().size();
        for(int i=0; i<size; i++){
            System.out.println("DATA");
            LessonScheduleView lessonScheduleView = new LessonScheduleView();

            lessonScheduleView.setId(singleTon.getLessonPlanningList().get(i).getLesson_plan_id()+"");
            lessonScheduleView.subject.setValue(singleTon.getLessonPlanningList().get(i).getSubject());
            lessonScheduleView.lesson.setValue(singleTon.getLessonPlanningList().get(i).getLesson());
            lessonScheduleView.topic.setValue(singleTon.getLessonPlanningList().get(i).getTopic());
            lessonScheduleView.classDate.setValue(singleTon.getLessonPlanningList().get(i).getClassDate().toString());
            
            data.add(lessonScheduleView);

        }
            
        singleTon.setLessonPlanningList(null);
        singleTon.setLessonPlanResponse(false);
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }

    @FXML
    private void print(ActionEvent event) {
        panePrintSchedule.setScaleX(1);
        panePrintSchedule.setScaleY(1);        
    }

    @FXML
    private void addNewLesson(ActionEvent event) {
        Stage stage = (Stage) btnAddNewLesson.getScene().getWindow();
        pageSwitch.nextScreen(stage, "AddLesson.fxml");
    }

    @FXML
    private void pastSchedule(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        singleTon.setLessonPlanResponse(false);
        LessonPlanningRequest lessonPlanningRequest = new LessonPlanningRequest();
        lessonPlanningRequest.setHash(clientSocket.getHash());
        lessonPlanningRequest.setMessageType("PastScheduleRequest");
        queue.add(lessonPlanningRequest);
        
        data.clear();
        
        singleTon = SingleTon.getSingleTon();
        while(!singleTon.isLessonPlanResponse()){
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getLessonPlanningList().size();
        for(int i=0; i<size; i++){
            LessonScheduleView lessonScheduleView = new LessonScheduleView();

            lessonScheduleView.setId(singleTon.getLessonPlanningList().get(i).getLesson_plan_id()+"");
            lessonScheduleView.subject.setValue(singleTon.getLessonPlanningList().get(i).getSubject());
            lessonScheduleView.lesson.setValue(singleTon.getLessonPlanningList().get(i).getLesson());
            lessonScheduleView.topic.setValue(singleTon.getLessonPlanningList().get(i).getTopic());
            lessonScheduleView.classDate.setValue(singleTon.getLessonPlanningList().get(i).getClassDate().toString());
            
            data.add(lessonScheduleView);
        }
        singleTon.setLessonPlanningList(null);
        singleTon.setLessonPlanResponse(false);
    }

    @FXML
    private void upcomingSchedule(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        singleTon.setLessonPlanResponse(false);
        LessonPlanningRequest lessonPlanningRequest = new LessonPlanningRequest();
        lessonPlanningRequest.setHash(clientSocket.getHash());
        lessonPlanningRequest.setMessageType("UpcomingScheduleRequest");
        queue.add(lessonPlanningRequest);
        
        data.clear();
        singleTon = SingleTon.getSingleTon();
        while(!singleTon.isLessonPlanResponse()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getLessonPlanningList().size();
        for(int i=0; i<size; i++){
            LessonScheduleView lessonScheduleView = new LessonScheduleView();

            lessonScheduleView.setId(singleTon.getLessonPlanningList().get(i).getLesson_plan_id()+"");
            lessonScheduleView.subject.setValue(singleTon.getLessonPlanningList().get(i).getSubject());
            lessonScheduleView.lesson.setValue(singleTon.getLessonPlanningList().get(i).getLesson());
            lessonScheduleView.topic.setValue(singleTon.getLessonPlanningList().get(i).getTopic());
            lessonScheduleView.classDate.setValue(singleTon.getLessonPlanningList().get(i).getClassDate().toString());
            
            data.add(lessonScheduleView);
        }
            
        singleTon.setLessonPlanningList(null);
        singleTon.setLessonPlanResponse(false);
    }

    @FXML
    private void monthlySchedule(ActionEvent event) {
        paneSelectMonthlySchedule.setScaleX(1);
        paneSelectMonthlySchedule.setScaleY(1);      
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    
                    @Override 
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item.isBefore(LocalDate.now().plusDays(1))&& Integer.parseInt(item.toString().substring(8, 10))!=1) {
                            setDisable(true);
                        }if (item.equals(LocalDate.now().plusDays(1))&& Integer.parseInt(item.toString().substring(8, 10))!=1) {
                            setDisable(true);
                        }if (item.isAfter(LocalDate.now().plusDays(1)) && Integer.parseInt(item.toString().substring(8, 10))!=1) {
                            setDisable(true);
                        }
                    }
                };
            }
        };
        
        selectMonthlySchedule.setDayCellFactory(dayCellFactory);
    }

    @FXML
    private void weeklySchedule(ActionEvent event) {
        paneSelectWeeklySchedule.setScaleX(1);
        paneSelectWeeklySchedule.setScaleY(1);
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    
                    @Override 
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item.equals(LocalDate.now().plusDays(1))&& (!item.getDayOfWeek().equals(DayOfWeek.MONDAY))) {
                            setDisable(true);
                        }if (item.isAfter(LocalDate.now().plusDays(1))&& (!item.getDayOfWeek().equals(DayOfWeek.MONDAY))) {
                            setDisable(true);
                        }if (item.isBefore(LocalDate.now().plusDays(1)) && (!item.getDayOfWeek().equals(DayOfWeek.MONDAY))) {
                            setDisable(true);
                        }
                    }
                };
            }
        };
        
        selectWeeklySchedule.setDayCellFactory(dayCellFactory);
    }

    @FXML
    private void getDatefrom(ActionEvent event) {
    }

    @FXML
    private void printSchedule(ActionEvent event) {
        
        if(radioButtonCurrentSchedule.isSelected()){
            Map<String, Object> params = new HashMap<>();
            params.put("title", "Lesson Plan");

            Report(params);
        }else if(radioButtonScheduleDate.isSelected()){
            if(dateFrom.getValue()!=null && dateto.getValue()!=null){
                String datefrom = dateFrom.getValue().getYear()+"/"+dateFrom.getValue().getMonthValue()+"/"+dateFrom.getValue().toString().substring(8, 10);
                String datetill = dateto.getValue().getYear()+"/"+dateto.getValue().getMonthValue()+"/"+dateto.getValue().toString().substring(8, 10);
                Map<String, Object> params = new HashMap<>();
                params.put("title", "Lesson Plan from "+datefrom+" to "+datetill);
                Report(params);
            }
        }
        panePrintSchedule.setScaleX(0);
        panePrintSchedule.setScaleY(0);
    }


    @FXML
    private void getDateTo(ActionEvent event) {
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    private void getSelectedSchedule(ActionEvent event) {
    }

    @FXML
    private void getWeeklySchedule(ActionEvent event) {
    }

    @FXML
    private void getSelectedWeeklySchedule(ActionEvent event) {
        
        if(selectWeeklySchedule.getValue()!=null){
            String date = selectWeeklySchedule.getValue().getYear()+"/"+selectWeeklySchedule.getValue().getMonthValue()+"/"+selectWeeklySchedule.getValue().toString().substring(8, 10);
            ClientSocket clientSocket = ClientSocket.getClientSocket();
            singleTon.setLessonPlanResponse(false);

            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            WeeklyScheduleRequest weeklyScheduleRequest = new WeeklyScheduleRequest();
            weeklyScheduleRequest.setHash(clientSocket.getHash());
            weeklyScheduleRequest.setMessageType("WeeklyScheduleRequest");
            weeklyScheduleRequest.setDateOfWeek(date);
            queue.add(weeklyScheduleRequest);
            
            data.clear();
            
            singleTon = SingleTon.getSingleTon();
            
            try {
                while(singleTon.isLessonPlanResponse()==false){
                    Thread.sleep(5);
                }Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            paneSelectWeeklySchedule.setScaleX(0);
            paneSelectWeeklySchedule.setScaleY(0);
        
            int size = singleTon.getLessonPlanningList().size();
            for(int i=0; i<size; i++){
                LessonScheduleView lessonScheduleView = new LessonScheduleView();

                lessonScheduleView.setId(singleTon.getLessonPlanningList().get(i).getLesson_plan_id()+"");
                lessonScheduleView.subject.setValue(singleTon.getLessonPlanningList().get(i).getSubject());
                lessonScheduleView.lesson.setValue(singleTon.getLessonPlanningList().get(i).getLesson());
                lessonScheduleView.topic.setValue(singleTon.getLessonPlanningList().get(i).getTopic());
                lessonScheduleView.classDate.setValue(singleTon.getLessonPlanningList().get(i).getClassDate().toString());

                data.add(lessonScheduleView);
            }

            singleTon.setLessonPlanningList(null);
            singleTon.setLessonPlanResponse(false);
        }
    }

    @FXML
    private void getMonthlySchedule(ActionEvent event) {
    }

    @FXML
    private void getSelectedMonthlySchedule(ActionEvent event) {
        
        if(selectMonthlySchedule.getValue()!=null){
            ClientSocket clientSocket = ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            singleTon.setLessonPlanResponse(false);
            MonthlyScheduleRequest monthlyScheduleRequest = new MonthlyScheduleRequest();
            monthlyScheduleRequest.setHash(clientSocket.getHash());
            monthlyScheduleRequest.setMessageType("MonthlyScheduleRequest");
            monthlyScheduleRequest.setDateOfMonth(selectMonthlySchedule.getValue().getMonthValue());
            queue.add(monthlyScheduleRequest);
            
            singleTon = SingleTon.getSingleTon();
            try {
                while(!singleTon.isLessonPlanResponse()){
                    Thread.sleep(5);
                }
            Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            data.clear();
            paneSelectMonthlySchedule.setScaleX(0);
            paneSelectMonthlySchedule.setScaleY(0);

            int size = singleTon.getLessonPlanningList().size();
            for(int i=0; i<size; i++){
                LessonScheduleView lessonScheduleView = new LessonScheduleView();

                lessonScheduleView.setId(singleTon.getLessonPlanningList().get(i).getLesson_plan_id()+"");
                lessonScheduleView.subject.setValue(singleTon.getLessonPlanningList().get(i).getSubject());
                lessonScheduleView.lesson.setValue(singleTon.getLessonPlanningList().get(i).getLesson());
                lessonScheduleView.topic.setValue(singleTon.getLessonPlanningList().get(i).getTopic());
                lessonScheduleView.classDate.setValue(singleTon.getLessonPlanningList().get(i).getClassDate().toString());
                data.add(lessonScheduleView);

            }

            singleTon.setLessonPlanningList(null);
            singleTon.setLessonPlanResponse(false);
        }
    }

    @FXML
    private void getUpdateDate(ActionEvent event) {
    }

    @FXML
    private void UpdateDate(ActionEvent event) {
        if(selectUpdatedDate.getValue()!=null){
            
            singleTon = SingleTon.getSingleTon();
            singleTon.setResponse(false);
            singleTon.setUpdateResponse(false);
            
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            String date = selectUpdatedDate.getValue().getYear()+"/"+selectUpdatedDate.getValue().getMonthValue()+"/"+selectUpdatedDate.getValue().toString().substring(8, 10);
            
            Calendar cal = Calendar.getInstance();

            if(Integer.parseInt(date.substring(0, 4)) >= cal.get(Calendar.YEAR)){
                if(Integer.parseInt(date.substring(5, 7)) > (cal.get(Calendar.MONTH))){
                    if(Integer.parseInt(date.substring(8, 10)) >= cal.get(Calendar.DATE)){

                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        UpdateLessonPlanRequest updateLessonPlanRequest = new UpdateLessonPlanRequest();
                        updateLessonPlanRequest.setHash(clientSocket.getHash());
                        updateLessonPlanRequest.setId(lessonScheduleView.getId());
                        updateLessonPlanRequest.setClassDate(date);

                        updateLessonPlanRequest.setMessageType("UpdateLessonPlanRequest");
                        queue.add(updateLessonPlanRequest);
                        singleTon = SingleTon.getSingleTon();
                        while(!singleTon.isResponse()){
                            try {
                                Thread.sleep(5);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        if(singleTon.isUpdateResponse()){
                            paneUpdateSchedule.setScaleX(0);
                            paneUpdateSchedule.setScaleY(0);
                            LessonScheduleView lessonScheduleView1 = new LessonScheduleView();
                            lessonScheduleView1.setId(lessonScheduleView.getId());
                            lessonScheduleView1.subject.setValue(lessonScheduleView.getSubject());
                            lessonScheduleView1.lesson.setValue(lessonScheduleView.getLesson());
                            lessonScheduleView1.topic.setValue(lessonScheduleView.getTopic());
                            lessonScheduleView1.classDate.setValue(date);
                            data.remove(lessonScheduleView);
                            data.add(lessonScheduleView1);
                        }
                    }else{
                        //Warning
                    }
                }else{
                    //Warning
                }
            }else{
                //Warning
            }
        }
    }

    @FXML
    private void closePanePrint(ActionEvent event) {
        panePrintSchedule.setScaleX(0);
        panePrintSchedule.setScaleY(0);
    }

    @FXML
    private void closePaneSelectWeek(ActionEvent event) {
        paneSelectWeeklySchedule.setScaleX(0);
        paneSelectWeeklySchedule.setScaleY(0);
    }

    @FXML
    private void closePaneSelectMonth(ActionEvent event) {
        paneSelectMonthlySchedule.setScaleX(0);
        paneSelectMonthlySchedule.setScaleY(0);
    }

    @FXML
    private void closePaneUpdateScheduleDate(ActionEvent event) {
        paneUpdateSchedule.setScaleX(0);
        paneUpdateSchedule.setScaleY(0);
    }

    @FXML
    private void closeErrorAction(ActionEvent event) {
    }
    
    public void Report(Map<String, Object> params) {
        try {
    
            InputStream st = getClass().getResourceAsStream("newfile.jrxml");
            
            JasperDesign jasperDesign = JRXmlLoader.load(st);
            
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, getDataSource());

            JasperViewer.viewReport(jasperPrint,false);
        } catch (JRException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
    
    private JRDataSource getDataSource() {
        ArrayList<BeanWithList> coll = new ArrayList<>();
        
        if(radioButtonCurrentSchedule.isSelected()){
            int size = lessonScheduleTable.getItems().size();
            for(int i=0; i<size; i++){
                LessonScheduleView lsv = lessonScheduleTable.getItems().get(i);
                BeanWithList bean = new BeanWithList(i+1, lsv.getSubject(), lsv.getLesson(), lsv.getTopic(), lsv.getClassDate());
                coll.add(bean);
            }
        }else if(radioButtonScheduleDate.isSelected()){
            String datefrom = dateFrom.getValue().getYear()+"/"+dateFrom.getValue().getMonthValue()+"/"+dateFrom.getValue().toString().substring(8, 10);
            String datetill = dateto.getValue().getYear()+"/"+dateto.getValue().getMonthValue()+"/"+dateto.getValue().toString().substring(8, 10);
            
            singleTon = SingleTon.getSingleTon();
            singleTon.setResponse(false);
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            LessonDateRequest lessonDateRequest = new LessonDateRequest();
            lessonDateRequest.setDateFrom(datefrom);
            lessonDateRequest.setDateTo(datetill);
            lessonDateRequest.setHash(clientSocket.getHash());
            lessonDateRequest.setMessageType("LessonDateRequest");
            queue.add(lessonDateRequest);

            while(!singleTon.isResponse()){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int size = singleTon.getLessonPlanningList().size();
            for(int i=0; i<size; i++){
                LessonPlanning lessonPlanning = singleTon.getLessonPlanningList().get(i);
                BeanWithList bean = new BeanWithList(i+1, lessonPlanning.getSubject(), lessonPlanning.getLesson(), lessonPlanning.getTopic(), lessonPlanning.getClassDate().toString());
                coll.add(bean);
            }
        }

        return new JRBeanCollectionDataSource(coll);
    }

    @FXML
    private void radioButtonScheduleDateAction(ActionEvent event) {
        dateFrom.setDisable(false);
        dateto.setDisable(false);
    }

    @FXML
    private void radioButtonScheduleCurrentAction(ActionEvent event) {
        dateFrom.setDisable(true);
        dateto.setDisable(true);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
    
    public class CreateButtonUpdate extends TableCell<LessonScheduleView, Boolean> {
        final Button updateButton = new Button("update");
        public CreateButtonUpdate(){
            setAlignment(Pos.CENTER);
            updateButton.setCursor(Cursor.HAND);
            System.out.println(CreateButtonUpdate.this.getIndex());
            updateButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    paneUpdateSchedule.setScaleX(1);
                    paneUpdateSchedule.setScaleY(1);
                    LessonScheduleView item = (LessonScheduleView) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    lessonScheduleView = item;
                }
            });
        }
          
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
            updateButton.setStyle("-fx-background-color:#008cba;-fx-text-fill: white;");}
            else{
                setGraphic(null);
        }}
    }

    public class CreateButtonDelete extends TableCell<LessonScheduleView, Boolean> {
        final Button deleteButton = new Button("delete");
        
        public CreateButtonDelete(){
            setAlignment(Pos.CENTER);
            deleteButton.setCursor(Cursor.HAND);
            System.out.println(CreateButtonDelete.this.getIndex());
            deleteButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    LessonScheduleView item = (LessonScheduleView) CreateButtonDelete.this.getTableView().getItems().get(CreateButtonDelete.this.getIndex());
                    
                    String date = item.classDate.getValue();
                    Calendar cal = Calendar.getInstance();
                    
                    if(Integer.parseInt(date.substring(0, 4)) >= cal.get(Calendar.YEAR)){
                        if(Integer.parseInt(date.substring(5, 7)) > (cal.get(Calendar.MONTH))){
                            if(Integer.parseInt(date.substring(8, 10)) >= cal.get(Calendar.DATE)){

                                ClientSocket clientSocket =ClientSocket.getClientSocket();
                                singleTon = SingleTon.getSingleTon();
                                singleTon.setLessonPlanResponse(false);
                                Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                                DeleteLessonPlanRequest deleteLessonPlanRequest = new DeleteLessonPlanRequest();
                                deleteLessonPlanRequest.setHash(clientSocket.getHash());
                                deleteLessonPlanRequest.setId(item.getId());

                                deleteLessonPlanRequest.setMessageType("DeleteLessonPlanRequest");
                                queue.add(deleteLessonPlanRequest);
                                
                                while(!singleTon.isLessonPlanResponse()){
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                if(singleTon.isResponse())
                                    data.remove(item);
                                else
                                    System.err.println("Can't Delete");
                            }else{
                                //Warning
                            }
                        }else{
                            //Warning
                        }
                    }else{
                        //Warning
                    }
                }
            });
        }
 
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(deleteButton);
                deleteButton.setStyle("-fx-background-color:#f04124;-fx-text-fill: white;");
            }else{
                setGraphic(null);
            }
        }
    }
}