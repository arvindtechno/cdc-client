/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.ChangePasswordRequest;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ChangePasswordController implements Initializable {
    @FXML
    private PasswordField oldPassword;
    @FXML
    private PasswordField newPassword;
    
    @FXML
    private CheckBox showReminders;
    @FXML
    private Button btnSubmit;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    private Label errorRetypePassword;
    private Label errorOldPassword;
    private Label errorNewPassword;
    
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Pane paneInsideBG;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private PasswordField retypePassword;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelChangepassword;
    @FXML
    private Text userName;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        SingleTon singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        pageSwitch = new PageSwitch();
//        btnSubmit.setGraphic(new ImageView("file:///C:/Users/user/Downloads/user_ffffff_32.png"));
        newPassword.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }
        });
        
        retypePassword.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }

        });
    }    

    @FXML
    private void submit(ActionEvent event) {
        if(oldPassword.getText().equals(""))
            errorOldPassword.setText("Enter Current Password");
        if(newPassword.getText().equals(""))
            errorNewPassword.setText("Enter New Password");
        if(retypePassword.getText().equals(""))
            errorRetypePassword.setText("Enter New Password");

        if(!(oldPassword.getText().equals("") && newPassword.getText().equals("") && retypePassword.getText().equals(""))){
            if(newPassword.getText().equals(retypePassword.getText())){
                ClientSocket clientSocket =ClientSocket.getClientSocket();
                Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                System.out.println("<<<<<<<<<<<<<<<<");
                ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                changePasswordRequest.setHash(clientSocket.getHash());
                changePasswordRequest.setOldPassword(oldPassword.getText());
                changePasswordRequest.setNewPassword(newPassword.getText());
                changePasswordRequest.setMessageType("ChangePasswordRequest");
                queue.add(changePasswordRequest);
                StackPane page;
                Stage stage = (Stage) btnBack.getScene().getWindow();
                pageSwitch.nextScreen(stage, "Settings.fxml");                
            }
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }
    
    private void passwordMatch() {
//        if(!newPassword.getText().equals(retypePassword.getText()))
//            lbPasswordMatch.setText("Passwords do not match! Retype the password");
//        else
//            lbPasswordMatch.setText("");
//        lbPasswordMatch.setStyle("-fx-text-fill:red;");
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
