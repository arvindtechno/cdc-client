/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.CreateUserRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdateUserRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class SettingsController implements Initializable {
    private Button logout;
    private Pane paneUserSettings;
    @FXML
    private Button btnCreateUser;
    @FXML
    private Button btnUpdateUser;
    @FXML
    private Button btnChangePassword;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;

    SingleTon singleTon;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelCNG;
    @FXML
    private ImageView iviewIcon;
    private ResourceBundle bundle;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader1;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Text userName;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bundle = rb;
        labelHome.setText(bundle.getString("Home"));
        labelBack.setText(bundle.getString("Back"));
        labelLogout.setText(bundle.getString("Logout"));
//        labelWelcome.setText(bundle.getString("Welcome"));
        btnCreateUser.setText(bundle.getString("Create User"));
        btnUpdateUser.setText(bundle.getString("Update User"));
        btnChangePassword.setText(bundle.getString("Change Password"));
//        labelCNG.setText(bundle.getString("Settings"));

        pageSwitch = new PageSwitch();
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }    

    @FXML
    private void createUser(ActionEvent event) {
        
        ClientSocket clientSocket =ClientSocket.getClientSocket();

        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        CreateUserRequest createUserRequest =new CreateUserRequest();
        createUserRequest.setHash(clientSocket.getHash());
        createUserRequest.setMessageType("CreateUserRequest");
        queue.add(createUserRequest);

        singleTon = SingleTon.getSingleTon();
        singleTon.setCreateUser(1);
        
        Stage stage = (Stage) btnCreateUser.getScene().getWindow();
        pageSwitch.nextScreen(stage, "CreateUserSchool.fxml");
    }

    @FXML
    private void upadateUser(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setUpdateUserList(null);
        ClientSocket clientSocket = ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setHash(clientSocket.getHash());
        updateUserRequest.setMessageType("UpdateUserRequest");
        queue.add(updateUserRequest);

        Stage stage = (Stage) btnCreateUser.getScene().getWindow();
        pageSwitch.nextScreen(stage, "UpdateUser.fxml");
    }

    @FXML
    private void changePassword(ActionEvent event) {
        Stage stage = (Stage) btnCreateUser.getScene().getWindow();
        pageSwitch.nextScreen(stage, "ChangePassword.fxml");
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
    
}
