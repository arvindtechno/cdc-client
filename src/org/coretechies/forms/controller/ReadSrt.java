/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ReadSrt {
    static  BufferedReader reader =null;
    static File file;
    String line="";
    int StartTime=0;
    int EndTime=0;
            
    static{
        try {
            file =new File("C:/Subtitle.srt");
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(ReadSrt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Object> readSubtitle(int currentTime){
        try {     
            String Subtitle = "";
            ArrayList<Object> list = new ArrayList<>();
            while(((line = reader.readLine()) != null) && !((StartTime<=currentTime) && EndTime>=currentTime)){
                line=reader.readLine();
                int shour=Integer.parseInt(line.substring(0, 2));
                int sminutes=Integer.parseInt(line.substring(3, 5));
                int sseconds=Integer.parseInt(line.substring(6, 8));
                int smiliseconds=((Integer.parseInt(line.substring(6, 8)))*1000)+Integer.parseInt(line.substring(9,12));
                StartTime=(((shour*3600)+(sminutes*60))*1000)+smiliseconds;
                
                int ehour=Integer.parseInt(line.substring(17, 19));
                int eminutes=Integer.parseInt(line.substring(20, 22));
                int eseconds=Integer.parseInt(line.substring(23, 25));
                int emiliseconds=((Integer.parseInt(line.substring(23, 25)))*1000)+Integer.parseInt(line.substring(26));

                EndTime=(((ehour*3600)+(eminutes*60))*1000)+emiliseconds;

                while((line = reader.readLine()).length()!=0)
                    Subtitle = Subtitle.concat(line);

               break;
            }
            list.add(StartTime);
            list.add(EndTime);
            list.add(Subtitle);
            System.out.println(Subtitle);
            return list;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
}
