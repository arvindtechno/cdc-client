/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.forms.controller;

import javafx.scene.control.TextField;

/**
 *
 * @author Tiwari
 */
public class ClassesDayScheduleTable {

    private final TextField monday;

    private final TextField tuesday;

    private final TextField wednesday;

    private final TextField thursday;

    private final TextField friday;

    public ClassesDayScheduleTable(TextField monday, TextField tuesday, TextField wednesday, TextField thursday, TextField friday) {
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }

    public TextField getMonday() {
        return monday;
    }

    public TextField getTuesday() {
        return tuesday;
    }

    public TextField getWednesday() {
        return wednesday;
    }

    public TextField getThursday() {
        return thursday;
    }

    public TextField getFriday() {
        return friday;
    }

}
