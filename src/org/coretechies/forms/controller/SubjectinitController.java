/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.PDFThread;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.ChapterRequest;
import org.coretechies.MessageTransporter.LessonRequest;
import org.coretechies.MessageTransporter.MediaDataRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SearchRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class SubjectinitController implements Initializable {
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnBack;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private TextField txtSearch;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelSubject;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Button search;
    @FXML
    private ComboBox<String> selectLanguage;
    @FXML
    private ComboBox<String> selectUnit;
    @FXML
    private ComboBox<String> selectChapter;
    @FXML
    private ComboBox<String> selectLesson;
    private PageSwitch pageSwitch;
    Stage stage;
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    double maxX= primaryScreenBounds.getMaxX();
    double maxY= primaryScreenBounds.getMaxY();
    double minY= primaryScreenBounds.getMinY();
    private SingleTon singleTon;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pageSwitch=new PageSwitch();
        singleTon=SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        while(singleTon.getUnitList()==null){
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(SubjectinitController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        int size = singleTon.getUnitList().size();
        for(int i=0;i<size;i++){
            selectUnit.getItems().add(singleTon.getUnitList().get(i));
        }
        
    }    

    @FXML
    private void back(ActionEvent event) {
        stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.back(stage, "SubjectList.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void searchContent(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            Stage stage = (Stage) search.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    @FXML
    private void lesson(ActionEvent event) {
       
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        if(selectChapter.getSelectionModel().getSelectedItem()!=null){
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            MediaDataRequest mediaDataRequest = new MediaDataRequest();
            mediaDataRequest.setHash(clientSocket.getHash());
            mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
            mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
            mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
            mediaDataRequest.setMessageType("MediaDataRequest");
            mediaDataRequest.setFileType("pdf");

            singleTon=SingleTon.getSingleTon();
            singleTon.setSearchLesson(selectChapter.getSelectionModel().getSelectedItem());
            singleTon.setSearchTopic(selectLesson.getSelectionModel().getSelectedItem());
            singleTon.setSearchChapter(selectUnit.getSelectionModel().getSelectedItem());
//            singleTon.setSearch(true);

            stage = (Stage) btnHome.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Subject.fxml");
        }
    }

    @FXML
    private void OnShowingLesson(Event event) {
        System.out.println("At on shown Lesson");
        singleTon = SingleTon.getSingleTon();
        selectLesson.getItems().clear();
        if(selectChapter.getSelectionModel().getSelectedItem()!=null){
            while(singleTon.getLessonList()==null){
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int size = singleTon.getLessonList().size();
            for(int i=0;i<size;i++){
                selectLesson.getItems().add(singleTon.getLessonList().get(i));
            }
        }
    }

    @FXML
    private void chapter(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        System.out.println("AT LESSON FUNCTION");
        if(selectUnit.getSelectionModel().getSelectedItem()!=null){
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            LessonRequest lessonRequest = new LessonRequest();        
            lessonRequest.setHash(clientSocket.getHash());
            lessonRequest.setChapterName(selectChapter.getSelectionModel().getSelectedItem());
            lessonRequest.setMessageType("LessonList");
            singleTon = SingleTon.getSingleTon();
            singleTon.setLessonList(null);
            queue.add(lessonRequest);
            selectLesson.getItems().clear();
        }
    }

    @FXML
    private void OnShowingChapter(Event event) {
        System.out.println("At on shown Chapter");
        singleTon = SingleTon.getSingleTon();
        selectChapter.getItems().clear();
        if(selectUnit.getSelectionModel().getSelectedItem()!=null){
            System.out.println("At on shown Chapterin"+selectUnit.getSelectionModel().getSelectedItem());
            while(singleTon.getChapterList()==null){
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int size = singleTon.getChapterList().size();
            for(int i=0;i<size;i++){
                selectChapter.getItems().add(singleTon.getChapterList().get(i));
            }
        }System.out.println("At on shown Chapterout");
    }

    @FXML
    private void unit(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();

        System.out.println("AT CHAPTER FUNCTION");
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        ChapterRequest chapterRequest =new ChapterRequest();
        chapterRequest.setHash(clientSocket.getHash());
        chapterRequest.setUnitName(selectUnit.getSelectionModel().getSelectedItem());
        chapterRequest.setMessageType("ChapterList");
        singleTon = SingleTon.getSingleTon();
        singleTon.setChapterList(null);
        queue.add(chapterRequest);
        selectChapter.getItems().clear();
        selectLesson.getItems().clear();
    }

    @FXML
    private void language(ActionEvent event) {
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.back(stage, "SubjectList.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

  
    
}
