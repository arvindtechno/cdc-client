/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.DataRequest;
import org.coretechies.MessageTransporter.LessonPlanningRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SearchRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class CurriculumAndGradeController implements Initializable{
    @FXML
    private Button search;
    @FXML
    private GridPane gradeGrid;
    @FXML
    private GridPane curriculumGrid;
    @FXML
    private Button settingBtn;
    @FXML
    private Button lessonPlanningBtn;
    @FXML
    private Button startBtn;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private CheckBox cboxRemainder;
    @FXML
    private Button btnYes;
    @FXML
    private Button btnNo;
    @FXML
    private Button btnClosePane;
    @FXML
    private TextField txtSearch;
    @FXML
    private ScrollPane spCurriculum;
    @FXML
    private AnchorPane canchorpaneinscroll;
    @FXML
    private ScrollPane spGrade;
    @FXML
    private AnchorPane ganchorpaneinscroll;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelCNG;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private HBox hBoxRemainder;
    private ResourceBundle bundle;
    private Pane cpaneinscroll;
    private String selectedCurriculum = "";
    private String selectedGrade = "";
    private SingleTon singleTon;
    private PageSwitch pageSwitch;
    private Stage stage;
    ClientSocket clientSocket;
    Integer i;
    Integer j=0;
    int k;
    private Pane gpaneinscroll;
    @FXML
    private Label lbChooseACurriculum;
    @FXML
    private Label lbLessonPlan;
    @FXML
    private Label lbConfirmation;
    @FXML
    private Label labelHome1;
    @FXML
    private Label labelBack1;
    @FXML
    private Label labelLogout1;
    @FXML
    private Text userName;
    
    public CurriculumAndGradeController() {
    }

    public CurriculumAndGradeController(ClientSocket clientSocket) {
        this.clientSocket = clientSocket;
    }
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bundle = rb;
        settingBtn.setText(bundle.getString("Setting"));
        lessonPlanningBtn.setText(bundle.getString("Lesson Planning"));
        startBtn.setText(bundle.getString("Start"));
        lbChooseACurriculum.setText(bundle.getString(" Choose a Curriculum "));
        labelWelcomeL.setText(bundle.getString("Welcome"));
        labelWelcomeR.setText(bundle.getString("Welcome"));
        labelCNG.setText(bundle.getString("Curriculum & Grade"));
        btnNo.setText(bundle.getString("No"));
        btnYes.setText(bundle.getString("Yes"));
        cboxRemainder.setText(bundle.getString("Do not show remainders again."));
        lbLessonPlan.setText(bundle.getString("You have lessons planned for today."));
        lbConfirmation.setText(bundle.getString("Do you want to see them now?"));
        
        ganchorpaneinscroll.setPrefHeight(200);
        canchorpaneinscroll.setPrefHeight(200);
        singleTon = SingleTon.getSingleTon();
        pageSwitch = new PageSwitch();
        if(singleTon.isLoginLessonPlanningResponce()){
            hBoxRemainder.setScaleX(1);
            hBoxRemainder.setScaleY(1);
        }else{
            hBoxRemainder.setScaleX(0);
            hBoxRemainder.setScaleY(0);
        }
        loadCNG();
        btnBack.setCursor(Cursor.HAND);
        btnHome.setCursor(Cursor.HAND);
        btnLogout.setCursor(Cursor.HAND);
        btnClosePane.setCursor(Cursor.HAND);
        btnNo.setCursor(Cursor.HAND);
        btnYes.setCursor(Cursor.HAND);
        labelBack1.setCursor(Cursor.HAND);
        labelHome1.setCursor(Cursor.HAND);
        labelLogout1.setCursor(Cursor.HAND);
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }    

    @FXML
    private void searchContent(ActionEvent event) {
        System.out.println("bScene");
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            Stage stage = (Stage) search.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    @FXML
    private void gridAction(MouseEvent event) {
    }

    public void loadCNG(){
        while(singleTon.getCurriculumList()== null && singleTon.getGradeList()==null){}
        ArrayList<String> arrayList = singleTon.getCurriculumList();
        int len = arrayList.size();
        for(k=0, i=0;i<len;i++,k++){
            
            Button button = new Button();
            button.setId(i+"");
            button.setPrefSize(97, 94);
            if(arrayList.get(i).length()==2){
                button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                System.out.println("TEXT : "+arrayList.get(i));
                button.setText(arrayList.get(i));
            }else{
                button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                System.out.println("TEXT : "+arrayList.get(i));
                button.setText(arrayList.get(i));
            }
            button.setCursor(Cursor.HAND);
            button.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!selectedCurriculum.equals(button.getText())){
                        if(button.getText().length()==2)
                            button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #1b707e;-fx-border-width:3;-fx-border-color:#2dbbd2;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 30%  ,#2a5f70,#111a24);");
                        else
                            button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: #1b707e;-fx-border-width:3;-fx-border-color:#2dbbd2;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 30%  ,#2a5f70,#111a24);");
                    }
                }
            });
            button.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!selectedCurriculum.equals(button.getText())){
                        if(button.getText().length()==2)
                            button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                        else
                            button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                    }                        //button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                }
            });
        
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    for(int count=0;count<curriculumGrid.getChildren().size();count++){
                        Button get = (Button) curriculumGrid.getChildren().get(count);
                        if(get.getText().length()==2)
                            get.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                        else
                            get.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                    }
                    selectedCurriculum = button.getText();
                    if(button.getText().length()==2)
                        button.setStyle("-fx-font-weight:bold;-fx-font-family:Arial;-fx-font-size:29;-fx-text-fill: orange;-fx-border-width:3;-fx-border-color:orange;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 45%  ,#2a5f70,#111a24);");
                    else
                        button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:20;-fx-text-fill: orange;-fx-border-width:3;-fx-border-color:orange;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 45%  ,#2a5f70,#111a24);");
                }
            });
            if(k==7){
                j++;
                k=0;
                curriculumGrid.add(button, k, j);
            }else{
                curriculumGrid.add(button, k, j);
            }
        }
        
        arrayList = singleTon.getGradeList();
        len = arrayList.size();
        String grd = "0";
        for(j=k=0, i=0;i<len;i++,k++){
            Button button = new Button();
            grd = "0";
            button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
            grd = grd + arrayList.get(i);
            button.setText(grd.substring(grd.length()-2));
            button.setPrefSize(97, 94);

            button.setCursor(Cursor.HAND);
            button.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!selectedGrade.equals(button.getText()))
                        button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #1b707e;-fx-border-width:3;-fx-border-color:#2dbbd2;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 30%  ,#2a5f70,#111a24);");
                }
            });
            button.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!selectedGrade.equals(button.getText()))
                        button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                }
            });
            
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                
                @Override
                public void handle(MouseEvent event) {
                    
                    for(int count=0;count<gradeGrid.getChildren().size();count++){
                        Button get = (Button) gradeGrid.getChildren().get(count);
                        get.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:29;-fx-text-fill: #3ab7cf;-fx-border-width:3;-fx-border-color:#84d7e5;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 90%  ,#2a5f70,#111a24);");
                    }
                    selectedGrade = button.getText();
                    button.setStyle("-fx-font-family:Arial;-fx-font-weight:bold;-fx-font-size:30;-fx-text-fill: orange;-fx-border-width:3;-fx-border-color:orange;-fx-shape: \"M230,70 L280,99 L260,150 L200,150 L180,99 Z\";-fx-background-color:radial-gradient(center 50% 50% , radius 45%  ,#2a5f70,#111a24);");
                }
            });
            if(k==7){
                j++;
                k=0;
                gradeGrid.add(button, k, j);
            }else{
                gradeGrid.add(button, k, j);
            }
        }
    }

    @FXML
    private void start(ActionEvent event) {
        if(selectedCurriculum != null && selectedGrade != null && !(selectedCurriculum.trim().equals("") || selectedGrade.trim().equals(""))){
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            DataRequest dataRequest =new DataRequest();
            dataRequest.setHash(clientSocket.getHash());
            dataRequest.setIdData(1);
            dataRequest.setCurriculum(selectedCurriculum);
            dataRequest.setGrade(Integer.parseInt(selectedGrade)+"");
            
            dataRequest.setMessageType("Data");
            queue.add(dataRequest);           
            
            Stage stage = (Stage) startBtn.getScene().getWindow();
            pageSwitch.nextScreen(stage, "SubjectList.fxml");
        }
    }

    @FXML
    private void back(ActionEvent event) {
        System.out.println("back");
    }

    @FXML
    private void logout(ActionEvent event) {
        System.out.println("logout");
        stage = (Stage) search.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        System.out.println("home");
    }

    @FXML
    private void setting(ActionEvent event) {
        stage = (Stage) search.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void lessonPlanning(ActionEvent event) {
        stage = (Stage) search.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void Yes(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        LessonPlanningRequest lessonPlanningRequest = new LessonPlanningRequest();
        lessonPlanningRequest.setHash(clientSocket.getHash());
        lessonPlanningRequest.setMessageType("LessonPlanningRequest");
        queue.add(lessonPlanningRequest);

        Stage stage = (Stage) btnYes.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonSchedule.fxml");
    }

    @FXML
    private void no(ActionEvent event) {
        hBoxRemainder.setScaleX(0);
        hBoxRemainder.setScaleY(0);
    }

    @FXML
    private void closePane(ActionEvent event) {
        hBoxRemainder.setScaleX(0);
        hBoxRemainder.setScaleY(0);
    }

    private void logoutlogout(MouseEvent event) {
        System.out.println("MM");
    }

    @FXML
    private void search(KeyEvent event) {
        if(event.getCode()==KeyCode.ENTER){
            searchContent(null);
        }
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        System.out.println("logout");
        stage = (Stage) search.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}