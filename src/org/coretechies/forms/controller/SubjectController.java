/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.awt.EventQueue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.JFrame;
import org.coretechies.Client.ClientReadMp4;
import org.coretechies.Client.ClientReadSWF;
import org.coretechies.Client.ClientReadUnity3d;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.ClientWriteMp4;
import org.coretechies.Client.DecryptionQueue;
import org.coretechies.Client.DecryptionQueue3d;
import org.coretechies.Client.FileConstraint;
import org.coretechies.Client.FileNumber;
import org.coretechies.Client.ImageThread;
import org.coretechies.Client.PDFThread;
import org.coretechies.Client.SWFJoin;
import org.coretechies.Client.SingleTon;
import org.coretechies.Client.Unity3dJoin;
import org.coretechies.MessageTransporter.ChapterRequest;
import org.coretechies.MessageTransporter.LessonRequest;
import org.coretechies.MessageTransporter.MediaDataContinueRequest;
import org.coretechies.MessageTransporter.MediaDataRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SearchRequest;
import org.coretechies.forms.PDFViewerWithOutManuBar;


/**
 * FXML Controller class
 *
 * @author user
 */
public class SubjectController extends JFrame implements Initializable {
    @FXML
    private Button search;
    @FXML
    private Button begin_quiz;
    @FXML
    private Button begin_simulation;
    @FXML
    private Button begin_key_vocabulary;
    @FXML
    private Button begin_concept;
    @FXML
    private StackPane mainstackpane;
    @FXML
    private StackPane spFScreen;
    @FXML
    private ComboBox<String> selectUnit;
    @FXML
    private ComboBox<String> selectChapter;
    @FXML
    private ComboBox<String> selectLesson;
    @FXML
    private Button pdfView;
    @FXML
    private Slider sliderFullscreen;
    @FXML
    private Button exitFullscreen;
    @FXML
    private Button playFullscreen;
    @FXML
    private Button fullscreenIntermediate;
    @FXML
    private Slider sliderIntermediate;
    @FXML
    private Button playIntermediate;
    @FXML
    private MediaView showMediaDefault;
    @FXML
    private Slider sliderDefault;
    @FXML
    private Button fullscreenDefault;
    private Task task;
    static Image imagev;
    private Pane sliderpane;
    private VBox vboxslider;
    private PageSwitch pageSwitch;
    public static MediaPlayer mp2d = null;
    public  Media m =null;
    private static boolean check = false;
//    public static boolean getMp() {
//        if(mp!=null)
//            return mp.getStatus().toString().equals("PLAYING");
//        else
//            return false;
//    }

    @FXML
    private Button playDefault;
    
    @FXML
    private Pane panewithmedia;
    @FXML
    private Pane imagePaneDefault;
    @FXML
    private Text showWebView;

    private SingleTon singleTon;
    private ClientSocket clientSocket;
    @FXML
    private ScrollPane scrollpane;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnLogout;
    
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getBounds();
    double maxX= primaryScreenBounds.getMaxX();
    double wid= primaryScreenBounds.getMaxX();
    double maxY= primaryScreenBounds.getMaxY();
    double hei= primaryScreenBounds.getMaxY();
    
    @FXML
    private TextField txtSearch;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelSubject;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Pane paneHBHeader;
    @FXML
    private Button btnVolumIcon;
    @FXML
    private Slider volumeSliderDefault;
    @FXML
    private Pane paneQBHeader;
    @FXML
    private Pane paneSBHeader;
    @FXML
    private Pane paneVBHeader;
    @FXML
    private Pane paneMBHeader;
    @FXML
    private AnchorPane anchorPanecontainDefaultMedia;
    @FXML
    private Label labelTime;
    private Duration duration;
    @FXML
    private Slider sliderVolumeFullscreen;
    @FXML
    private Button btnVolumIconFullScreen;
    @FXML
    private Label labelSubtitle;
    private ReadSrt readSrt;
    private ArrayList<Object> list;
    private int Starttime =0;
    private int Endtime=0;
    private String Subtitle="";
    DecryptionQueue decryptionQueue;
    DecryptionQueue3d decryptionQueue3d;
    @FXML
    private Label labelFSTime;
    @FXML
    private Label labelFSSubtitle;
    @FXML
    private ProgressBar progressbarDefault;
    
    private  double totalDuration;
    @FXML
    private Button btnSubtitle;
    @FXML
    private AnchorPane anchorpaneToolbar;
    @FXML
    private AnchorPane anchorPaneFSToolbar;
    @FXML
    private Button btnSubtitleFS;
    @FXML
    private Button btn3DMedia;
    @FXML
    private Button btnNormalMedia;
    private String chooseMedia="mp4";
    private boolean normalMediaPlayer;
    private boolean differentMediaPlayer;
    private boolean mediaPlayerStatus = true;
    private boolean currentMediaPlayer;//true for mp4 and false for 3dmp4
    public static MediaPlayer mp3D = null;
    @FXML
    private StackPane stackpanecontainmedia;
    @FXML
    private ProgressBar progressbarVolumeDefault;
    @FXML
    private ProgressBar progressbarVolumeFS;
    @FXML
    private ProgressBar progressbarFS;
    @FXML
    private ImageView gifLoadingDefault;
    @FXML
    private ImageView showImageDefault;
    @FXML
    private AnchorPane anchorpaneFSDisable;
    @FXML
    private ImageView gifLoadingFS;
    @FXML
    private StackPane stackpaneGifDefault;
    @FXML
    private StackPane stackpaneGifFS;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Text userName;
    
    Socket socket3dMp4 = null, socketMp4 = null;
    @FXML
    private MediaView showMediaDefault3d;
    @FXML
    private MediaView showMediaIntermediate3d;
    
    private double mpSlider = 0.0, mp3dSlider = 0.0;
    String mpText, mp3dText;
    @FXML
    private Slider sliderDefault3d;
    @FXML
    private ProgressBar progressbarDefault3d;
    @FXML
    private Label labelTime3d;
    int iSwf=0, iunity=0;
    private static MediaPlayer mp;
    
    public static boolean getMp2dStatus() {
        if(mp == mp2d){
            System.out.println("mp2dddddddddddddddddd");
            if(mp==null){
                System.out.println("mp==null");
                return false;
            }
            return mp.getStatus().toString().equals("PLAYING");
        }else
            return false;
    }
    public static void setMp2dStatus() {
        if(mp == mp2d){
            System.out.println("mp2d playyyyyyyyyyyyyyyy");
            if(mp!=null){
                System.out.println("mp!=null");
                mp.play();
            }
        }
    }
    
    public static boolean getMp3dStatus() {
        if(mp == mp3D)
            return mp.getStatus().toString().equals("PLAYING");
        else
            return false;
    }
    
    public static void setMp3dStatus() {
        if(mp == mp3D)
            mp.play();
    }
    @FXML
    private MediaView showMediaIntermediate2d;
    //currentMediaPlayer
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iSwf=0;
        iunity=0;
        decryptionQueue = DecryptionQueue.getDecryption();
        pageSwitch = new PageSwitch();
        sliderDefault3d.setScaleX(0);
        sliderDefault3d.setScaleY(0);
        labelTime3d.setScaleX(0);
        labelTime3d.setScaleY(0);
        progressbarDefault3d.setScaleX(0);
        progressbarDefault3d.setScaleY(0);
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getBounds();
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        sliderFullscreen.setOpacity(0);
        playFullscreen.setOpacity(0);
        spFScreen.setScaleX(0);
        spFScreen.setScaleY(0);
        exitFullscreen.setOpacity(0);
        
        if(singleTon.isSearch()){
            selectUnit.getItems().add(singleTon.getSearchChapter());
            selectChapter.getItems().add(singleTon.getSearchLesson());
            selectLesson.getItems().add(singleTon.getSearchTopic());
            selectChapter.getSelectionModel().selectFirst();
            selectUnit.getSelectionModel().selectFirst();
            selectLesson.getSelectionModel().selectFirst();
            System.out.println("check for waitttttt");
//            singleTon.setSearch(false);
        }
        else{
            selectUnit.getItems().add(singleTon.getSearchChapter());
            selectChapter.getItems().add(singleTon.getSearchLesson());
            selectLesson.getItems().add(singleTon.getSearchTopic());
            selectChapter.getSelectionModel().selectFirst();
            selectUnit.getSelectionModel().selectFirst();
            selectLesson.getSelectionModel().selectFirst();
            System.out.println("check for waitttttt");
            singleTon.setSearch(false);
            System.out.println("check for wait");
//            while(singleTon.getUnitList()==null){
//                try {
//                    System.out.println("waiting....");
//                    Thread.sleep(50);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
            int size = singleTon.getUnitList().size();
            for(int i=0;i<size;i++){
                System.out.println(singleTon.getUnitList().get(i));
                selectUnit.getItems().add(singleTon.getUnitList().get(i));
            }
        }System.out.println("cccccccccccccheck for wait");
        singleTon = SingleTon.getSingleTon();
        singleTon.setMp4(true);
        singleTon.setMp4player(false);
        singleTon.setMp43dplayer(false);
        btnNormalMedia.setStyle("-fx-effect: dropshadow( three-pass-box , white , 10, 0.3 , 0 , 0 );-fx-background-color:null;");
       btn3DMedia.setStyle("-fx-effect:null;-fx-background-color:null;");
        interLesson();
    }

    private void search(ActionEvent event) {

        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);

        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());        
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            Stage stage = (Stage) search.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    @FXML
    private void unit(ActionEvent event) {
//        System.out.println("At chaapter Showing function");
//        if(!singleTon.isSearch()){
//            clientSocket =ClientSocket.getClientSocket();
//            System.out.println("AT CHAPTER FUNCTION");
//            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
//            ChapterRequest chapterRequest =new ChapterRequest();
//            chapterRequest.setHash(clientSocket.getHash());
//            chapterRequest.setUnitName(selectUnit.getSelectionModel().getSelectedItem());
//            chapterRequest.setMessageType("ChapterList");
//            singleTon = SingleTon.getSingleTon();
//            singleTon.setChapterList(null);
//            queue.add(chapterRequest);
//            selectChapter.getItems().clear();
//            selectLesson.getItems().clear();
//        }
    }

    @FXML
    private void chapter(ActionEvent event) {
//        ClientSocket clientSocket =ClientSocket.getClientSocket();
//            
//        if(!singleTon.isSearch()){
//            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
//            LessonRequest lessonRequest = new LessonRequest();
//            lessonRequest.setHash(clientSocket.getHash());
//            lessonRequest.setChapterName(selectChapter.getSelectionModel().getSelectedItem());
//            lessonRequest.setMessageType("LessonList");
//            singleTon = SingleTon.getSingleTon();
//            singleTon.setLessonList(null);
//            queue.add(lessonRequest);
//            selectLesson.getItems().clear();
//        }
    }

    @FXML
    private void lesson(ActionEvent event) {
    }
    
    private void interLesson(){
        System.out.println("lessooooooonnnnnnnn");
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        singleTon = SingleTon.getSingleTon();
        singleTon.setResponse(false);
        singleTon.setPdfByte(null);
        PDFThread thread = PDFThread.getObject();
        Thread pdfThread = new Thread(thread);
        pdfThread.setDaemon(true);
        pdfThread.start();
        
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();

        MediaDataRequest mediaDataRequest = new MediaDataRequest();
        mediaDataRequest.setHash(clientSocket.getHash());
        mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
        mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
        mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
        mediaDataRequest.setMessageType("MediaDataRequest");
        mediaDataRequest.setFileType("pdf");
        queue.add(mediaDataRequest);

        showWeb();
    }

    @FXML
    private void quiz(ActionEvent event) {        
        clientSocket =ClientSocket.getClientSocket();
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            try {
                clientSocket = ClientSocket.getClientSocket();
                InetAddress add = InetAddress.getByName(clientSocket.getIP());
                
                Socket[] sockets = new Socket[20];
                ClientReadSWF[] read = new ClientReadSWF[20];
                SWFJoin sWFJoin = SWFJoin.getSWFJoin();
                Thread swfJoinThread = new Thread(sWFJoin);
                swfJoinThread.setDaemon(true);
                swfJoinThread.start();

                for(; iSwf<20; iSwf++){
                    MediaDataRequest mediaDataRequest = new MediaDataRequest();
                    mediaDataRequest.setHash(clientSocket.getHash());
                    mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setMessageType("MediaDataRequest");
                    mediaDataRequest.setFileType("swf");
                    
                    sockets[iSwf] = new Socket(add,5051);
                    read[iSwf] = new ClientReadSWF(sockets[iSwf], mediaDataRequest, iSwf);
                    Thread readThread = new Thread(read[iSwf]);
                    readThread.setDaemon(true);
                    readThread.start();
                }
//                Thread readThread = new Thread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        singleTon = SingleTon.getSingleTon();
//                        SWFJoin sWFJoin = SWFJoin.getSWFJoin();
//                        long size = singleTon.getSizeSWF();
//                        FileNumber[] fileNumber = sWFJoin.getFileNumber();
//                        
//                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                    }
//                });
//                readThread.setDaemon(true);
//                readThread.start();
            } catch (IOException ex) {
                Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    private void simulation(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setFileDataResponce(false);
        
        ClientSocket clientSocket = ClientSocket.getClientSocket();
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            
            try {
                clientSocket = ClientSocket.getClientSocket();
                InetAddress add = InetAddress.getByName(clientSocket.getIP());
                
                Socket[] sockets = new Socket[20];
                ClientReadUnity3d[] read = new ClientReadUnity3d[20];
//                Unity3dJoin.removeUnity3dJoin();
                Unity3dJoin unity3dJoin = Unity3dJoin.getUnity3dJoin();
                Thread unity3dJoinThread = new Thread(unity3dJoin);
                unity3dJoinThread.setDaemon(true);
                unity3dJoinThread.start();

                for(; iunity<20; iunity++){
                    MediaDataRequest mediaDataRequest = new MediaDataRequest();
                    mediaDataRequest.setHash(clientSocket.getHash());
                    mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
                    mediaDataRequest.setMessageType("MediaDataRequest");
                    mediaDataRequest.setFileType("unity3d");
                    
                    sockets[iunity] = new Socket(add,5051);
                    read[iunity] = new ClientReadUnity3d(sockets[iunity], mediaDataRequest, iunity);
                    Thread readThread = new Thread(read[iunity]);
                    readThread.setDaemon(true);
                    readThread.start();
                    
                }
            } catch (IOException ex) {
                Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    private void keyvocabulary(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            
//            SWFThread thread = SWFThread.getObject();
//            new Thread(thread).start();
            
            MediaDataRequest mediaDataRequest = new MediaDataRequest();
            mediaDataRequest.setHash(clientSocket.getHash());
            mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
            mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
            mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
            mediaDataRequest.setMessageType("MediaDataRequest");
            mediaDataRequest.setFileType("swf1");
            queue.add(mediaDataRequest);
        }
    }

    @FXML
    private void concept(ActionEvent event) {

        ClientSocket clientSocket =ClientSocket.getClientSocket();
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            
            ImageThread imageThread = ImageThread.getObject();
            Thread tImage = new Thread(imageThread);
            tImage.setDaemon(true);
            tImage.start();
            
            MediaDataRequest mediaDataRequest = new MediaDataRequest();
            mediaDataRequest.setHash(clientSocket.getHash());
            mediaDataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
            mediaDataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
            mediaDataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
            mediaDataRequest.setMessageType("MediaDataRequest");
            mediaDataRequest.setFileType("jpg");
            queue.add(mediaDataRequest);
        }
    }

    private void releaseFiles(){
        try {
            Unity3dJoin unity3dJoin = Unity3dJoin.getUnity3dJoin();
            FileNumber[] fileNumber = unity3dJoin.getFileNumber();
            singleTon = SingleTon.getSingleTon();
            singleTon.setMp4Packate(0);
            singleTon.setMp43dPackate(0);
//            singleTon.setmpMp4Packate(0);
            if(fileNumber[0]!=null){
                singleTon.releaseFileLock();
                fileNumber[0].getFile().delete();
            }
            SWFJoin sWFJoin = SWFJoin.getSWFJoin();
            fileNumber = sWFJoin.getFileNumber();
            singleTon = SingleTon.getSingleTon();
            if(fileNumber[0]!=null){
                singleTon.releaseSWFFileLock();
                fileNumber[0].getFile().delete();
            }
            if(mp2d!=null){
                mp2d.stop();
                mp2d.dispose();
                decryptionQueue = DecryptionQueue.getDecryption();
                decryptionQueue.setFileLock();
                DecryptionQueue.closeDecryption();
                singleTon.getMediaFile().delete();
            }if(mp3D!=null){
                mp3D.stop();
                mp3D.dispose();
                decryptionQueue3d = DecryptionQueue3d.getDecryption();
                decryptionQueue3d.setFileLock();
                DecryptionQueue3d.closeDecryption();
                singleTon.getMediaFile3D().delete();
            }
            if(socket3dMp4!=null)
                socket3dMp4.close();
            if(socketMp4!=null)
                socketMp4.close();
        } catch (IOException ex) {
            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void home(ActionEvent event) {
        releaseFiles();
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void back(ActionEvent event) {
        releaseFiles();
        singleTon=SingleTon.getSingleTon();
        Stage stage = (Stage) btnBack.getScene().getWindow();
        if(singleTon.isSearch()){
            pageSwitch.nextScreen(stage, "Search.fxml");
        }else{
            pageSwitch.nextScreen(stage, "SubjectList.fxml");
        }
    }

    @FXML
    private void logout(ActionEvent event) {
        releaseFiles();
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void play(MouseEvent event) {
    }

    //exit to full screen pressing 
    @FXML
    private void exitFS(javafx.scene.input.KeyEvent evt) {
        String code = evt.getCode().toString();
        if (code == "ESCAPE" ) {
            spFScreen.setScaleX(0);
            spFScreen.setScaleY(0);
            showMediaIntermediate2d.setOpacity(1);
            showMediaIntermediate3d.setOpacity(1);
//            showMediaIntermediate2d.setScaleX(1);
//                 showMediaIntermediate3d.setScaleX(1);
        }
    }

    @FXML
    private void actionFullscreenIntermediate(MouseEvent event) {
    }
    
    public void setFullScreen(MediaView showMediaIntermediate){
        Rectangle2D primaryScreenBound = Screen.getPrimary().getBounds();
        Stage stage =(Stage)showMediaIntermediate.getScene().getWindow();
        exitFullscreen.setOpacity(1);
        fullscreenIntermediate.setOpacity(0);
        spFScreen.setScaleX(1);
        spFScreen.setScaleY(1);
        sliderFullscreen.setOpacity(1);
        sliderIntermediate.setOpacity(0);
        playFullscreen.setOpacity(1);
        playIntermediate.setOpacity(0);
        stage.setFullScreen(!stage.isFullScreen());
        if(stage.isFullScreen()){
            progressbarVolumeFS.setProgress(sliderVolumeFullscreen.getValue());
            if(mp == mp2d){
                System.err.println(">DD");
                showMediaIntermediate3d.setOpacity(0);
                showMediaIntermediate2d.toFront();
//                showMediaIntermediate3d.setScaleX(0);
            }else{
                System.err.println(">DdD");
                
                showMediaIntermediate2d.setOpacity(0);
                showMediaIntermediate3d.toFront();
//                showMediaIntermediate2d.setScaleX(0);
                
            }
            panewithmedia.setLayoutX(0);
            panewithmedia.setLayoutY(0);
            spFScreen.setStyle("-fx-background-color:black;");
            int h =(int) primaryScreenBound.getMaxY();
            int w=(int) primaryScreenBound.getMaxX();
            showMediaIntermediate.setFitHeight(h);
            showMediaIntermediate.setFitWidth(w);
//            System.err.println(showMediaIntermediate.getInputMethodRequests);
            System.err.println("");

        }else{
            exitFullscreen.setOpacity(0);
            sliderFullscreen.setOpacity(0);
            sliderIntermediate.setOpacity(1);
            playFullscreen.setOpacity(0);
            playIntermediate.setOpacity(0);
            spFScreen.setStyle("-fx-background-color:transparent;");
            spFScreen.setScaleX(0);
            spFScreen.setScaleY(0);
        }
    }
    
    //Action for PDF VIEW
    @FXML
    private void pdf(ActionEvent event) {
        clientSocket =ClientSocket.getClientSocket();
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            byte[] newBs = singleTon.getPdfByte();
            if(newBs!=null){
                final ByteBuffer buf = ByteBuffer.allocate(newBs.length);
                buf.put(newBs);
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        PDFViewerWithOutManuBar dFViewer = new PDFViewerWithOutManuBar(true);
                        dFViewer.openPDFByteBuffer(buf);
                    }
                });
//            }else{
//                EventQueue.invokeLater(new Runnable() {
//                    public void run() {
//                        ByteBuffer buf = null;
//                        PDFViewerWithOutManuBar dFViewer = new PDFViewerWithOutManuBar(true);
//                        dFViewer.openPDFByteBuffer(buf);
//                    }
//                });
            }
        }
    }

    @FXML
    private void fullScreen0(MouseEvent event) {
        System.out.println(".................................................................0");
        if(mp!=null){
                if(mp==mp2d){
                    setFullScreen(showMediaIntermediate2d);
                }else{
                   setFullScreen(showMediaIntermediate3d);
                }
        }
       
    }

    @FXML
    private void fullScreen2(MouseEvent event) {
        System.out.println(".................................................................1");
         if(mp!=null){
                if(mp==mp2d){
                    setFullScreen(showMediaIntermediate2d);
                }else{
                   setFullScreen(showMediaIntermediate3d);
                }
        }
        spFScreen.setScaleX(0);
            spFScreen.setScaleY(0);
            showMediaIntermediate2d.setOpacity(1);
            showMediaIntermediate3d.setOpacity(1);
       
    }

    //playvideo on click image
    @FXML
    private void playMedia(MouseEvent event) {
        if(!(selectUnit.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null && selectLesson.getSelectionModel().getSelectedItem()==null)){
            decryptionQueue = DecryptionQueue.getDecryption();
            DecryptionQueue.setCheckFile(false);
            decryptionQueue3d = decryptionQueue3d.getDecryption();
            DecryptionQueue3d.setCheckFile3D(false);
//            readSrt = new ReadSrt();
            mediaPlayerStart();//create new thread
            playDefault.setStyle("-fx-background-color: null;");
            playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_20.png"))));
        }
    }

    public void mediaPlayerStart(){
        try {
            clientSocket =ClientSocket.getClientSocket();
//        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            MediaDataRequest dataRequest =new MediaDataRequest();
            dataRequest.setHash(clientSocket.getHash());
            dataRequest.setSubject(selectUnit.getSelectionModel().getSelectedItem());
            dataRequest.setLesson(selectChapter.getSelectionModel().getSelectedItem());
            dataRequest.setTopic(selectLesson.getSelectionModel().getSelectedItem());
            dataRequest.setFileType(chooseMedia);
            dataRequest.setMessageType("MediaDataRequest");
//        queue.add(dataRequest);
            
            clientSocket = ClientSocket.getClientSocket();
            InetAddress add = InetAddress.getByName(clientSocket.getIP());
            Socket socket = new Socket(add,5051);
            
//            Queue<MessageRequest> queue = clientSocket.getMp4requestQueue();
//            queue.add(dataRequest);
            
            ClientWriteMp4 write = new ClientWriteMp4(socket, dataRequest);
            ClientReadMp4 read = new ClientReadMp4(socket, write);
            
            Thread readThread = new Thread(read);
            readThread.setDaemon(true);
            readThread.start();
            
            Thread writeThread = new Thread(write);
            writeThread.setDaemon(true);
            writeThread.start();
            
            stackpaneGifDefault.setStyle("-fx-background-color:rgb(0,0,0,1)");
            if(chooseMedia.equals("mp4")){
                socketMp4 = socket;
                normalMediaPlayer = true;
                singleTon.setMp4player(true);
                imagePaneDefault.setScaleX(0);
                imagePaneDefault.setScaleY(0);
                Service<Void> service = new Service<Void>() {
                    @Override
                    protected Task<Void> createTask() {
                        return new Task<Void>() {
                            @Override
                            protected Void call() throws Exception {
                                //Background work
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        stackpaneGifDefault.setScaleX(1);
                                        stackpaneGifDefault.setScaleY(1);
                                        stackpaneGifFS.setScaleX(1);
                                        stackpaneGifFS.setScaleY(1);
                                        /*AKASH*/stackpaneGifFS.toFront();/*AKASH*/
                                    }
                                });
                                decryptionQueue = DecryptionQueue.getDecryption();
                                while(!decryptionQueue.isCheckFile()){
                                    try {
                                        check= true;
                                        System.out.println("Checkfile");
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        singleTon = SingleTon.getSingleTon();
                                        File file = singleTon.getMediaFile();
                                        mp2d = new MediaPlayer(new Media(file.toURI().toString()));
                                        mp2d.setAutoPlay(true);
                                        currentMediaPlayer = true;
                                        
                                        stackpaneGifDefault.setScaleX(0);
                                        stackpaneGifDefault.setScaleY(0);
                                        stackpaneGifFS.setScaleX(0);
                                        stackpaneGifFS.setScaleY(0);
                                        stackpaneGifDefault.setStyle("-fx-background-color:rgb(0,0,0,0.5)");
                                        showMediaDefault3d.setScaleX(0);
                                        showMediaDefault3d.setScaleY(0);
                                        showMediaDefault.setScaleX(1);
                                        showMediaDefault.setScaleY(1);
                                        showMediaDefault.setMediaPlayer(mp2d);
                                        showMediaIntermediate2d.setMediaPlayer(mp2d);
                                        mp = mp2d;
                                        PlayMediaPlayer(showMediaDefault, showMediaIntermediate2d, sliderDefault, progressbarDefault, labelTime);
                                    }
                                });
                                return null;
                            }
                        };
                    }
                };
                service.start();
            }
            else{
                socket3dMp4 = socket;
                differentMediaPlayer = true;
                singleTon.setMp43dplayer(true);
                imagePaneDefault.setScaleX(0);
                imagePaneDefault.setScaleY(0);
                
                Task<Integer> task = new Task<Integer>(){
                    
                    @Override
                    protected Integer call() throws Exception{
                        int iterations=0;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                stackpaneGifDefault.setScaleX(1);
                                stackpaneGifDefault.setScaleY(1);
                                stackpaneGifFS.setScaleX(1);
                                stackpaneGifFS.setScaleY(1);
                                /*AKASH*/stackpaneGifFS.toFront();/*AKASH*/
                            }
                        });
                        
                        decryptionQueue3d = DecryptionQueue3d.getDecryption();
                        while(!decryptionQueue3d.isCheckFile3D()){
                            try {
                                check= true;
                                System.out.println("Checkfile3d");
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                singleTon = SingleTon.getSingleTon();
                                File file = singleTon.getMediaFile3D();
                                mp3D = new MediaPlayer(new Media(file.toURI().toString()));
                                mp3D.setAutoPlay(true);
                                currentMediaPlayer = false;
                                stackpaneGifDefault.setScaleX(0);
                                stackpaneGifDefault.setScaleY(0);
                                showMediaDefault.setScaleX(0);
                                showMediaDefault.setScaleY(0);
                                showMediaDefault3d.setScaleX(1);
                                showMediaDefault3d.setScaleY(1);
                                showMediaDefault3d.setMediaPlayer(mp3D);//ScaleY(1);
                                showMediaIntermediate3d.setMediaPlayer(mp3D);
                                mp = mp3D;
                                PlayMediaPlayer(showMediaDefault3d, showMediaIntermediate3d, sliderDefault3d, progressbarDefault3d, labelTime3d);
                                stackpaneGifFS.setScaleX(0);
                                stackpaneGifFS.setScaleY(0);
                                stackpaneGifDefault.setStyle("-fx-background-color:rgb(0,0,0,0.5)");
                            }
                        });
                        return iterations;
                    }
                };
                Thread th = new Thread(task);
                th.setDaemon(true);
                th.start();
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    public void showWeb(){
        singleTon = singleTon.getSingleTon();
        while(!singleTon.isResponse()){
            try {
                System.out.println("Response");
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        singleTon = SingleTon.getSingleTon();
        while(singleTon.getPdfByte()==null){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
        showWebView.setText(convertPDFToText());
    }

    public String convertPDFToText(){
        String data = "";
        try{
            singleTon = singleTon.getSingleTon();
            PdfReader pr=new PdfReader(new ByteArrayInputStream(singleTon.getPdfByte()));
            int pNum=pr.getNumberOfPages();
            for(int page=1;page<=pNum;page++){
                String text=PdfTextExtractor.getTextFromPage(pr, page);
                text = text.replace("\n", "").replace("\r", "");
                data = data + text;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return data;
    }
    
    @FXML
    private void OnShowingChapter(Event event) {
        System.out.println("At on shown Chapter");
        singleTon = SingleTon.getSingleTon();
        if(!singleTon.isSearch()){
            selectChapter.getItems().clear();
            while(singleTon.getChapterList()==null){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int size = singleTon.getChapterList().size();
            for(int i=0;i<size;i++){
                selectChapter.getItems().add(singleTon.getChapterList().get(i));
            }
        }
    }

    @FXML
    private void OnShowingLesson(Event event) {
        System.out.println("At on shown Lesson");
        singleTon = SingleTon.getSingleTon();
        if(!singleTon.isSearch()){
            selectLesson.getItems().clear();
            while(singleTon.getLessonList()==null){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int size = singleTon.getLessonList().size();
            for(int i=0;i<size;i++){
                selectLesson.getItems().add(singleTon.getLessonList().get(i));
            }
        }
    }
    
    public void PlayMediaPlayer(MediaView showMediaDefault, MediaView showMediaIntermediate, Slider sliderDefault, ProgressBar progressbarDefault, Label labelTime){
        SetMediaView(showMediaDefault); 
//        mp.
//        showMediaDefault.setMediaPlayer(mp);
//        showMediaIntermediate.setMediaPlayer(mp);
        
//        System.out.println("d..................."+d);
        volumeSliderDefault.setValue(mp.getVolume());
        progressbarVolumeDefault.setProgress(volumeSliderDefault.getValue());
        sliderVolumeFullscreen.setValue(volumeSliderDefault.getValue());
        sliderVolumeFullscreen.setValue(mp.getVolume());
        clientSocket = ClientSocket.getClientSocket();

        Duration dur = mp.getTotalDuration();
        System.out.println("Dur "+dur.toSeconds());
       
        /*AKASH*/mp.statusProperty().addListener(new ChangeListener<MediaPlayer.Status>() {

            @Override
            public void changed(ObservableValue<? extends MediaPlayer.Status> observable, MediaPlayer.Status oldValue, MediaPlayer.Status newValue) {
                if(mp!=null){
                if(mp==mp2d){
                    System.err.println("2d"+newValue);
                    if(newValue==MediaPlayer.Status.PLAYING){
//                        playDefault.setStyle();
                         playDefault.setStyle("-fx-background-color: null;");
                        playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_20.png"))));
                        
                    }else{
                         playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_20.png"))));
                        playDefault.setStyle("-fx-background-color: null;");
                    }
                }else{
                    System.err.println("3d"+newValue);
                     if(newValue==MediaPlayer.Status.PLAYING){
//                        playDefault.setStyle();
                         playDefault.setStyle("-fx-background-color: null;");
                        playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_20.png"))));
                        
                    }else{
                         playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_20.png"))));
                        playDefault.setStyle("-fx-background-color: null;");
                    }
                }
                }
            }
        });/*AKASH*/
        mp.currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observable, Duration duration, Duration currentDuration) {
                sliderDefault.setValue(currentDuration.toSeconds());
                sliderIntermediate.setValue(currentDuration.toSeconds());
                sliderFullscreen.setValue(currentDuration.toSeconds());
                sliderDefault.setCursor(Cursor.HAND);
                sliderIntermediate.setCursor(Cursor.HAND);
                sliderFullscreen.setCursor(Cursor.HAND);
                setLabelTime(currentDuration,duration);
                if(currentMediaPlayer){
                    if(singleTon.getMp4Packate()!=-1){
                        int mp4packate = singleTon.getMp4Packate();
                        if(((int)currentDuration.toSeconds())>=((19*singleTon.getMp4Packate())-2)){
                            System.out.println("pausemovie");
                            mp.pause();
                            stackpaneGifDefault.setScaleX(1);
                            stackpaneGifDefault.setScaleY(1);
                            stackpaneGifFS.setScaleX(1);
                            stackpaneGifFS.setScaleY(1);
                            /*AKASH*/stackpaneGifFS.toFront();/*AKASH*/
                            playDefault.setDisable(true);
                            sliderDefault.setDisable(true);
                            playFullscreen.setDisable(true);
                            sliderFullscreen.setDisable(true);
                            mediaPlayerStatus = false;
                            
                            Task<Integer> task = new Task<Integer>(){

                                @Override
                                protected Integer call() throws Exception{
                                    int iterations=0;
                                    while(mp4packate==singleTon.getMp4Packate()){
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    Platform.runLater(new Runnable() {                          
                                        @Override
                                        public void run() {
                                            mp.play();
                                            mediaPlayerStatus = true;
                                            stackpaneGifDefault.setScaleX(0);
                                            stackpaneGifDefault.setScaleY(0);
                                            stackpaneGifFS.setScaleX(0);
                                            stackpaneGifFS.setScaleY(0);
                                            playDefault.setDisable(false);
                                            sliderDefault.setDisable(false);
                                            playFullscreen.setDisable(false);
                                            sliderFullscreen.setDisable(false);
                                        }
                                    });
                                    return iterations;
                                }
                            };
                            Thread th = new Thread(task);
                            th.setDaemon(true);
                            th.start();
//                            new Thread(new Runnable() {
//
//                                @Override
//                                public void run() {                                    
//                                    while(mp4packate==singleTon.getMp4Packate()){
//                                        try {
//                                            Thread.sleep(500);
//                                        } catch (InterruptedException ex) {
//                                            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
//                                        }
//                                    }
//                                    mp.play();
//                                    mediaPlayerStatus = true;
//                                    stackpaneGifDefault.setScaleX(0);
//                                    stackpaneGifDefault.setScaleY(0);
//                                    playDefault.setDisable(false);
//                                    sliderDefault.setDisable(false);
//                                }
//                            }).start();
                        }
                    }
                }else{
                    if(singleTon.getMp43dPackate()!=-1){
                        int mp43dpackate = singleTon.getMp43dPackate();
                        if(((int)currentDuration.toSeconds())>=((19*singleTon.getMp43dPackate())-2)){
                            System.out.println("pausemovie");
                            mediaPlayerStatus = false;
                            mp.pause();
                            stackpaneGifDefault.setScaleX(1);
                            stackpaneGifDefault.setScaleY(1);
                            stackpaneGifFS.setScaleX(1);
                            stackpaneGifFS.setScaleY(1);
                            /*AKASH*/stackpaneGifFS.toFront();/*AKASH*/
                            playDefault.setDisable(true);
                            sliderDefault.setDisable(true);
                            playFullscreen.setDisable(true);
                            sliderFullscreen.setDisable(true);
                            mediaPlayerStatus = false;
                            
                            Task<Integer> task = new Task<Integer>(){

                                @Override
                                protected Integer call() throws Exception{
                                    int iterations=0;
                                    while(mp43dpackate==singleTon.getMp43dPackate()){
                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(SubjectController.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    Platform.runLater(new Runnable() {                          
                                        @Override
                                        public void run() {
                                            mp.play();
                                            mediaPlayerStatus = true;
                                            stackpaneGifDefault.setScaleX(0);
                                            stackpaneGifDefault.setScaleY(0);
                                            stackpaneGifFS.setScaleX(0);
                                            stackpaneGifFS.setScaleY(0);
                                            playDefault.setDisable(false);
                                            sliderDefault.setDisable(false);
                                            playFullscreen.setDisable(false);
                                            sliderFullscreen.setDisable(false);
                                        }
                                    });
                                    return iterations;
                                }
                            };
                            Thread th = new Thread(task);
                            th.setDaemon(true);
                            th.start();
                        }
                    }
                }
            }

            private void setLabelTime(Duration currentDuration, Duration duration) {
//                if(Endtime < (int)currentDuration.toSeconds()){
//                    list = readSrt.readSubtitle((int)currentDuration.toSeconds());
//                    Starttime = Integer.parseInt(list.get(0)+"");
//                    Endtime = Integer.parseInt(list.get(1)+"");
//                    Endtime=Endtime/1000;
//                    Subtitle = list.get(2)+"";
//                    labelSubtitle.setText(Subtitle);
//                    labelFSSubtitle.setText(Subtitle);
//                }
                if((int)currentDuration.toSeconds()==0){
                    sliderDefault.setMax(mp.getTotalDuration().toSeconds());
                    totalDuration = mp.getTotalDuration().toSeconds();
                    sliderFullscreen.setMax(mp.getTotalDuration().toSeconds());
                }
                String time= String.format("%02d:%02d",(int)currentDuration.toMinutes(),((int)currentDuration.toSeconds())%60);
                labelTime.setText(time);
                labelTime.setText(labelTime.getText().concat("/0"+((int)(totalDuration/60))+":"+((int)(totalDuration%60))));

                labelFSTime.setText(time);
                labelFSTime.setText(labelFSTime.getText().concat("/0"+((int)(totalDuration/60))+":"+((int)(totalDuration%60))));
            }
        });
        
        if(mediaPlayerStatus){
            showMediaIntermediate.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println("Click>>");
                    if(mp.getStatus().toString().equals("PLAYING"))
                    {
                        mp.pause();
                        playFullscreen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_25.png"))));
                        playFullscreen.setStyle("-fx-background-color: null;");
//                    playFullscreen.setStyle("-fx-shape: \"M5,0 L10,8 L5,15 Z\";");
                    }else{
                        playFullscreen.setStyle("-fx-background-color: null;");
                        playFullscreen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_25.png"))));
//                        playFullscreen.setStyle("-fx-shape: \"M200,110 L200,200 L230,200 L230,110 L160,110 L160,200 L190,200 L190,110 Z\";");
                        mp.play();
                    }
                }
            });
            showMediaDefault.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(mp.getStatus().toString().equals("PLAYING"))
                    {
                        mp.pause();
                    }else{
                        mp.play();
                    }
                }
            });

            playDefault.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    if(mp.getStatus().toString().equals("PLAYING")){
                        mp.pause();
                        /*playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_20.png"))));
                        playDefault.setStyle("-fx-background-color: null;");*/
//                        playDefault.setStyle("-fx-shape: \"M5,0 L10,8 L5,15 Z\";");
                    }else{
                        System.out.println("buttonclicked");
                       /* playDefault.setStyle("-fx-background-color: null;");
                        playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_20.png"))));*/
//                        playDefault.setStyle("-fx-shape: \"M200,110 L200,200 L230,200 L230,110 L160,110 L160,200 L190,200 L190,110 Z\";");
                        mp.play();
                    }
                }
            });
            playIntermediate.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    if(mp.getStatus().toString().equals("PLAYING")){
                        mp.pause();
                    }else{
                        mp.play();
                    }
                }
            });
            playFullscreen.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    if(mp.getStatus().toString().equals("PLAYING")){
                        mp.pause();
                        playFullscreen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_25.png"))));
                        playFullscreen.setStyle("-fx-background-color: null;");
                    }else{
                        System.out.println("fulscreen button ");
                        playFullscreen.setStyle("-fx-background-color: null;");
                        playFullscreen.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_25.png"))));
                        mp.play();
                    }
                }
            });
            sliderDefault.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
//                    int num =(int) (((1/totalDuration)*(new_val.doubleValue()))*10);
                    progressbarDefault.setProgress(((1/totalDuration)*(new_val.doubleValue())));
                    progressbarFS.setProgress(((1/totalDuration)*(new_val.doubleValue())));
                }
            });
            volumeSliderDefault.setOnMousePressed(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    mp.getVolume();
                    System.out.println("volume:"+mp.getVolume());
                }
            });
            volumeSliderDefault.setOnMouseReleased(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    mp.setVolume(volumeSliderDefault.getValue());
                    progressbarVolumeDefault.setProgress(volumeSliderDefault.getValue());
                    sliderVolumeFullscreen.setValue(volumeSliderDefault.getValue());
                }
            });
            sliderDefault.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    mp.pause();
                }
            });

            sliderDefault.setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    mp.seek(Duration.seconds(sliderDefault.getValue()));
                    mp.play();
                }
            });

            sliderIntermediate.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    mp.pause();
                }
            });

            sliderIntermediate.setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    mp.seek(Duration.seconds(sliderIntermediate.getValue()));
                    mp.play();
                }
            });

            //Action on slider for full screen
            sliderVolumeFullscreen.setOnMousePressed(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    mp.getVolume();
                }
            });
            sliderVolumeFullscreen.setOnMouseReleased(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    mp.setVolume(sliderVolumeFullscreen.getValue());
                    progressbarVolumeFS.setProgress(sliderVolumeFullscreen.getValue());
                    volumeSliderDefault.setValue(sliderVolumeFullscreen.getValue());
                    progressbarDefault.setProgress(volumeSliderDefault.getValue());
                }
            });
            sliderFullscreen.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    mp.pause();
                }
            });

            sliderFullscreen.setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    mp.seek(Duration.seconds(sliderFullscreen.getValue()));
                    mp.play();
                }
            });

            showMediaDefault.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getClickCount()==2){
                        setFullScreen(showMediaIntermediate);
                    }else{
                        if(mp.getStatus().toString().equals("PLAYING")) {
                            mp.pause();
                            /*playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("play_1facff_20.png"))));
                            playDefault.setStyle("-fx-background-color: null;");*/
                        }else {/*
                            playDefault.setStyle("-fx-background-color: null;");
                            playDefault.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("pause_1facff_20.png"))));*/
                            mp.play();
                        }
                    }
                }
            });
        }
    }

    @FXML
    private void searchContent(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);

        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());        
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            System.out.println("<<<<<<<<<<<<<<<<");
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            Stage stage = (Stage) search.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    private void SetMediaView(MediaView showMediaDefault) {
        
        showMediaDefault.setPreserveRatio(false);
        showMediaDefault.setFitWidth((anchorPanecontainDefaultMedia.getWidth())-10);
        showMediaDefault.setFitHeight((anchorPanecontainDefaultMedia.getHeight())-57);
    }

    @FXML
    private void subtitleAction(ActionEvent event) {
        readSrt = new ReadSrt();
        if(Endtime < (int)sliderDefault.getValue()){
            list = readSrt.readSubtitle((int)sliderDefault.getValue());
            Starttime = Integer.parseInt(list.get(0)+"");
            Endtime = Integer.parseInt(list.get(1)+"");
            Endtime=Endtime/1000;
            Subtitle = list.get(2)+"";
            labelSubtitle.setText(Subtitle);
            labelFSSubtitle.setText(Subtitle);
        }

        if(labelSubtitle.getOpacity()==0)
            labelSubtitle.setOpacity(1);
        else
            labelSubtitle.setOpacity(0);
    }

    @FXML
    private void subtitleFSAction(ActionEvent event) {
         if(labelFSSubtitle.getOpacity()==0)
            labelFSSubtitle.setOpacity(1);
        else
            labelFSSubtitle.setOpacity(0);
    }

    @FXML
    private void btnAction3DMedia(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setMp4(false);
        sliderDefault3d.setScaleX(1);
        sliderDefault3d.setScaleY(1);
        progressbarDefault3d.setScaleX(1);
        progressbarDefault3d.setScaleY(1);
        progressbarDefault.setScaleX(0);
        progressbarDefault.setScaleY(0);
        labelTime3d.setScaleX(1);
        labelTime3d.setScaleY(1);
        labelTime.setScaleX(0);
        labelTime.setScaleY(0);
        sliderDefault.setScaleX(0);
        sliderDefault.setScaleY(0);
        btn3DMedia.setStyle("-fx-effect: dropshadow( three-pass-box , white , 10, 0.3 , 0 , 0 );-fx-background-color:null;");
       btnNormalMedia.setStyle("-fx-effect:null;-fx-background-color:null;");
//        mpText = labelTime.getText();
        if(mp2d!=null)
            mp2d.pause();
        if(!differentMediaPlayer){
            imagePaneDefault.setScaleX(1);
            imagePaneDefault.setScaleY(1);
            chooseMedia = "3dmp4";
            
//            sliderDefault.setValue(0.0);
//            sliderFullscreen.setValue(0.0);
//            sliderIntermediate.setValue(0.0);
//            
//            progressbarDefault.setProgress(0.0);
//            progressbarFS.setProgress(0.0);
            mpSlider = sliderDefault.getValue();
        }else{
            mpSlider = sliderDefault.getValue();
            clientSocket = ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getMp4requestQueue();
            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
            continueRequest.setHash(clientSocket.getHash());
            continueRequest.setFileType("3dmp4");
            continueRequest.setMessageType("MediaDataContinueRequest");
//            queue.add(continueRequest);
//            showMediaDefault.setMediaPlayer(mp3D);
            currentMediaPlayer = false;
            stackpaneGifDefault.setStyle("-fx-background-color:rgb(0,0,0,0.5)");
            showMediaDefault3d.setScaleX(1);
            showMediaDefault3d.setScaleY(1);
            showMediaDefault.setScaleX(0);
            showMediaDefault.setScaleY(0);
            mp = mp3D;
            PlayMediaPlayer(showMediaDefault3d, showMediaIntermediate3d, sliderDefault3d, progressbarDefault3d, labelTime3d);
        }   
    }

    @FXML
    private void btnActionNomal(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setMp4(true);
        sliderDefault3d.setScaleX(0);
        sliderDefault3d.setScaleY(0);
        sliderDefault.setScaleX(1);
        sliderDefault.setScaleY(1);
        progressbarDefault3d.setScaleX(0);
        progressbarDefault3d.setScaleY(0);
        progressbarDefault.setScaleX(1);
        progressbarDefault.setScaleY(1);
        labelTime3d.setScaleX(0);
        labelTime3d.setScaleY(0);
        labelTime.setScaleX(1);
        labelTime.setScaleY(1);
//        mp3dText = labelTime.getText();
       btnNormalMedia.setStyle("-fx-effect: dropshadow( three-pass-box , white , 10, 0.3 , 0 , 0 );-fx-background-color:null;");
       btn3DMedia.setStyle("-fx-effect:null;-fx-background-color:null;");
        if(mp3D!=null)
            mp3D.pause();
        
        if(!normalMediaPlayer){
            imagePaneDefault.setScaleX(1);
            imagePaneDefault.setScaleY(1);
            chooseMedia = "mp4";
            mp3dSlider = sliderDefault.getValue();
        }else{
            mp3dSlider = sliderDefault.getValue();
            clientSocket = ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getMp4requestQueue();
            MediaDataContinueRequest continueRequest = new MediaDataContinueRequest();
            continueRequest.setHash(clientSocket.getHash());
            continueRequest.setFileType("mp4");
            continueRequest.setMessageType("MediaDataContinueRequest");
//            queue.add(continueRequest);
//            showMediaDefault.setMediaPlayer(mp);
            currentMediaPlayer = true;
            showMediaDefault3d.setScaleX(0);
            showMediaDefault3d.setScaleY(0);
            showMediaDefault.setScaleX(1);
            showMediaDefault.setScaleY(1);
            mp = mp2d;
            PlayMediaPlayer(showMediaDefault, showMediaIntermediate2d, sliderDefault, progressbarDefault, labelTime);
            stackpaneGifDefault.setStyle("-fx-background-color:rgb(0,0,0,0.5)");
        }
    }

    @FXML
    private void labelHomeAction(MouseEvent event) {
        releaseFiles();
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        releaseFiles();
         singleTon=SingleTon.getSingleTon();
        Stage stage = (Stage) btnBack.getScene().getWindow();
        if(singleTon.isSearch()){
            System.out.println("Search");
            pageSwitch.nextScreen(stage, "Search.fxml");
        }else{
            System.out.println("Subjectlist");
            pageSwitch.nextScreen(stage, "SubjectList.fxml");
        }
    }

    @FXML
    private void labelLogoutAction(MouseEvent event) {
        releaseFiles();
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
