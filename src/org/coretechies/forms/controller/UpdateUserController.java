/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.ChangePasswordRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdateUser;
import org.coretechies.MessageTransporter.UpdateUserInfoRequest;
import org.coretechies.forms.controller.java.UpdateUserView;

/**
 * FXML Controller class
 *
 * @author user
 */
public class UpdateUserController implements Initializable {
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    
    @FXML
    private TableView<UpdateUserView> updateUser;
    @FXML
    private TableColumn columnUsername;
    @FXML
    private TableColumn columnFirstname;
    @FXML
    private TableColumn columnLastname;
    @FXML
    private TableColumn columnPassword;
    @FXML
    private TableColumn columnUpdate;
    @FXML
    private TableColumn columnDelete;
    ObservableList<UpdateUserView> data;

    SingleTon singleTon;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelUpdateUser;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pageSwitch = new PageSwitch();
        columnUsername.setCellValueFactory(
            new PropertyValueFactory<UpdateUserView,String>("username")
        );
        columnFirstname.setCellValueFactory(
            new PropertyValueFactory<UpdateUserView,String>("firstname")
        );
        columnLastname.setCellValueFactory(
            new PropertyValueFactory<UpdateUserView,String>("lastname")
        );
        columnPassword.setCellValueFactory(
            new PropertyValueFactory<UpdateUserView,String>("password")
        );
        columnUpdate.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<UpdateUserView, Boolean>, 
            ObservableValue<Boolean>>() {
 
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<UpdateUserView, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
         
        columnUpdate.setCellFactory(
                new Callback<TableColumn<UpdateUserView, Boolean>, TableCell<UpdateUserView, Boolean>>() {
 
           

            @Override
            public TableCell<UpdateUserView, Boolean> call(TableColumn<UpdateUserView, Boolean> param) {
                 return new CreateButtonUpdate();
            }
        
        });
        columnDelete.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<UpdateUserView, Boolean>, 
            ObservableValue<Boolean>>() {
 
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<UpdateUserView, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
         
        columnDelete.setCellFactory(
            new Callback<TableColumn<UpdateUserView, Boolean>, TableCell<UpdateUserView, Boolean>>() {

            @Override
            public TableCell<UpdateUserView, Boolean> call(TableColumn<UpdateUserView, Boolean> param) {
                 return new CreateButtonDelete();
            }
        
        });
        data = FXCollections.observableArrayList();
        updateUser.setItems(data);

        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        while(singleTon.getUpdateUserList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(UpdateUserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getUpdateUserList().size();
        for(int i=0; i<size; i++){
            System.out.println("DATA");
            UpdateUserView updateUserView = new UpdateUserView();
            
            updateUserView.setUserId(singleTon.getUpdateUserList().get(i).getUserId());
            updateUserView.username.setValue(singleTon.getUpdateUserList().get(i).getUserName());
            updateUserView.firstname.setValue(singleTon.getUpdateUserList().get(i).getFirstName());
            updateUserView.lastname.setValue(singleTon.getUpdateUserList().get(i).getLastName());
            updateUserView.password.setValue(singleTon.getUpdateUserList().get(i).getPassword());
            
            data.add(updateUserView);

        }
    }    

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
        
    public class CreateButtonUpdate extends TableCell<UpdateUserView, Boolean> {
        final Button updateButton = new Button("Update");
        
        public CreateButtonUpdate(){
            updateButton.setCursor(Cursor.HAND);
            System.out.println(CreateButtonUpdate.this.getIndex());
            updateButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    UpdateUserView item = (UpdateUserView) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    
                    ClientSocket clientSocket =ClientSocket.getClientSocket();
                    
                    Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                    System.out.println("<<<<<<<<<<<<<<<<");
                    UpdateUserInfoRequest updateUserInfoRequest = new UpdateUserInfoRequest();
                    updateUserInfoRequest.setHash(clientSocket.getHash());
                    updateUserInfoRequest.setId(item.getUserId());
                    
                    updateUserInfoRequest.setMessageType("UpdateUserDataRequest");
                    queue.add(updateUserInfoRequest);

                    singleTon = SingleTon.getSingleTon();
                    singleTon.setCreateUser(2);
                    singleTon.setUpdateUserId(item.getUserId());
                    
                    Stage stage = (Stage) btnBack.getScene().getWindow();
                    pageSwitch.nextScreen(stage, "CreateUserSchool.fxml");                      
                }
            });
        }
 
        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
                updateButton.setStyle("-fx-background-color:#008cba;-fx-text-fill: white;");
            }
        }
    }

    public class CreateButtonDelete extends TableCell<UpdateUserView, Boolean> {
        final Button deleteButton = new Button("Change");
        
        public CreateButtonDelete(){
    
            System.out.println(CreateButtonDelete.this.getIndex());
            deleteButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    UpdateUserView item = (UpdateUserView) CreateButtonDelete.this.getTableView().getItems().get(CreateButtonDelete.this.getIndex());
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setUpdateUserName(item.getUsername());
                    singleTon.setUpdateUserId(item.getUserId());
                    
                    Stage stage = (Stage) btnBack.getScene().getWindow();
                    pageSwitch.nextScreen(stage, "Update.fxml");
                }
            });
        }
 
        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(deleteButton);
                deleteButton.setStyle("-fx-background-color:#008cba;-fx-text-fill: white;");
            }
        }
    }
}