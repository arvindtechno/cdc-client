/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.AddNewLessonChapterDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonLessonDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonSubjectDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonTopicDataRequest;
import org.coretechies.MessageTransporter.CreateLesson;
import org.coretechies.MessageTransporter.CreateNewLessonRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.forms.controller.java.AddLessonView;
//import org.coretechies.forms.controller.java.DatePickerCell;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AddLessonController implements Initializable {
    @FXML
    private ComboBox<String> selectCurriculum;
    @FXML
    private ComboBox<String> selectGrade;
    @FXML
    private ComboBox<String> selectChapter;
    @FXML
    private ComboBox<String> selectSubject;
    @FXML
    private ComboBox<String> selectLesson;
    @FXML
    private Button btnCreateLesson;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    ObservableList<AddLessonView> data;
    @FXML
    private TableView<AddLessonView> tableTopic;
    @FXML
    private TableColumn columnTopic;
    @FXML
    private TableColumn columnClassDate;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
        
    private SingleTon singleTon;
    private ClientSocket clientSocket;
    private PageSwitch pageSwitch;
    @FXML
    private HBox ERRORHbox;
    @FXML
    private Text textErrorInfo;
    @FXML
    private Text textMessageStart;
    @FXML
    private Text textDate;
    @FXML
    private Text textMessageLast;
    @FXML
    private Button btnCloseError;
    @FXML
    private TableColumn columnDelete;
    @FXML
    private Text userName;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
        pageSwitch = new PageSwitch();
        clientSocket = ClientSocket.getClientSocket();
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    
        int size = singleTon.getCurriculumList().size();
        for(int i=0;i<size;i++){
            selectCurriculum.getItems().add(singleTon.getCurriculumList().get(i));
        }
        
        size = singleTon.getGradeList().size();
        for(int i=0;i<size;i++){
            selectGrade.getItems().add(singleTon.getGradeList().get(i));
        }
    }    

    @FXML
    private void curriculum(ActionEvent event) {
        if(!(selectGrade.getSelectionModel().getSelectedItem()==null && selectCurriculum.getSelectionModel().getSelectedItem()==null)){
            
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            AddNewLessonSubjectDataRequest addNewLessonSubjectDataRequest = new AddNewLessonSubjectDataRequest();
            addNewLessonSubjectDataRequest.setHash(clientSocket.getHash());
            addNewLessonSubjectDataRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
            addNewLessonSubjectDataRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
            addNewLessonSubjectDataRequest.setMessageType("AddNewLessonSubjectRequest");
            queue.add(addNewLessonSubjectDataRequest);
        }
    }

    @FXML
    private void grade(ActionEvent event) {
        if(!(selectGrade.getSelectionModel().getSelectedItem()==null && selectCurriculum.getSelectionModel().getSelectedItem()==null)){
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            AddNewLessonSubjectDataRequest addNewLessonSubjectDataRequest = new AddNewLessonSubjectDataRequest();
            addNewLessonSubjectDataRequest.setHash(clientSocket.getHash());
            addNewLessonSubjectDataRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
            addNewLessonSubjectDataRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
            addNewLessonSubjectDataRequest.setMessageType("AddNewLessonSubjectRequest");
            queue.add(addNewLessonSubjectDataRequest);
        }
    }

    @FXML
    private void chapter(ActionEvent event) {
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        AddNewLessonLessonDataRequest addNewLessonLessonDataRequest = new AddNewLessonLessonDataRequest();
        addNewLessonLessonDataRequest.setHash(clientSocket.getHash());
        addNewLessonLessonDataRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
        addNewLessonLessonDataRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
        addNewLessonLessonDataRequest.setSubject(selectSubject.getSelectionModel().getSelectedItem());
        addNewLessonLessonDataRequest.setChapter(selectChapter.getSelectionModel().getSelectedItem());
        addNewLessonLessonDataRequest.setMessageType("AddNewLessonLessonRequest");
        queue.add(addNewLessonLessonDataRequest);
    }

    @FXML
    private void subject(ActionEvent event) {
        
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        AddNewLessonChapterDataRequest addNewLessonChapterDataRequest = new AddNewLessonChapterDataRequest();
        addNewLessonChapterDataRequest.setHash(clientSocket.getHash());
        addNewLessonChapterDataRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
        addNewLessonChapterDataRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
        addNewLessonChapterDataRequest.setSubject(selectSubject.getSelectionModel().getSelectedItem());
        addNewLessonChapterDataRequest.setMessageType("AddNewLessonChapterRequest");
        queue.add(addNewLessonChapterDataRequest);
    }

    @FXML
    private void lesson(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLeaveDateList(null);
        
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        AddNewLessonTopicDataRequest addNewLessonTopicDataRequest = new AddNewLessonTopicDataRequest();
        addNewLessonTopicDataRequest.setHash(clientSocket.getHash());
        addNewLessonTopicDataRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
        addNewLessonTopicDataRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
        addNewLessonTopicDataRequest.setSubject(selectSubject.getSelectionModel().getSelectedItem());
        addNewLessonTopicDataRequest.setChapter(selectChapter.getSelectionModel().getSelectedItem());
        addNewLessonTopicDataRequest.setLesson(selectLesson.getSelectionModel().getSelectedItem());
        addNewLessonTopicDataRequest.setMessageType("AddNewLessonTopicRequest");
        queue.add(addNewLessonTopicDataRequest);        
        
        columnTopic.setCellValueFactory(
            new PropertyValueFactory<AddLessonView,String>("topic")
        );
        columnClassDate.setCellValueFactory(
            new PropertyValueFactory<AddLessonView,String>("classDate")
        );
	columnClassDate.setCellValueFactory(new PropertyValueFactory<AddLessonView, Date>("date"));
        columnClassDate.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn p) {
                DatePickerCell datePick = new DatePickerCell(data);
                return datePick;
            }
        });
        
        columnDelete.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<AddLessonView, Boolean>, ObservableValue<Boolean>>() {
            
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<AddLessonView, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        
        columnDelete.setCellFactory(
            new Callback<TableColumn<AddLessonView, Boolean>, TableCell<AddLessonView, Boolean>>() {
 
            @Override
            public TableCell<AddLessonView, Boolean> call(TableColumn<AddLessonView, Boolean> param) {
                return new CreateButtonUpdate();
            }
        });
        
        data = FXCollections.observableArrayList();
        tableTopic.setItems(data);
        
        singleTon = SingleTon.getSingleTon();
        try { 
            while(!singleTon.isResponse()){
                Thread.sleep(50);
            }Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        int size = singleTon.getAddNewLessonTopicList().size();  
        
        for(int i=0;i<size;i++) {
            System.out.println("DATA");
            AddLessonView item = new AddLessonView((String)singleTon.getAddNewLessonTopicList().get(i).getTopic(), new Date());
            item.setId(singleTon.getAddNewLessonTopicList().get(i).getId());
            item.setStatus(false);
            data.add(item);
        }
    }

    @FXML
    private void createLesson(ActionEvent event) {
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        CreateNewLessonRequest createNewLessonRequest = new CreateNewLessonRequest();
        createNewLessonRequest.setHash(clientSocket.getHash());
        createNewLessonRequest.setCurriculum(selectCurriculum.getSelectionModel().getSelectedItem());
        createNewLessonRequest.setGrade(selectGrade.getSelectionModel().getSelectedItem());
        createNewLessonRequest.setSubject(selectSubject.getSelectionModel().getSelectedItem());
        createNewLessonRequest.setChapter(selectChapter.getSelectionModel().getSelectedItem());
        createNewLessonRequest.setLesson(selectLesson.getSelectionModel().getSelectedItem());
        
        ArrayList<CreateLesson> createLessons = new ArrayList<>();
        int size = tableTopic.getItems().size();
        boolean stop = true;
        for(int i=0; i<size; i++){
            Date date = tableTopic.getItems().get(i).getDate();
            if(tableTopic.getItems().get(i).isStatus()){
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                System.out.println("DATE : "+date);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                String dateString = year+"/"+(month+1)+"/"+day;
                System.out.println("DATE STRING : "+dateString);
                boolean b = false;
                if(date.getDay()==1){
                    for(Date d : singleTon.getDateList()){
                        if(d.toString().equals(date.toString()))
                            i++;
                    }
                    if(singleTon.getScheduleList().get(0)>i){
                        b = true;
                    }
                }else if (date.getDay()==2) {
                    for(Date d : singleTon.getDateList()){
                        if(d.toString().equals(date.toString()))
                            i++;
                    }
                    if(singleTon.getScheduleList().get(1)>i){
                        b = true;
                    }
                }else if (date.getDay()==3) {
                    for(Date d : singleTon.getDateList()){
                        if(d.toString().equals(date.toString()))
                            i++;
                    }
                    if(singleTon.getScheduleList().get(2)>i){
                        b = true;
                    }
                }else if (date.getDay()==4) {
                    for(Date d : singleTon.getDateList()){
                        if(d.toString().equals(date.toString()))
                            i++;
                    }
                    if(singleTon.getScheduleList().get(3)>i){
                        b = true;
                    }
                }else if (date.getDay()==5) {
                    for(Date d : singleTon.getDateList()){
                        if(d.toString().equals(date.toString()))
                            i++;
                    }
                    if(singleTon.getScheduleList().get(4)>i){
                        b = true;
                    }
                }
                if(b){
                    CreateLesson createLesson = new CreateLesson();
                    createLesson.setTopic(tableTopic.getItems().get(i).getTopic());
                    createLesson.setDate(dateString);
                    createLessons.add(createLesson);
                }else{
                    ERRORHbox.setScaleX(1);
                    ERRORHbox.setScaleY(1);//ERROR: All lessons were completely scheduled for the day, 05/26/14. Please try another date.
                    textErrorInfo.setFill(new Color(1, 0, 0, 1));
                    textErrorInfo.setText("ERROR: ");
                    textMessageStart.setText("All lessons were completely scheduled for the day, ");
                    textDate.setFill(new Color(1, 0, 0, 1));
                    textDate.setText(dateString);
                    textMessageLast.setText("");
                    stop = false;
                    break;
                }
            }
        }
        if(createLessons.size()>0){
            createNewLessonRequest.setTopicList(createLessons);
            createNewLessonRequest.setMessageType("CreateNewLessonRequest");
            singleTon = SingleTon.getSingleTon();
            singleTon.setResponse(false);
            queue.add(createNewLessonRequest);
            while(!singleTon.isResponse()){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(AddLessonController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            textErrorInfo.setFill(new Color(0.3216, 0.502, 0.2039, 1));
            textErrorInfo.setText("Info: ");
            textMessageStart.setText(" You can assign a new lesson now on ");
            textMessageLast.setText("");
            textDate.setText("");
            for(String s : singleTon.getDateStringList())
                textDate.setText(textDate.getText()+", "+s);
            
            ERRORHbox.setScaleX(1);
            ERRORHbox.setScaleY(1);
        }else if(stop){
            textErrorInfo.setFill(new Color(0.3216, 0.502, 0.2039, 1));
            textErrorInfo.setText("Info: ");
            textMessageStart.setText("Please select minimum one Lesson by check on checkbox");
            textMessageLast.setText("");
            textDate.setText("");
            
            ERRORHbox.setScaleX(1);
            ERRORHbox.setScaleY(1);
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void OnShowingChapter(Event event) {
        selectChapter.getItems().clear();
        if(!(selectCurriculum.getSelectionModel().getSelectedItem()==null && selectGrade.getSelectionModel().getSelectedItem()==null && selectSubject.getSelectionModel().getSelectedItem()==null)){

            try {
                while(!singleTon.isResponse()){}
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(AddLessonController.class.getName()).log(Level.SEVERE, null, ex);
            }
            int size = singleTon.getAddNewLessonChapterList().size();
            for(int i=0;i<size;i++){
                selectChapter.getItems().add(singleTon.getAddNewLessonChapterList().get(i));
            }
        }
    }

    @FXML
    private void OnShowingSubject(Event event) {
        selectSubject.getItems().clear();
        if(!(selectCurriculum.getSelectionModel().getSelectedItem()==null && selectGrade.getSelectionModel().getSelectedItem()==null)){
            try {
                while(!singleTon.isResponse()){}
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(AddLessonController.class.getName()).log(Level.SEVERE, null, ex);
            }
            int size = singleTon.getAddNewLessonSubjectList().size();
            for(int i=0;i<size;i++){
                selectSubject.getItems().add(singleTon.getAddNewLessonSubjectList().get(i));
            }
        }
    }

    @FXML
    private void OnShowingLesson(Event event) {
        selectLesson.getItems().clear();
        if(!(selectCurriculum.getSelectionModel().getSelectedItem()==null && selectGrade.getSelectionModel().getSelectedItem()==null && selectSubject.getSelectionModel().getSelectedItem()==null && selectChapter.getSelectionModel().getSelectedItem()==null)){
            try {
                while(!singleTon.isResponse()){}
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(AddLessonController.class.getName()).log(Level.SEVERE, null, ex);
            }
            int size = singleTon.getAddNewLessonLessonList().size();
            for(int i=0;i<size;i++){
                selectLesson.getItems().add(singleTon.getAddNewLessonLessonList().get(i));
            }
        }
    }

    @FXML
    private void closeErrorAction(ActionEvent event) {
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
    
    public class CreateButtonUpdate extends TableCell<AddLessonView, Boolean> {
        final CheckBox updateButton = new CheckBox();
        public CreateButtonUpdate(){
            setAlignment(Pos.CENTER);
            updateButton.setCursor(Cursor.HAND);
            updateButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    AddLessonView addLessonView = (AddLessonView) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    addLessonView.setStatus(!addLessonView.isStatus());
                }
            });
        }
 
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
            }else{
                setGraphic(null);
            }
        }
    }
    
    class DatePickerCell<S, T> extends TableCell<AddLessonView, Date> {

        private DatePicker datePicker;
        private ObservableList<AddLessonView> birthdayData;
        private SingleTon singleTon;

        public DatePickerCell(ObservableList<AddLessonView> listBirthdays) {

            super();
            setAlignment(Pos.CENTER);
            this.birthdayData = listBirthdays;
            if (datePicker == null) {
                createDatePicker();
            }
            setGraphic(datePicker);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    datePicker.requestFocus();
                }
            });

            final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
                public DateCell call(final DatePicker datePicker) {
                    return new DateCell() {

                        @Override 
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                                setDisable(true);
                            }if (item.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                                setDisable(true);
                            }if (item.isBefore(LocalDate.now())) {
                                setDisable(true);
                            }
                            singleTon = SingleTon.getSingleTon();
                            int size = singleTon.getLeaveDateList().size();
                            for(int i = 0; size>i; i++){
                                if (item.getDayOfMonth()==singleTon.getLeaveDateList().get(i).getDate() && item.getMonthValue()==(singleTon.getLeaveDateList().get(i).getMonth()+1) && item.getYear()==Integer.parseInt(singleTon.getLeaveDateList().get(i).toString().substring(0, 4))) 
                                    setDisable(true);
                            }
                        }
                    };
                }
            };    
            datePicker.setDayCellFactory(dayCellFactory);
        }

        @Override
        public void updateItem(Date item, boolean empty) {

            super.updateItem(item, empty);

            SimpleDateFormat smp = new SimpleDateFormat("dd/MM/yyyy");

            if (null == this.datePicker) {
                System.out.println("datePicker is NULL");
            }

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                } else {
                    setDatepikerDate(smp.format(item));
                    setText(smp.format(item));
                    setGraphic(this.datePicker);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                }
            }
        }

        private void setDatepikerDate(String dateAsStr) {

            LocalDate ld = null;
            int jour, mois, annee;

            jour = mois = annee = 0;
            try {
                jour = Integer.parseInt(dateAsStr.substring(0, 2));
                mois = Integer.parseInt(dateAsStr.substring(3, 5));
                annee = Integer.parseInt(dateAsStr.substring(6, dateAsStr.length()));
            } catch (NumberFormatException e) {
                System.out.println("setDatepikerDate / unexpected error " + e);
            }

            ld = LocalDate.of(annee, mois, jour);
            datePicker.setValue(ld);
        }

        private void createDatePicker() {
            this.datePicker = new DatePicker();
            datePicker.setPromptText("jj/mm/aaaa");
            datePicker.setEditable(true);
            
            datePicker.setOnHidden(new EventHandler() {
                public void handle(Event t) {
                    LocalDate date = datePicker.getValue();
                    int index = getIndex();

                    SimpleDateFormat smp = new SimpleDateFormat("dd/MM/yyyy");
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.DAY_OF_MONTH, date.getDayOfMonth());
                    cal.set(Calendar.MONTH, date.getMonthValue() - 1);
                    cal.set(Calendar.YEAR, date.getYear());
                    singleTon = SingleTon.getSingleTon();
                    int i = 0;
                    boolean b = false;
                    if (date.getDayOfWeek().equals(DayOfWeek.MONDAY)) {
                        for(Date d : singleTon.getDateList()){
                            if(d.toString().equals(date.toString()))
                                i++;
                        }
                        if(singleTon.getScheduleList().get(0)>i){
                            b = true;
                        }
                    }else if (date.getDayOfWeek().equals(DayOfWeek.TUESDAY)) {
                        for(Date d : singleTon.getDateList()){
                            if(d.toString().equals(date.toString()))
                                i++;
                        }
                        if(singleTon.getScheduleList().get(1)>i){
                            b = true;
                        }
                    }else if (date.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) {
                        for(Date d : singleTon.getDateList()){
                            if(d.toString().equals(date.toString()))
                                i++;
                        }
                        if(singleTon.getScheduleList().get(2)>i){
                            b = true;
                        }
                    }else if (date.getDayOfWeek().equals(DayOfWeek.THURSDAY)) {
                        for(Date d : singleTon.getDateList()){
                            if(d.toString().equals(date.toString()))
                                i++;
                        }
                        if(singleTon.getScheduleList().get(3)>i){
                            b = true;
                        }
                    }else if (date.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
                        for(Date d : singleTon.getDateList()){
                            if(d.toString().equals(date.toString()))
                                i++;
                        }
                        if(singleTon.getScheduleList().get(4)>i){
                            b = true;
                        }
                    }
                    if(b){
                        setText(smp.format(cal.getTime()));
                        commitEdit(cal.getTime());

                        if (null != getBirthdayData()) {
                            getBirthdayData().get(index).setDate(cal.getTime());
                        }
                    }else{
                        //Show Error
                        setText(smp.format(cal.getTime()));
                        commitEdit(cal.getTime());

                        if (null != getBirthdayData()) {
                            getBirthdayData().get(index).setDate(cal.getTime());
                        }
                        ERRORHbox.setScaleX(1);
                        ERRORHbox.setScaleY(1);//ERROR: All lessons were completely scheduled for the day, 05/26/14. Please try another date.
                        textErrorInfo.setFill(new Color(1, 0, 0, 1));
                        textErrorInfo.setText("ERROR: ");
                        textMessageStart.setText("All lessons were completely scheduled for the day, ");
                        textDate.setFill(new Color(1, 0, 0, 1));
                        textDate.setText(cal.get(Calendar.DATE)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.YEAR));
                        textMessageLast.setText("");
                    }
                }
            });
            setAlignment(Pos.CENTER);
        }

        @Override
        public void startEdit() {
            super.startEdit();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        public ObservableList<AddLessonView> getBirthdayData() {
            return birthdayData;
        }

        public void setBirthdayData(ObservableList<AddLessonView> birthdayData) {
            this.birthdayData = birthdayData;
        }

        public DatePicker getDatePicker() {
            return datePicker;
        }

        public void setDatePicker(DatePicker datePicker) {
            this.datePicker = datePicker;
        }
    }
}
