/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.File;
import java.io.IOException;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.LogOutRequest;
import org.coretechies.MessageTransporter.MessageRequest;

/**
 *
 * @author user
 */
public class PageSwitch {
    
    private SingleTon singleTon;
    StackPane page;
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    double maxX= primaryScreenBounds.getMaxX();
    double maxY= primaryScreenBounds.getMaxY();
    
    public void home(Stage stage){
        singleTon = SingleTon.getSingleTon();
        try {
            AnchorPane anchorPane = (AnchorPane) FXMLLoader.load(getClass().getResource("CurriculumAndGrade.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(anchorPane);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void logout(Stage stage){
        singleTon =SingleTon.getSingleTon();
        File file = singleTon.getMediaFile();
        ClientSocket clientSocket =ClientSocket.getClientSocket();

        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        LogOutRequest logOutRequest = new LogOutRequest();
        logOutRequest.setHash(clientSocket.getHash());
        logOutRequest.setMessageType("LogOutRequest");
        queue.add(logOutRequest); 
//        if(file.exists())
//            file.delete();
        gotoLoginPage(stage);
    }
    
    public void back(Stage stage,String fxmlName){
        singleTon =SingleTon.getSingleTon();
        try {
            page = (StackPane) FXMLLoader.load(getClass().getResource(fxmlName), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void nextScreen(Stage stage,String fxmlName){
        singleTon =SingleTon.getSingleTon();
        try {
            Parent anchorPane = FXMLLoader.load(getClass().getResource(fxmlName), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(anchorPane);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            System.out.println("exceptionnnnnnnnnnnnnnnnnnnnnnnnnnnn");
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }System.out.println("outttttttttttttttttttttttttttttttt");
    }
    
    public void nextScreenCreate(Stage stage,String fxmlName){
        singleTon =SingleTon.getSingleTon();
        try {
            Parent anchorPane = FXMLLoader.load(getClass().getResource(fxmlName));
            Scene scene = new Scene(anchorPane);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoSubject(Stage stage){
        singleTon =SingleTon.getSingleTon();
        try {
            StackPane pane;
            pane = (StackPane) FXMLLoader.load(getClass().getResource("Subject.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoSubjectList(Stage stage){
        singleTon =SingleTon.getSingleTon();
        try {
            ScrollPane pane;
            pane = (ScrollPane) FXMLLoader.load(getClass().getResource("SubjectList.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    public void gotoLoginPage(Stage stage) {
        try {
            StackPane page;
            page = (StackPane) FXMLLoader.load(getClass().getResource("Loginip.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.setTitle("CDC");
            stage.setHeight(maxY);
            stage.setWidth(maxX);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(PageSwitch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
