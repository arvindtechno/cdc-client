/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.AddLeaveRequest;
import org.coretechies.MessageTransporter.DeleteMyLeaveRequest;
import org.coretechies.MessageTransporter.LeaveDateRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdateLeaveRequest;
import org.coretechies.forms.controller.java.Item;

/**
 * FXML Controller class
 *
 * @author user
 */
public class MyLeavesController implements Initializable {
   
    @FXML
    private Pane paneAddLeave;
    @FXML
    private DatePicker leaveDate;
    @FXML
    private TextArea leaveComment;
    @FXML
    private Button btnSubmit;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnAddLeaves;
    @FXML
    private TableView<Item> leaveTable;
    @FXML
    private TableColumn columnDate;
    @FXML
    private TableColumn columnComment;
    @FXML
    private TableColumn columnUpdate;
    @FXML
    private TableColumn columnDelete;
    @FXML
    private Label labelAddleave;
    @FXML
    private Button btnClosePaneAddLeave;
    @FXML
    private Button btnUpdate;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private HBox hboxAddLeave;
    @FXML
    private AnchorPane infoPane;
    @FXML
    private Button btnInfo;
    
    private ObservableList<Item> data;
    private SingleTon singleTon;
    private String updateId;
    private Item updateItem;
    private PageSwitch pageSwitch;
    @FXML
    private HBox ERRORHbox;
    @FXML
    private Text textErrorInfo;
    @FXML
    private Text textMessage;
    @FXML
    private Button btnCloseError;
    @FXML
    private Text userName;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
        pageSwitch = new PageSwitch();
        hboxAddLeave.setScaleX(0);
        hboxAddLeave.setScaleY(0);
        paneAddLeave.setScaleX(0);
        paneAddLeave.setScaleY(0);
        
        columnDate.setCellValueFactory(
            new PropertyValueFactory<Item,String>("date")
        );
        
        columnComment.setCellValueFactory(
            new PropertyValueFactory<Item,String>("comment")
        );
        
        columnUpdate.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Item, Boolean>, ObservableValue<Boolean>>() {
            
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Item, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        
        columnUpdate.setCellFactory(
            new Callback<TableColumn<Item, Boolean>, TableCell<Item, Boolean>>() {
 
            @Override
            public TableCell<Item, Boolean> call(TableColumn<Item, Boolean> param) {
                return new CreateButtonUpdate();
            }
        });

        columnDelete.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Item, Boolean>, 
            ObservableValue<Boolean>>() {
 
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Item, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        
        columnDelete.setCellFactory(
            new Callback<TableColumn<Item, Boolean>, TableCell<Item, Boolean>>() {
                
            @Override
            public TableCell<Item, Boolean> call(TableColumn<Item, Boolean> param) {
                 return new CreateButtonDelete();
            }        
        });
        
        data = FXCollections.observableArrayList();
        leaveTable.setItems(data);
        
        singleTon = SingleTon.getSingleTon();
        while(singleTon.getMyLeavesList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getMyLeavesList().size();  
        
        for(int i=0;i<size;i++) {
            System.out.println("DATA");
            Item item = new Item();
            item.date.setValue(singleTon.getMyLeavesList().get(i).getDate().toString());
            item.comment.setValue(singleTon.getMyLeavesList().get(i).getComment());
            item.setId(singleTon.getMyLeavesList().get(i).getId());
            data.add(item);
        }
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }    

    @FXML
    private void addLeaves(ActionEvent event) {
        
        labelAddleave.setText("Add New Leave Date Below");        
        ClientSocket clientSocket =ClientSocket.getClientSocket();        
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        LeaveDateRequest leaveDateRequest = new LeaveDateRequest();
        leaveDateRequest.setHash(clientSocket.getHash());
        leaveDateRequest.setMessageType("LeaveDateRequest");
        queue.add(leaveDateRequest);
        
        btnSubmit.setScaleX(1);
        btnSubmit.setScaleY(1);
        paneAddLeave.setScaleX(1);
        paneAddLeave.setScaleY(1);
        hboxAddLeave.setScaleX(1);
        hboxAddLeave.setScaleY(1);
        leaveComment.setText("");
        btnUpdate.setScaleX(0);
        btnUpdate.setScaleY(0);
        
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    
                    @Override 
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                            setDisable(true);
                        }if (item.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                            setDisable(true);
                        }if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                        }
                        
                        int size = data.size();
                        for(int i = 0; size>i; i++){
                            String date = data.get(i).date.getValue();
                            if (item.getDayOfMonth()==Integer.parseInt(date.substring(8, 10)) && item.getMonthValue()==Integer.parseInt(date.substring(5, 7)) && item.getYear()==Integer.parseInt(date.substring(0, 4))) {
                                if(updateItem!=null){
                                    String updateDate = updateItem.date.getValue();
                                    if(!(item.getDayOfMonth()==Integer.parseInt(updateDate.substring(8, 10)) && item.getMonthValue()==Integer.parseInt(updateDate.substring(5, 7)) && item.getYear()==Integer.parseInt(updateDate.substring(0, 4))))
                                        setDisable(true);
                                }else{
                                    setDisable(true);
                                }
                            }
                        }
                        
                        singleTon  = SingleTon.getSingleTon();
                        size = singleTon.getLeaveDateList().size();
                        for(int i = 0; size>i; i++){
                            if (item.getDayOfMonth()==singleTon.getLeaveDateList().get(i).getDate() && item.getMonthValue()==(singleTon.getLeaveDateList().get(i).getMonth()+1) && item.getYear()==Integer.parseInt(singleTon.getLeaveDateList().get(i).toString().substring(0, 4))) 
                                setDisable(true);
                        }
                    }
                };
            }
        };
        leaveDate.setDayCellFactory(dayCellFactory);        
    }

    @FXML
    private void submit(ActionEvent event) {
        boolean flage = true;
        singleTon = SingleTon.getSingleTon();
        if(leaveDate.getValue()!=null && leaveComment.getText()!=null){
            
            System.out.println(leaveDate.getValue());
            System.out.println(leaveComment.getText());
            String date = leaveDate.getValue().getYear()+"-"+leaveDate.getValue().getMonthValue()+"-"+leaveDate.getValue().toString().substring(8, 10);
            int size = singleTon.getClassDateList().size();
            for(int i = 0; i<size; i++){
                String date1 = singleTon.getClassDateList().get(i).toString();
                if (date.equals(date1)) {
                    flage = false;
                    break;
                }
            }
        
            if(!flage){
                infoPane.setScaleX(1);
                infoPane.setScaleY(1);
            }
            else{

                singleTon = SingleTon.getSingleTon();
                singleTon.setResponse(false);
                ClientSocket clientSocket =ClientSocket.getClientSocket();

                Queue<MessageRequest> queue = clientSocket.getRequestQueue();

                AddLeaveRequest addLeaveRequest = new AddLeaveRequest();
                addLeaveRequest.setHash(clientSocket.getHash());
                addLeaveRequest.setMessageType("AddLeaveRequest");
                addLeaveRequest.setComment(leaveComment.getText());
                addLeaveRequest.setLeaveDate(date);
                queue.add(addLeaveRequest);
//                singleTon = SingleTon.getSingleTon();
                singleTon.setMyLeavesList(null);

                while(!singleTon.isResponse()){
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                if(singleTon.isInturrept()){
                    textErrorInfo.setFill(new Color(1, 0, 0, 1));
                    textErrorInfo.setText("ERROR");
                    textMessage.setText("A lesson is already scheduled for this date "+date+" for  "+singleTon.getSubject()+": "+singleTon.getChapter()+": "+singleTon.getLesson()+": "+singleTon.getTopic()+". Please update user lesson planning first and try again.");
                    ERRORHbox.setScaleX(1);
                    ERRORHbox.setScaleY(1);
                }else{

                    hboxAddLeave.setScaleX(0);
                    hboxAddLeave.setScaleY(0);
                    paneAddLeave.setScaleX(0);
                    paneAddLeave.setScaleY(0);
                    
                    Item item = new Item();
                    item.setId(singleTon.getId());
                    item.date.setValue(leaveDate.getValue().toString());
                    item.comment.setValue(leaveComment.getText());
                    data.add(item);
                    singleTon.setId(0);
                }
            }
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void getLeaveDate(ActionEvent event) {
    }

    @FXML
    private void closePaneAddLeave(ActionEvent event) {
        paneAddLeave.setScaleX(0);
        paneAddLeave.setScaleY(0);
        
        hboxAddLeave.setScaleX(0);
        hboxAddLeave.setScaleY(0);
    }

    @FXML
    private void update(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setUpdateResponse(false);
        
        String date = leaveDate.getValue().getYear()+"-"+leaveDate.getValue().getMonthValue()+"-"+leaveDate.getValue().toString().substring(8, 10);
        ClientSocket clientSocket =ClientSocket.getClientSocket();

        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        singleTon.setResponse(false);
        
        UpdateLeaveRequest updateLeaveRequest = new UpdateLeaveRequest();
        updateLeaveRequest.setHash(clientSocket.getHash());
        updateLeaveRequest.setMessageType("UpdateLeaveRequest");
        updateLeaveRequest.setComment(leaveComment.getText());
        updateLeaveRequest.setLeaveDate(date);
        updateLeaveRequest.setId(updateId);
        updateId = null;
        queue.add(updateLeaveRequest);
        
        singleTon.setMyLeavesList(null);
        
        singleTon.setSchoolHolidayList(null);
        while(!singleTon.isResponse()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(singleTon.isInturrept()){
            textErrorInfo.setFill(new Color(1, 0, 0, 1));
            textErrorInfo.setText("ERROR ");
            textMessage.setText("A lesson is already scheduled for this date "+date+" for  "+singleTon.getSubject()+": "+singleTon.getChapter()+": "+singleTon.getLesson()+": "+singleTon.getTopic()+". Please update user lesson planning first and try again.");
            ERRORHbox.setScaleX(1);
            ERRORHbox.setScaleY(1);
        }else{
            hboxAddLeave.setScaleX(0);
            hboxAddLeave.setScaleY(0);

            paneAddLeave.setScaleX(0);
            paneAddLeave.setScaleY(0);

            Item replaceItem = new Item();
            replaceItem.setId(updateItem.getId());
            replaceItem.date.setValue(date);
            replaceItem.comment.setValue(leaveComment.getText());

            data.remove(updateItem);
            data.add(replaceItem);
            singleTon.setUpdateResponse(false);
            updateItem = null;
        }
    }

    @FXML
    private void ActionInfo(ActionEvent event) {
        infoPane.setScaleX(0);
        infoPane.setScaleY(0);
    }

    @FXML
    private void closeErrorAction(ActionEvent event) {
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    private class DateAllign extends TableCell<Item, String> {

        public DateAllign() {
            System.out.println("Allign");
            setAlignment(Pos.CENTER);
        }
    }

    public class CreateButtonUpdate extends TableCell<Item, Boolean> {
        final Button updateButton = new Button("Update");
        public CreateButtonUpdate(){
            setAlignment(Pos.CENTER);
            updateButton.setCursor(Cursor.HAND);
            System.out.println(CreateButtonUpdate.this.getIndex());
            updateButton.setOnAction(new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent t) {
                    System.out.println("update");
                    
                    hboxAddLeave.setScaleX(1);
                    hboxAddLeave.setScaleY(1);
                    paneAddLeave.setScaleX(1);
                    paneAddLeave.setScaleY(1);
                    btnUpdate.setScaleX(1);
                    btnUpdate.setScaleY(1);
                    btnSubmit.setScaleX(0);
                    btnSubmit.setScaleY(0);
                    labelAddleave.setText("Update Leave date below");

                    Item item = (Item) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    updateItem = item;
                    updateId = item.getId()+"";
                    leaveComment.setText(item.getComment());
                }
            });
        }
 
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
                updateButton.setStyle("-fx-background-color:#008cba;-fx-text-fill: white;");
            }else{
                setGraphic(null);
            }
        }
    }

    public class CreateButtonDelete extends TableCell<Item, Boolean> {
        final Button deleteButton = new Button("Delete");
        
        public CreateButtonDelete(){
            setAlignment(Pos.CENTER);
            System.out.println(CreateButtonDelete.this.getIndex());
            deleteButton.setCursor(Cursor.HAND);
            deleteButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    Item item = (Item) CreateButtonDelete.this.getTableView().getItems().get(CreateButtonDelete.this.getIndex());
                    singleTon = SingleTon.getSingleTon();   
                    ClientSocket clientSocket =ClientSocket.getClientSocket();
                    Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                    DeleteMyLeaveRequest deleteMyLeaveRequest = new DeleteMyLeaveRequest();
                    deleteMyLeaveRequest.setHash(clientSocket.getHash());
                    deleteMyLeaveRequest.setMessageType("DeleteLeaveRequest");
                    deleteMyLeaveRequest.setId(item.getId());
                    queue.add(deleteMyLeaveRequest);
                    data.remove(item);
                    singleTon.setMyLeavesList(null);
                    textErrorInfo.setFill(new Color(0.3216, 0.502, 0.2039, 1));
                    textErrorInfo.setText("INFO:");
                    String date = item.date.getValue();
                    System.out.println("DATE : \n"+date);
                    date = date.substring(8, 10) + "/" + date.substring(5, 7) + "/" + date.substring(0, 4);
                    textMessage.setText(" All users can plan their classes now on "+date+"  since you specified that school is not closed on that day.");
                    ERRORHbox.setScaleX(1);
                    ERRORHbox.setScaleY(1);
                }
            });
        }
 
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){

                setGraphic(deleteButton);
                deleteButton.setStyle("-fx-background-color:#f04124;-fx-text-fill: white;");
            }
            else{
                setGraphic(null);
            }
        }
    }
}
