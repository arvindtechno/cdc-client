/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class LessonScheduleView {
    private String id;
    public SimpleStringProperty subject = new SimpleStringProperty();
    public SimpleStringProperty lesson = new SimpleStringProperty();
    public SimpleStringProperty topic = new SimpleStringProperty();
    public SimpleStringProperty classDate = new SimpleStringProperty();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject.get();
    }

    public String getLesson() {
        return lesson.get();
    }

    public String getTopic() {
        return topic.get();
    }

    public String getClassDate() {
        return classDate.get();
    }
    
}
