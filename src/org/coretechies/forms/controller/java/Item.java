/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class Item {
    public SimpleStringProperty date = new SimpleStringProperty();
    public SimpleStringProperty comment = new SimpleStringProperty();
    private int id;

    public Item(String date,String comment) {
        
        this.date=new SimpleStringProperty(date);
        this.comment=new SimpleStringProperty(comment);
    }
    public Item() {
        
    }
    
    public String getDate() {
        return date.get();
    }

    public String getComment() {
        return comment.get();
    }

    public SimpleStringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public SimpleStringProperty commentProperty() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment.set(comment);
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
