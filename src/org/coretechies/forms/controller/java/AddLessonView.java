package org.coretechies.forms.controller.java;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddLessonView {

    private String id;
    private boolean status;
    private final SimpleObjectProperty<Date> date;
    private final StringProperty topic;

    public AddLessonView(String topic, Date date) {
        this.topic = new SimpleStringProperty(topic);
        this.date = new SimpleObjectProperty(date);
    }

    public AddLessonView() {
        this.topic = new SimpleStringProperty();
        this.date = new SimpleObjectProperty();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    

    public String getTopic() {
        return topic.get();
    }

    public void setTopic(String topic) {
        this.topic.set(topic);
    }

    public StringProperty topicProperty() {
        return topic;
    }

    public Date getDate() {
        return (Date) date.get();
    }

    public String getDateAsString() {
        SimpleDateFormat smp = new SimpleDateFormat("dd MMMMM yyyy");
        String strDate = (null == date || null == date.get())
                ? "" : smp.format(date.get());
        
        return strDate;
    }

    public void setDate(Date date) {
        this.date.set(date);
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
}