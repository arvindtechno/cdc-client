/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class SearchResultView {
    
    public SimpleStringProperty subject = new SimpleStringProperty();
    public SimpleStringProperty chapter = new SimpleStringProperty();
    public SimpleStringProperty lesson = new SimpleStringProperty();
    public SimpleStringProperty topic = new SimpleStringProperty();
    public SimpleStringProperty grade = new SimpleStringProperty();
    public SimpleStringProperty curriculum = new SimpleStringProperty();

    public String getSubject() {
        return subject.get();
    }

    public String getLesson() {
        return lesson.get();
    }

    public String getTopic() {
        return topic.get();
    }

    public String getChapter() {
        return chapter.get();
    }

    public String getGrade() {
        return grade.get();
    }

    public String getCurriculum() {
        return curriculum.get();
    }
    
}
