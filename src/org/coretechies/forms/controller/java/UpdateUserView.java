/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class UpdateUserView {
    private String userId;
    public SimpleStringProperty username = new SimpleStringProperty();
    public SimpleStringProperty firstname = new SimpleStringProperty();
    public SimpleStringProperty lastname = new SimpleStringProperty();
    public SimpleStringProperty password = new SimpleStringProperty();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username.get();
    }

    public String getFirstname() {
        return firstname.get();
    }

    public String getLastname() {
        return lastname.get();
    }

    public String getPassword() {
        return password.get();
    }
    
     
}
