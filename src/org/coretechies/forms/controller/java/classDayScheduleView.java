/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TextField;

/**
 *
 * @author user
 */
public class classDayScheduleView{
    public SimpleStringProperty monday = new SimpleStringProperty();
    public SimpleStringProperty tuesday = new SimpleStringProperty();
    public SimpleStringProperty wednesday = new SimpleStringProperty();
    public SimpleStringProperty thursday = new SimpleStringProperty();
    public SimpleStringProperty friday = new SimpleStringProperty();
    public classDayScheduleView(String monday, String tuesday, String wednesday, String thursday,String friday) {
        this.monday = new SimpleStringProperty(monday);
        this.tuesday = new SimpleStringProperty(tuesday);
        this.wednesday = new SimpleStringProperty(wednesday);
        this.thursday = new SimpleStringProperty(thursday);
        this.friday = new SimpleStringProperty(friday);
    }

   

    public String getMonday() {
        return monday.get();
    }

    public void setMonday(String monday) {
        this.monday.set(monday);
    }

    public String getTuesday() {
        return tuesday.get();
    }

    public void setTuesday(String tuesday) {
        this.tuesday.set(tuesday);
    }

    public String getWednesday() {
        return wednesday.get();
    }

    public void setWednesday(String wednesday) {
        this.wednesday.set(wednesday);
    }

    public String getThursday() {
        return thursday.get();
    }

    public void setThursday(String thursday) {
        this.thursday.set(thursday);
    }

    public String getFriday() {
        return friday.get();
    }

    public void setFriday(String friday) {
        this.friday.set(friday);
    }
    public SimpleStringProperty mondayProperty() {
        return monday;
    }
    public SimpleStringProperty tuesdayProperty() {
        return tuesday;
    }
    public SimpleStringProperty wednesdayProperty() {
        return wednesday;
    }
    public SimpleStringProperty thursdayProperty() {
        return thursday;
    }
    public SimpleStringProperty fridayProperty() {
        return friday;
    }
//  
//    public String getMonday() {
//        return monday.get();
//    }
//
//    public String getTuesday() {
//        return tuesday.get();
//    }
//
//    public String getWednesday() {
//        return wednesday.get();
//    }
//
//    public String getThursday() {
//        return thursday.get();
//    }
//
//    public String getFriday() {
//        return friday.get();
//    }


    
    
    
}
