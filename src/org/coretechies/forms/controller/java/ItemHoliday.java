/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller.java;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author user
 */
public class ItemHoliday {
    public SimpleStringProperty date = new SimpleStringProperty();
    public SimpleStringProperty comment = new SimpleStringProperty();
    private String id;
    
    public String getDate() {
        return date.get();
    }

    public String getComment() {
        return comment.get();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
