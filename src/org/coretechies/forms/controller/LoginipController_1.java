package org.coretechies.forms.controller;

import org.coretechies.forms.*;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.Controller;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.MessageRequest;

public class LoginipController_1 implements Initializable {
    @FXML
    private TextField userName;
    @FXML
    private TextField password;
    @FXML
    private Button enter;
    static Boolean nextscreen = false;
    Stage stage;
    ClientSocket clientSocket;
    @FXML
    private TextField serverip;
    @FXML
    private Label lusername;
    @FXML
    private Label lpassword;
    @FXML
    private Button toEspanol;
    @FXML
    private Button toChineese;
    @FXML
    private Button toFrancais;
    @FXML
    private Label lip;
    @FXML
    private Label luserlogin;
    @FXML
    private StackPane log;
    @FXML
    private Pane loginpane;
    @FXML
    private Button toEnglish;
    private ResourceBundle bundle;
    private SingleTon singleTon;
    
    private PageSwitch pageSwitch;
    @FXML
    private Pane paneError;
    @FXML
    private Label labelError;
    @FXML
    private Button btnCloseErrorpane;
    @FXML
    private Button btnXErrorpane;
    @FXML
    private Pane loginpane1;
    @FXML
    private Label lusername1;
    @FXML
    private Label lpassword1;
    @FXML
    private TextField userName1;
    @FXML
    private PasswordField password1;
    @FXML
    private TextField serverip1;
    @FXML
    private Button enter1;
    @FXML
    private Button toEnglish1;
    @FXML
    private Button toEspanol1;
    @FXML
    private Button toChineese1;
    @FXML
    private Button toFrancais1;
    @FXML
    private Label lip1;
    @FXML
    private Label luserlogin1;
    @FXML
    private Pane panemove1;
    
     Rectangle2D primaryScreenBounds = Screen.getPrimary().getBounds();
    double maxX= primaryScreenBounds.getMaxX();
    double maxY= primaryScreenBounds.getMaxY();
    public LoginipController_1(ClientSocket clientSocket) {
        this.clientSocket = clientSocket;
    }
    
    public LoginipController_1() {
    }
    /**
     * Initializes the controller class.
     */
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        singleTon = SingleTon.getSingleTon();
        bundle = rb;
        lusername.setText(bundle.getString("User Name"));
        lpassword.setText(bundle.getString("Password"));
        enter.setText(bundle.getString("Enter"));
        lip.setText(bundle.getString("Server I.P."));
        luserlogin.setText(bundle.getString("User Login"));

        pageSwitch = new PageSwitch();
        enter.setCursor(Cursor.HAND);
        toFrancais.setCursor(Cursor.HAND);
        toEspanol.setCursor(Cursor.HAND);
        toEnglish.setCursor(Cursor.HAND);
        toChineese.setCursor(Cursor.HAND);
        userName.setText("core");
        password.setText("core");
        serverip.setText("localhost");
        
        paneError.setScaleX(0);
        paneError.setScaleY(0);
//        loginpane.clipProperty();
        moveTwink();
    }    

    @FXML
    private void userName(ActionEvent event) {
        
    }

    @FXML
    private void password(ActionEvent event) {
    }
    
    @FXML
    private void serverip(ActionEvent event) {
    }

    @FXML
    private void login(ActionEvent event) throws InterruptedException {
        authenticateLogin();
    }

    public Scene loadCurr(){
        try {
            singleTon = SingleTon.getSingleTon();
            
            System.out.println("Come");
            CurriculumAndGradeController cc = new CurriculumAndGradeController(clientSocket);
            AnchorPane page = (AnchorPane) FXMLLoader.load(getClass().getResource("CurriculumAndGrade.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(page);
            
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void nextScreen(){
    }    
    
    @FXML
    private void english(ActionEvent event) {
        singleTon.setLocale(new Locale("en", "EN"));
        lusername.setText("User Name");
        lpassword.setText("Password");
        lip.setText("Server I.P.");
        luserlogin.setText("User Login");
        enter.setText("Enter");
    }

    @FXML
    private void espanol(ActionEvent event) {
        singleTon.setLocale(new Locale("es", "ES"));
        lusername.setText("Nombre de Usuario");
        lpassword.setText("Contraseña");
        lip.setText("I.P. servidor");
        luserlogin.setText("Login de Usuario");
        enter.setText("entrar en");
    }

    @FXML
    private void chineese(ActionEvent event) {
        singleTon.setLocale(new Locale("zh", "ZH"));
        lusername.setText("用户名");
        lpassword.setText("密码");
        lip.setText("服务器I.P.");
        luserlogin.setText("用户登录");
        enter.setText("進入");
    }

    @FXML
    private void francais(ActionEvent event) {
        singleTon.setLocale(new Locale("fr", "FR"));
        lusername.setText("Nom d'utilisateur");
        lpassword.setText("mot de passe");
        lip.setText("serveur I.P.");
        luserlogin.setText("login utilisateur");
        enter.setText("entrer");
    }
    
    @FXML
    private void logg(javafx.scene.input.KeyEvent event) {
        String code = event.getCode().toString();
        System.out.println(event.getCode());
        if(code.equals("ENTER")){
        try {
            authenticateLogin();
        } catch (InterruptedException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
    }

    
    
    private boolean isEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    public void setbool(boolean b) {
        LoginipController.nextscreen=b;
        System.out.println("boole<<>>"+b);
        System.out.println("next"+nextscreen);
    }

    public Scene iview() {
         try {

             StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("simple"
                    + ".fxml"));
             ImageView imageView = clientSocket.getView();
             page.getChildren().add(imageView);
            Scene scene = new Scene(page);
            
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @FXML
    private void closepaneAction(ActionEvent event) {
        paneError.setScaleX(0);
        paneError.setScaleY(0);
        labelError.setText("");
    }

    private void moveTwink() {
         panemove1.setLayoutX(0);
        panemove1.setLayoutX(0);
        panemove1.setPrefSize(2*maxX, 2*maxY);
    
        Path tpath = new Path();
    tpath.getElements().add(new MoveTo(0, 0));
    tpath.getElements().add(new LineTo(maxX, 0));
    tpath.getElements().add(new ClosePath());
    
         final PathTransition tpathTransition = new PathTransition();
      tpathTransition.setDuration(Duration.seconds(80.0));
//      tpathTransition.setDelay(Duration.seconds(2.0));
      tpathTransition.setPath(tpath);
      tpathTransition.setNode(panemove1);
//      tpathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
      tpathTransition.setCycleCount(Timeline.INDEFINITE); 
      tpathTransition.setAutoReverse(true);
      tpathTransition.play();
    }

    
    
    private final class UserData implements UserfieldDataip {

        @Override
        public String getUserName() {
            if (userName == null || isEmpty(userName.getText())) {
                return null;
            }
            return userName.getText();
        }

        @Override
        public String getPassword() {
            if (password == null || isEmpty(password.getText())) {
                return null;
            }
            return password.getText();
        }

        @Override
        public String getserverip() {
            if (serverip == null || isEmpty(serverip.getText())) {
                return null;
            }
            return serverip.getText();
        }
        
    }
    
    public void authenticateLogin() throws InterruptedException{
        String UsernameS;
        String passwordS;
        String serveripS;

        UserfieldDataip ufd= new UserData();
        UsernameS=ufd.getUserName();
        passwordS=ufd.getPassword();
        serveripS=ufd.getserverip();

        Controller c =new Controller();
        MessageRequest mr= c.getUserField(UsernameS, passwordS);
        stage = (Stage) enter.getScene().getWindow();
        ClientSocket cs = ClientSocket.getClientSocket();
//        cs.createss(mr,stage,serveripS);
        clientSocket = ClientSocket.getClientSocket();
        if(clientSocket.getX()==0){
            paneError.setScaleX(1);
            paneError.setScaleY(1);
            labelError.setText("Invalid user name and password");
        }else if(clientSocket.getX()==2){
            paneError.setScaleX(1);
            paneError.setScaleY(1);
            labelError.setText("User Already login");
        }
        System.out.println("bScene");
    }
}
