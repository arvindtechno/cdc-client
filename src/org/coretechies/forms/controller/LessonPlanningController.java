/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.ClassDayScheduleRequest;
import org.coretechies.MessageTransporter.LessonPlanningRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MyLeavesRequest;
import org.coretechies.MessageTransporter.SchoolHolidayRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class LessonPlanningController implements Initializable {
    @FXML
    private Button btnClassDaySchedule;
    @FXML
    private Button btnMyLeaves;
    @FXML
    private Button btnSchoolHoliday;
    @FXML
    private Button btnLessonPlanning;
//    private Pane paneLogo;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnLogout;
    
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelLessonPlanning;
    @FXML
    private ImageView iviewIcon;

    private ResourceBundle bundle;
    @FXML
    private HBox ErrorHbox;
    @FXML
    private Button btnCloseError;
    
    SingleTon singleTon;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ErrorHbox.setScaleX(0);
        ErrorHbox.setScaleY(0);
        bundle = rb;

        btnClassDaySchedule.setText(bundle.getString("Class/Day Schedule"));
        btnMyLeaves.setText(bundle.getString("My Leaves"));
        btnSchoolHoliday.setText(bundle.getString("School Holiday"));
//        btnLessonPlanning.setText(bundle.getString("Lesson Planning"));
        labelWelcomeL.setText(bundle.getString("Welcome"));
        labelWelcomeR.setText(bundle.getString("Welcome"));
//        labelLessonPlanning.setText(bundle.getString("Lesson Planning"));
        labelHome.setText(bundle.getString("Home"));
        labelBack.setText(bundle.getString("Back"));
        labelLogout.setText(bundle.getString("Logout"));

        pageSwitch = new PageSwitch();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        singleTon = SingleTon.getSingleTon();
        if(singleTon.isLessonPlanningErrorPane()){
            ErrorHbox.setScaleX(1);
            ErrorHbox.setScaleY(1);
            singleTon.setLessonPlanningErrorPane(false);
        }
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }    

    @FXML
    private void classDaySchedule(ActionEvent event) {

        System.out.println("Class Day Schedule");
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        ClassDayScheduleRequest classDayScheduleRequest = new ClassDayScheduleRequest();
        classDayScheduleRequest.setHash(clientSocket.getHash());
        classDayScheduleRequest.setMessageType("ClassDayScheduleRequest");
        queue.add(classDayScheduleRequest);
            
        Stage stage = (Stage) btnClassDaySchedule.getScene().getWindow();
        pageSwitch.nextScreen(stage, "ClassesDaySchedule.fxml");
    }

    @FXML
    private void myLeaves(ActionEvent event) {

        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        MyLeavesRequest myLeavesRequest = new MyLeavesRequest();
        myLeavesRequest.setHash(clientSocket.getHash());
        myLeavesRequest.setMessageType("MyLeavesRequest");
        queue.add(myLeavesRequest);

        Stage stage = (Stage) btnMyLeaves.getScene().getWindow();
        pageSwitch.nextScreen(stage, "MyLeaves.fxml");
    }

    @FXML
    private void schoolHoliday(ActionEvent event) {
        
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        SchoolHolidayRequest schoolHolidayRequest = new SchoolHolidayRequest();
        schoolHolidayRequest.setHash(clientSocket.getHash());
        schoolHolidayRequest.setMessageType("SchoolHolidayRequest");
        queue.add(schoolHolidayRequest);
        
        Stage stage = (Stage) btnSchoolHoliday.getScene().getWindow();
        pageSwitch.nextScreen(stage, "SchoolHoliday.fxml");
    }

    @FXML
    private void lessonPlanning(ActionEvent event) {
        SingleTon singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanningList(null);
        singleTon.setLessonPlanResponse(false);
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        LessonPlanningRequest lessonPlanningRequest = new LessonPlanningRequest();
        lessonPlanningRequest.setHash(clientSocket.getHash());
        lessonPlanningRequest.setMessageType("LessonPlanningRequest");
        queue.add(lessonPlanningRequest);
                
        singleTon = SingleTon.getSingleTon();
        while(singleTon.isLessonPlanResponse()==false){
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(singleTon.isErrorResponse()){
            Stage stage = (Stage) btnLessonPlanning.getScene().getWindow();
            pageSwitch.nextScreen(stage, "LessonSchedule.fxml");
        }else{
            ErrorHbox.setScaleX(1);
            ErrorHbox.setScaleY(1);
        }
    }

  
    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }    

    private void back(MouseEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
