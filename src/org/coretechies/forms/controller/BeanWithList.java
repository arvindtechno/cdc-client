/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.forms.controller;

import java.util.List;

/**
 *
 * @author Tiwari
 */
public class BeanWithList {

    private Integer m_id;
    private String m_subject;
    private String m_lesson;
    private String m_topic;
    private String m_classDate;

    public BeanWithList(Integer m_id, String m_subject, String m_lesson, String m_topic, String m_classDate) {
        this.m_id = m_id;
        this.m_subject = m_subject;
        this.m_lesson = m_lesson;
        this.m_topic = m_topic;
        this.m_classDate = m_classDate;
    }

    public Integer getId() {
        return m_id;
    }

    public String getSubject() {
        return m_subject;
    }

    public String getLesson() {
        return m_lesson;
    }

    public String getTopic() {
        return m_topic;
    }

    public String getClassDate() {
        return m_classDate;
    }
    
}