package org.coretechies.forms.controller;

import org.coretechies.forms.*;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.Controller;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.MessageRequest;

public class LoginipController implements Initializable {
    @FXML
    private TextField userName;
    @FXML
    private TextField password;
    @FXML
    private Button enter;
    static Boolean nextscreen = false;
    Stage stage;
    ClientSocket clientSocket;
    @FXML
    private TextField serverip;
    @FXML
    private Label lusername;
    @FXML
    private Label lpassword;
    @FXML
    private Button toEspanol;
    @FXML
    private Button toChineese;
    @FXML
    private Button toFrancais;
    @FXML
    private Label lip;
    @FXML
    private Label luserlogin;
    @FXML
    private StackPane log;
    @FXML
    private Pane loginpane;
    @FXML
    private Button toEnglish;
    private ResourceBundle bundle;
    private SingleTon singleTon;
    
    private PageSwitch pageSwitch;
    @FXML
    private Pane paneError;
    @FXML
    private Text labelError;
    @FXML
    private Button btnCloseErrorpane;
    @FXML
    private Button btnXErrorpane;
    @FXML
    private Pane panemove1;
    
     Rectangle2D primaryScreenBounds = Screen.getPrimary().getBounds();
    double maxX= primaryScreenBounds.getMaxX();
    double maxY= primaryScreenBounds.getMaxY();
    double X= primaryScreenBounds.getWidth();
//    @FXML
//    private AnchorPane panemove;
//    @FXML
//    private AnchorPane panemove2;
//    @FXML
//    private AnchorPane panemove3;
//    @FXML
//    private AnchorPane apCloud;
    public LoginipController(ClientSocket clientSocket) {
        this.clientSocket = clientSocket;
    }
    
    public LoginipController() {
    }
    /**
     * Initializes the controller class.
     */
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        singleTon = SingleTon.getSingleTon();
        bundle = rb;
        lusername.setText(bundle.getString("User Name"));
        lpassword.setText(bundle.getString("Password"));
        enter.setText(bundle.getString("Enter"));
        lip.setText(bundle.getString("Server I.P."));
        luserlogin.setText(bundle.getString("User Login"));

        pageSwitch = new PageSwitch();
        enter.setCursor(Cursor.HAND);
        toFrancais.setCursor(Cursor.HAND);
        toEspanol.setCursor(Cursor.HAND);
        toEnglish.setCursor(Cursor.HAND);
        toChineese.setCursor(Cursor.HAND);
        userName.setText("core");
        password.setText("core");
        serverip.setText("localhost");
        
        paneError.setScaleX(0);
        paneError.setScaleY(0);
//        loginpane.clipProperty().set(userName);
        moveTwink();
//        moveCloud();
    }    

    @FXML
    private void userName(ActionEvent event) {
        
    }

    @FXML
    private void password(ActionEvent event) {
    }
    
    @FXML
    private void serverip(ActionEvent event) {
    }

    @FXML
    private void login(ActionEvent event) throws InterruptedException {
        authenticateLogin();
    }

    public Scene loadCurr(){
        try {
            singleTon = SingleTon.getSingleTon();
            
            System.out.println("Come");
            CurriculumAndGradeController cc = new CurriculumAndGradeController(clientSocket);
            AnchorPane page = (AnchorPane) FXMLLoader.load(getClass().getResource("CurriculumAndGrade.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            Scene scene = new Scene(page);
            
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void nextScreen(){
    }    
    
    @FXML
    private void english(ActionEvent event) {
        singleTon.setLocale(new Locale("en", "EN"));
        lusername.setText("User Name");
        lpassword.setText("Password");
        lip.setText("Server I.P.");
        luserlogin.setText("User Login");
        enter.setText("Enter");
    }

    @FXML
    private void espanol(ActionEvent event) {
        singleTon.setLocale(new Locale("es", "ES"));
        lusername.setText("Nombre de Usuario");
        lpassword.setText("Contraseña");
        lip.setText("I.P. servidor");
        luserlogin.setText("Login de Usuario");
        enter.setText("entrar en");
    }

    @FXML
    private void chineese(ActionEvent event) {
        singleTon.setLocale(new Locale("zh", "ZH"));
        lusername.setText("用户名");
        lpassword.setText("密码");
        lip.setText("服务器I.P.");
        luserlogin.setText("用户登录");
        enter.setText("進入");
    }

    @FXML
    private void francais(ActionEvent event) {
        singleTon.setLocale(new Locale("fr", "FR"));
        lusername.setText("Nom d'utilisateur");
        lpassword.setText("mot de passe");
        lip.setText("serveur I.P.");
        luserlogin.setText("login utilisateur");
        enter.setText("entrer");
    }
    
    @FXML
    private void logg(javafx.scene.input.KeyEvent event) {
        String code = event.getCode().toString();
        System.out.println(event.getCode());
        if(code.equals("ENTER")){
        try {
            authenticateLogin();
        } catch (InterruptedException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
    }
    
//    private boolean isEmpty(String s) {
//        return s == null || s.trim().isEmpty();
//    }

//    public void setbool(boolean b) {
//        LoginipController.nextscreen=b;
//        System.out.println("boole<<>>"+b);
//        System.out.println("next"+nextscreen);
//    }

    public Scene iview() {
         try {

            StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("simple"
                   + ".fxml"));
            ImageView imageView = clientSocket.getView();
            page.getChildren().add(imageView);
            Scene scene = new Scene(page);    
            return scene;
        } catch (IOException ex) {
            Logger.getLogger(LoginipController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @FXML
    private void closepaneAction(ActionEvent event) {
        paneError.setScaleX(0);
        paneError.setScaleY(0);
        labelError.setText("");
    }

    private void moveTwink() {
        
        panemove1.setLayoutX(0);
        panemove1.setLayoutX(0);
        panemove1.setPrefSize(2*maxX, 2*maxY);
    
        Path tpath = new Path();
        tpath.getElements().add(new MoveTo(0, 0));
        tpath.getElements().add(new LineTo(maxX, 0));
        tpath.getElements().add(new ClosePath());
    
        final PathTransition tpathTransition = new PathTransition();
        tpathTransition.setDuration(Duration.seconds(80.0));
//      tpathTransition.setDelay(Duration.seconds(2.0));
        tpathTransition.setPath(tpath);
        tpathTransition.setNode(panemove1);
//      tpathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        tpathTransition.setCycleCount(Timeline.INDEFINITE); 
        tpathTransition.setAutoReverse(true);
        tpathTransition.play();
    }
/*
    private void moveCloud() {
        panemove.setPrefHeight(2*maxY);
        panemove.setPrefWidth(3*(maxX/2));
        //        panemove.setPrefWidth((3*maxX)/2);
        panemove2.setPrefHeight(2*maxY);
        panemove2.setPrefWidth((3*(maxX/2))-20);
        //        panemove2.setPrefWidth((3*maxX)/2);
        panemove3.setPrefHeight(2*maxY);
        panemove3.setPrefWidth((3*(maxX/2))-20);
        //        panemove.setLayoutX(0);
        //        panemove.setLayoutY(0);
        //        panemove.setLayoutX((-panemove.getPrefWidth())+20);
        //        panemove.setLayoutY(0);
        System.out.println("wid"+panemove.getPrefWidth()+"total");
        Path path = new Path();
        path.getElements().add(new MoveTo((-maxX/2), (panemove.getPrefHeight())/2));
        path.getElements().add(new LineTo((2*(maxX)), (panemove.getPrefHeight())/2));
        final PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(40.0));
        pathTransition.setDelay(Duration.seconds(0.0));
        pathTransition.setPath(path);
        pathTransition.setNode(panemove2);
        //      pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(1); 
        pathTransition.play();

        pathTransition.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                pathTransition.play();
        //                 System.out.println("*************************************************************************************************************");
            }
        });
                   Path path2 = new Path();
               path2.getElements().add(new MoveTo((-maxX/2), (panemove.getPrefHeight())/2));
               path2.getElements().add(new LineTo((2*(maxX)), (panemove.getPrefHeight())/2));
                   final PathTransition pathTransition2 = new PathTransition();
                 pathTransition2.setDuration(Duration.seconds(40.0));
                 pathTransition.setDelay(Duration.seconds(0.0));
                 pathTransition2.setPath(path);
                 pathTransition2.setNode(panemove3);
        //                  pathTransition2.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
                 pathTransition2.setCycleCount(1); 

        //    
                pathTransition.currentTimeProperty().addListener(new ChangeListener<Duration>() {

            @Override
            public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
        //                 System.err.println(oldValue.toSeconds());
        //                 System.out.println(newValue.toSeconds());
        //                 System.out.println("pane"+path.toString());
                if((int)oldValue.toSeconds()==19){
                     pathTransition2.play();

        //                      System.out.println((int)oldValue.toSeconds()+"---------------------------------------------------++-+-----------------------+++++++++");
                }
            }
        });
        //                 pathTransition2.currentTimeProperty().addListener(new ChangeListener<Duration>() {
        //
        //             @Override
        //             public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
        ////                 System.err.println(oldValue.toSeconds());
        ////                 System.out.println(newValue.toSeconds());
        ////                 System.out.println("pane"+path.toString());
        //                 if((int)oldValue.toSeconds()==17){
        //                      pathTransition.play();
        //                      
        //                      System.out.println((int)oldValue.toSeconds()+"---------------------------------------------------++-+-----------------------+++++++++");
        //                 }
        //             }
        //         });
    }
  */  
    
    public void authenticateLogin() throws InterruptedException{
        
        if(!userName.getText().trim().equals("") && !password.getText().trim().equals("") && !serverip.getText().trim().equals("")){
            Controller controller =new Controller();
            MessageRequest messageRequest = controller.getUserField(userName.getText(), password.getText());
            
            clientSocket = ClientSocket.getClientSocket();
            clientSocket.createss(messageRequest, serverip.getText());

            if(clientSocket.getX()==1) {
                System.out.println("X : "+1);
                pageSwitch = new PageSwitch();
                Stage stage = (Stage) enter.getScene().getWindow();
                pageSwitch.home(stage);
            }else if(clientSocket.getX()==0){
                paneError.setScaleX(1);
                paneError.setScaleY(1);
                labelError.setText("Invalid user name and password");
            }else if(clientSocket.getX()==2){
                paneError.setScaleX(1);
                paneError.setScaleY(1);
                labelError.setText("User Already login");
            }else if(clientSocket.getX()==3){
                paneError.setScaleX(1);
                paneError.setScaleY(1);
                labelError.setText("Unable to connect Server I. P. = "+serverip.getText().trim());
            }
            System.out.println("bScene");
        }
    }
}