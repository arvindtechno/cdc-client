/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Track;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.CheckZIPRequest;
import org.coretechies.MessageTransporter.CreateUserSubmitRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SchoolDistrictRequest;
import org.coretechies.MessageTransporter.SchoolRequest;
import org.coretechies.MessageTransporter.StateRequest;
import org.coretechies.MessageTransporter.UpdateUserInfo;
import org.coretechies.MessageTransporter.UpdateUserSubmitRequest;
import org.coretechies.MessageTransporter.UserNameVerificationRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class CreateUserSchoolController extends SettingsController implements Initializable {
    @FXML
    private CheckBox checkSchoolUser;
    @FXML
    private ComboBox<String> selectCountry;
    @FXML
    private ComboBox<String> selectSchoolCategory;
    @FXML
    private ComboBox<String> selectSchoolDistrict;
    @FXML
    private ComboBox<String> selectState;
    @FXML
    private ComboBox<String> selectSchool;
    @FXML
    private ComboBox<String> selectUserType;
    private Pane paneSchoolUser;
    @FXML
    private Button btnSubmit;
    @FXML
    private TextField ZIP;
    @FXML
    private Button btnChooseFile;
    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
//    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField retypePassword;
    @FXML
    private Label lbUserNameVerification;
    @FXML
    private Label lbPasswordMatch;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private Label labelCreateUser;
    @FXML
    private Button btnUpdate;

    private SingleTon singleTon;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private AnchorPane anchorPaneSchoolUser;
    @FXML
    private HBox hboxSubmit;
    @FXML
    private HBox hboxUpdate;
    @FXML
    private HBox ERRORHbox;
    @FXML
    private Text textErrorInfo;
    @FXML
    private Text textMessageStart;
    @FXML
    private Button btnCloseError;
    @FXML
    private Button btnErrorYes;
    @FXML
    private Button btnErrorNo;
    @FXML
    private StackPane mainstackpane;
    @FXML
    private ScrollPane scrollpane;
    @FXML
    private Text userName1;
    @FXML
    private TextField userNameText;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.out.println("init.................");
        System.out.println("init.................");
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
        pageSwitch = new PageSwitch();
        singleTon = SingleTon.getSingleTon();
        userName1.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        ZIP.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    DistrictRequest();
                }
            }
        });
        
        userNameText.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setResponse(false);
                    System.out.println("Textfield out focus");
                    ClientSocket clientSocket =ClientSocket.getClientSocket();
            
                    Queue<MessageRequest> queue = clientSocket.getRequestQueue();

                    UserNameVerificationRequest userNameVerificationRequest = new UserNameVerificationRequest();
                    userNameVerificationRequest.setHash(clientSocket.getHash());
                    userNameVerificationRequest.setUserName(userNameText.getText());
                    userNameVerificationRequest.setMessageType("UserNameVerificationRequest");
                    queue.add(userNameVerificationRequest);
                    
                    while(!singleTon.isResponse()){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if(singleTon.isUserNameVerification())
                        lbUserNameVerification.setText("");
                    else
                        lbUserNameVerification.setText("User Name is already tafen. Please select a different user name.");
                    lbUserNameVerification.setStyle("-fx-text-fill:red;");
                }
            }
        });
        password.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }

            private void passwordMatch() {
                if(!password.getText().equals(retypePassword.getText()))
                    lbPasswordMatch.setText("Passwords do not match! Retype the password");
                else
                    lbPasswordMatch.setText("");
                lbPasswordMatch.setStyle("-fx-text-fill:red;");
            }
        });
        
        retypePassword.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    System.out.println("Textfield out focus");
                    passwordMatch();
                }
            }

            private void passwordMatch() {
                if(!password.getText().equals(retypePassword.getText()))
                    lbPasswordMatch.setText("Passwords do not match! Retype the password");
                else
                    lbPasswordMatch.setText("");
                lbPasswordMatch.setStyle("-fx-text-fill:red;");
            }
        });

        selectUserType.getItems().add("admin");
        selectUserType.getItems().add("Guest");
        
        if(singleTon.getCreateUser()==1){
            System.out.println("Create User");
            hboxUpdate.setScaleX(0);
            hboxUpdate.setScaleY(0);
            btnUpdate.setScaleX(0);
            btnUpdate.setScaleY(0);
            btnSubmit.setScaleX(1);
            btnSubmit.setScaleY(1);
//            labelUpdateUser.setScaleX(0);
//            labelUpdateUser.setScaleY(0);

            while(singleTon.getCountryList()==null && singleTon.getStateList() == null && singleTon.getSchoolCategaryList() == null){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            int size = singleTon.getCountryList().size();
            for(int i=0;i<size;i++){
                selectCountry.getItems().add(singleTon.getCountryList().get(i));
            }
//                if(singleTon.getCountryList().get(i).equalsIgnoreCase("United States of America"))
            selectCountry.setValue("United States of America");//setValue(singleTon.getCountryList().get(i));

            size = singleTon.getStateList().size();
            for(int i=0;i<size;i++){
                selectState.getItems().add(singleTon.getStateList().get(i));
            }

            size = singleTon.getSchoolCategaryList().size();
            for(int i=0;i<size;i++){
                selectSchoolCategory.getItems().add(singleTon.getSchoolCategaryList().get(i));
            }
            singleTon.setCreateUser(0);

        }else if(singleTon.getCreateUser()==2){
            System.out.println("UPDATE SCHOOL USER");
            btnSubmit.setScaleX(0);
            btnSubmit.setScaleY(0);
             
            hboxSubmit.setScaleX(0);
            hboxSubmit.setScaleY(0);
             
            hboxUpdate.setScaleX(1);
            hboxUpdate.setScaleY(1);
            btnUpdate.setScaleX(1);
            btnUpdate.setScaleY(1);
            
            labelCreateUser.setScaleX(0);
            labelCreateUser.setScaleY(0);
/*            btnUpdate.setScaleX(1);
            btnUpdate.setScaleY(1);
           labelUpdateUser.setScaleX(1);
            labelUpdateUser.setScaleY(1);
    */         singleTon.setCreateUser(0);
        
            singleTon = SingleTon.getSingleTon();
            while(singleTon.getUpdateUserInfo()==null){
                try {
                    System.out.print("1 ");
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            UpdateUserInfo updateUserInfo = singleTon.getUpdateUserInfo();
            userNameText.setText(updateUserInfo.getUserName());
            firstName.setText(updateUserInfo.getFirstName());
            lastName.setText(updateUserInfo.getLastName());
            password.setText(updateUserInfo.getPassword());
            retypePassword.setText(updateUserInfo.getPassword());
            selectUserType.setValue(updateUserInfo.getUserType());
            System.out.println("2");
            while(updateUserInfo.getCountry()==null){
                try {
                    System.out.println("3");
                        Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            System.out.println("Country "+updateUserInfo.getCountry());
            System.out.println("Country "+updateUserInfo.getCountry().equals("USA"));
            if(!updateUserInfo.getCountry().equals("USA")){
                
                selectCountry.setValue(updateUserInfo.getCountry());
                selectState.setValue(updateUserInfo.getState());
                selectSchoolDistrict.setValue(updateUserInfo.getSchoolDistrict());
                selectSchoolCategory.setValue(updateUserInfo.getSchoolCategary());
                selectSchool.setValue(updateUserInfo.getSchool());
//                selectUserType.setValue(updateUserInfo.getUserType());
                ZIP.setText(updateUserInfo.getZip());

                while(singleTon.getCountryList()==null && singleTon.getStateList() == null && singleTon.getSchoolCategaryList() == null && singleTon.getSchoolList() == null && singleTon.getSchoolDistrictList() == null){
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                int size = singleTon.getCountryList().size();
                for(int i=0;i<size;i++){
                    selectCountry.getItems().add(singleTon.getCountryList().get(i));
                }

                size = singleTon.getStateList().size();
                for(int i=0;i<size;i++){
                    selectState.getItems().add(singleTon.getStateList().get(i));
                }

                size = singleTon.getSchoolCategaryList().size();
                for(int i=0;i<size;i++){
                    selectSchoolCategory.getItems().add(singleTon.getSchoolCategaryList().get(i));
                }

                size = singleTon.getSchoolDistrictList().size();
                for(int i=0;i<size;i++){
                    selectSchoolDistrict.getItems().add(singleTon.getSchoolDistrictList().get(i));
                }

                size = singleTon.getSchoolList().size();
                for(int i=0;i<size;i++){
                    selectSchool.getItems().add(singleTon.getSchoolList().get(i));
                }
            }else{
                checkSchoolUser.setSelected(false);
                checkSchoolUser.setDisable(true);
                paneSchoolUser.setScaleX(0);
                paneSchoolUser.setScaleY(0);
            }
            
        }else if(singleTon.getCreateUser()==3){
//            System.out.println("UPDATE USER");
//            btnSubmit.setScaleX(0);
//            btnSubmit.setScaleY(0);
//            labelCreateUser.setScaleX(0);
//            labelCreateUser.setScaleY(0);
//            btnUpdate.setScaleX(1);
//            btnUpdate.setScaleY(1);
//            labelUpdateUser.setScaleX(1);
//            labelUpdateUser.setScaleY(1);
//            singleTon.setCreateUser(0);
//        
//            while(singleTon.getUpdateUserInfo()==null){
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//            UpdateUserInfo updateUserInfo = singleTon.getUpdateUserInfo();
//            selectCountry.setValue(updateUserInfo.getCountry());
//            selectState.setValue(updateUserInfo.getState());
//            selectSchoolDistrict.setValue(updateUserInfo.getSchoolDistrict());
//            selectSchoolCategory.setValue(updateUserInfo.getSchoolCategary());
//            selectSchool.setValue(updateUserInfo.getSchool());
//            selectUserType.setValue(updateUserInfo.getUserType());
//            ZIP.setText(updateUserInfo.getZip());
//            
//            userName.setText(updateUserInfo.getUserName());
//            firstName.setText(updateUserInfo.getFirstName());
//            lastName.setText(updateUserInfo.getLastName());
//            password.setText(updateUserInfo.getPassword());
//            retypePassword.setText(updateUserInfo.getPassword());
//            
//            checkSchoolUser.setDisable(true);
//            paneSchoolUser.setScaleX(0);
//            paneSchoolUser.setScaleY(0);
        }
    }    

    @FXML
    private void schoolUser(ActionEvent event) {
        if(checkSchoolUser.isSelected()){
            anchorPaneSchoolUser.setPrefHeight(192);
            anchorPaneSchoolUser.setOpacity(1);
        }else{
            anchorPaneSchoolUser.setPrefHeight(0);
            anchorPaneSchoolUser.setOpacity(0);
        }
    }

    @FXML
    private void country(ActionEvent event) {
        ClientSocket clientSocket =ClientSocket.getClientSocket();
            
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        StateRequest stateRequest = new StateRequest();
        stateRequest.setHash(clientSocket.getHash());
        stateRequest.setCountry(selectCountry.getSelectionModel().getSelectedItem());
        stateRequest.setMessageType("StateList");
        singleTon = SingleTon.getSingleTon();
        selectState.getItems().clear();
        selectSchoolDistrict.getItems().clear();
        selectSchool.getItems().clear();
        ZIP.setText("");
        queue.add(stateRequest);
    }

    @FXML
    private void schoolCategory(ActionEvent event) {
        DistrictRequest();
    }

    @FXML
    private void schoolDistrict(ActionEvent event) {
        ClientSocket clientSocket = ClientSocket.getClientSocket();
            
        if(ZIP.getText().length()>0){
            System.out.println("AT District11 FUNCTION");
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();

            SchoolRequest schoolRequest = new SchoolRequest();

            schoolRequest.setHash(clientSocket.getHash());
            schoolRequest.setCountry(selectCountry.getSelectionModel().getSelectedItem());
            schoolRequest.setState(selectState.getSelectionModel().getSelectedItem());
            schoolRequest.setSchoolCategory(selectSchoolCategory.getSelectionModel().getSelectedItem());
            schoolRequest.setZIP(Integer.parseInt(ZIP.getText()));
            schoolRequest.setSchoolDistrict(selectSchoolDistrict.getSelectionModel().getSelectedItem());
            schoolRequest.setMessageType("SchoolRequest");
            singleTon = SingleTon.getSingleTon();
            queue.add(schoolRequest);
        }
    }

    @FXML
    private void state(ActionEvent event) {
    }

    @FXML
    private void school(ActionEvent event) {
    }

    @FXML
    private void userType(ActionEvent event) {
    }

    @FXML
    private void submit(ActionEvent event) {
        System.out.println("ASDASD");
        if(lbUserNameVerification.getText().equals("")){
            if(checkSchoolUser.isSelected()){
                System.out.println("Selected");
                if(!(selectCountry.getSelectionModel().getSelectedItem().equals(null) && selectSchool.getSelectionModel().getSelectedItem().equals(null) && selectState.getSelectionModel().getSelectedItem().equals(null) && selectSchoolDistrict.getSelectionModel().getSelectedItem().equals(null) && selectSchoolCategory.getSelectionModel().getSelectedItem().equals(null) && ZIP.getText().equals("") && firstName.getText().equals("") && lastName.getText().equals("") && userNameText.getText().equals("") && password.getText().equals("") && retypePassword.getText().equals("") && selectUserType.getSelectionModel().getSelectedItem().equals(null))){
                    System.out.println("SEKECT");
                    if(password.getText().equals(retypePassword.getText())){
                        ClientSocket clientSocket =ClientSocket.getClientSocket();

                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        System.out.println("<<<<<<<<<<<<<<<<");
                        CreateUserSubmitRequest createUserSubmitRequest = new CreateUserSubmitRequest();
                        createUserSubmitRequest.setHash(clientSocket.getHash());
                        createUserSubmitRequest.setCountryName(selectCountry.getSelectionModel().getSelectedItem());
                        createUserSubmitRequest.setStateName(selectState.getSelectionModel().getSelectedItem());
                        createUserSubmitRequest.setDistrictName(selectSchoolDistrict.getSelectionModel().getSelectedItem());
                        createUserSubmitRequest.setSchoolCategory(selectSchoolCategory.getSelectionModel().getSelectedItem());
                        createUserSubmitRequest.setSchoolName(selectSchool.getSelectionModel().getSelectedItem());
                        createUserSubmitRequest.setZip(ZIP.getText());
                        createUserSubmitRequest.setFirstName(firstName.getText());
                        createUserSubmitRequest.setLastName(lastName.getText());
                        createUserSubmitRequest.setUserName(userNameText.getText());
                        createUserSubmitRequest.setPassword(password.getText());
                        createUserSubmitRequest.setUserType(selectUserType.getSelectionModel().getSelectedItem());

                        createUserSubmitRequest.setMessageType("CreateSchoolUserSubmitRequest");
                        queue.add(createUserSubmitRequest);
                    }
                }
            }else{
                if(!(firstName.getText().equals("") && lastName.getText().equals("") && userNameText.getText().equals("") && password.getText().equals("") && retypePassword.getText().equals("") && selectUserType.getSelectionModel().getSelectedItem().equals(null))){
                    if(password.getText().equals(retypePassword.getText())){
                        ClientSocket clientSocket =ClientSocket.getClientSocket();

                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        System.out.println("<<<<<<<<<<<<<<<<");
                        CreateUserSubmitRequest createUserSubmitRequest = new CreateUserSubmitRequest();
                        createUserSubmitRequest.setHash(clientSocket.getHash());

                        createUserSubmitRequest.setFirstName(firstName.getText());
                        createUserSubmitRequest.setLastName(lastName.getText());
                        createUserSubmitRequest.setUserName(userNameText.getText());
                        createUserSubmitRequest.setPassword(password.getText());
                        createUserSubmitRequest.setUserType(selectUserType.getSelectionModel().getSelectedItem());

                        createUserSubmitRequest.setMessageType("CreateUserSubmitRequest");
                        queue.add(createUserSubmitRequest);
                    }
                }
            }
            Stage stage = (Stage) btnSubmit.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Settings.fxml");
        }
    }

    @FXML
    private void ChooseFile(ActionEvent event) {
    }

    @FXML
    private void OnShowingSchoolDistrict(Event event) {
        selectSchoolDistrict.getItems().clear();
        if(!(selectCountry.getSelectionModel().getSelectedItem() == null || selectState.getSelectionModel().getSelectedItem() == null || selectSchoolCategory.getSelectionModel().getSelectedItem() == null && ZIP.getText().equals(""))){
            System.out.println("on showing school district");
            Service<Void> service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            //Background work
                            while(!singleTon.isResponse()){
                                Thread.sleep(50);
                                singleTon.setResponse(false);
                            }Thread.sleep(50);
                            
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    int size = singleTon.getSchoolDistrictList().size();
                                    for(int i=0;i<size;i++){
                                        selectSchoolDistrict.getItems().add(singleTon.getSchoolDistrictList().get(i));
                                    }
                                }
                            });
                            return null;
                        }
                    };
                }
            };
            service.start();
        }
    }

    @FXML
    private void OnShowningSchool(Event event) {
        selectSchool.getItems().clear();
        singleTon = SingleTon.getSingleTon();
        if(!(selectCountry.getSelectionModel().getSelectedItem() == null || selectState.getSelectionModel().getSelectedItem() == null || selectSchoolCategory.getSelectionModel().getSelectedItem() == null || selectSchoolDistrict.getSelectionModel().getSelectedItem() == null || ZIP.getText().equals(""))){
            Service<Void> service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            //Background work
                            while(!singleTon.isResponse()){
                                Thread.sleep(50);
                            }Thread.sleep(50);
                            
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    int size = singleTon.getSchoolList().size();
                                    for(int i=0;i<size;i++){
                                        selectSchool.getItems().add(singleTon.getSchoolList().get(i));
                                    }
                                }
                            });
                            return null;
                        }
                    };
                }
            };
            service.start();
//            try {
//                while(!singleTon.isResponse()){
//                    Thread.sleep(50);
//                }Thread.sleep(50);
//                int size = singleTon.getSchoolList().size();
//                for(int i=0;i<size;i++){
//                    selectSchool.getItems().add(singleTon.getSchoolList().get(i));
//                }
//            } catch (InterruptedException ex) {
//                Logger.getLogger(CreateUserSchoolController.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }

    @FXML
    private void OnShowingState(Event event) {
//        selectState.getItems().clear();
//        singleTon = SingleTon.getSingleTon();
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        //Background work
                        selectState.getItems().clear();
                        singleTon = SingleTon.getSingleTon();
                        while(singleTon.getStateList()==null){
                            Thread.sleep(50);
                        }Thread.sleep(50);

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                int size = singleTon.getStateList().size();
                                for(int i=0;i<size;i++){
                                    selectState.getItems().add(singleTon.getStateList().get(i));
                                }
                            }
                        });
                        return null;
                    }
                };
            }
        };
        service.start();
//        while(singleTon.getStateList()==null){}
//        int size = singleTon.getStateList().size();
//        for(int i=0;i<size;i++){
//            selectState.getItems().add(singleTon.getStateList().get(i));
//        }
    }
    
    private void DistrictRequest(){
        if(ZIP.getText().length()!=0){
            if(!(selectCountry.getSelectionModel().getSelectedItem().equals("") && selectState.getSelectionModel().getSelectedItem().equals("") && selectSchoolCategory.getSelectionModel().getSelectedItem().equals("") && ZIP.getText().length()<5 && (!ZIP.getText().toString().equals(""))))
            {
                System.out.println("UAAAA");
                ClientSocket clientSocket =ClientSocket.getClientSocket();

                System.out.println("AT CHAPTER FUNCTION");
                Queue<MessageRequest> queue = clientSocket.getRequestQueue();

                SchoolDistrictRequest schoolDistrictRequest = new SchoolDistrictRequest();
                schoolDistrictRequest.setHash(clientSocket.getHash());
                schoolDistrictRequest.setCountry(selectCountry.getSelectionModel().getSelectedItem());
                schoolDistrictRequest.setState(selectState.getSelectionModel().getSelectedItem());
                schoolDistrictRequest.setSchoolCategory(selectSchoolCategory.getSelectionModel().getSelectedItem());
                schoolDistrictRequest.setZIP(Integer.parseInt(ZIP.getText()));

                schoolDistrictRequest.setMessageType("SchoolDistrictRequest");
                singleTon = SingleTon.getSingleTon();
                selectSchoolDistrict.getItems().clear();
                selectSchool.getItems().clear();

                queue.add(schoolDistrictRequest);
            }
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void update(ActionEvent event) {
        System.out.println("UPDATE");
        if(lbUserNameVerification.getText().equals("")){
            if(checkSchoolUser.isSelected()){
                System.out.println("Selected");
                if(!(selectCountry.getSelectionModel().getSelectedItem() == null && selectSchool.getSelectionModel().getSelectedItem() == null && selectState.getSelectionModel().getSelectedItem() == null && selectSchoolDistrict.getSelectionModel().getSelectedItem() == null && selectSchoolCategory.getSelectionModel().getSelectedItem() == null && ZIP.getText().equals("") && firstName.getText().equals("") && lastName.getText().equals("") && userNameText.getText().equals("") && password.getText().equals("") && retypePassword.getText().equals("") && selectUserType.getSelectionModel().getSelectedItem() == null)){
                    System.out.println("SEKECT");
                    if(password.getText().equals(retypePassword.getText())){
                        ClientSocket clientSocket =ClientSocket.getClientSocket();

                        singleTon = SingleTon.getSingleTon();
                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        System.out.println("<<<<<<<<<<<<<<<<");
                        UpdateUserSubmitRequest updateUserSubmitRequest = new UpdateUserSubmitRequest();
                        updateUserSubmitRequest.setHash(clientSocket.getHash());
                        updateUserSubmitRequest.setCountryName(selectCountry.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setStateName(selectState.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setDistrictName(selectSchoolDistrict.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setSchoolCategory(selectSchoolCategory.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setSchoolName(selectSchool.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setZip(ZIP.getText());
                        updateUserSubmitRequest.setFirstName(firstName.getText());
                        updateUserSubmitRequest.setLastName(lastName.getText());
                        updateUserSubmitRequest.setUserName(userNameText.getText());
                        updateUserSubmitRequest.setPassword(password.getText());
                        updateUserSubmitRequest.setUserType(selectUserType.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setUserId(singleTon.getUpdateUserId());

                        updateUserSubmitRequest.setMessageType("UpdateSchoolUserSubmitRequest");
                        queue.add(updateUserSubmitRequest);
                        singleTon = SingleTon.getSingleTon();
                        singleTon.setUpdateUserList(null);
                        
                        Stage stage = (Stage) btnUpdate.getScene().getWindow();
                        pageSwitch.nextScreen(stage, "Settings.fxml");
                    }
                }
            }else{
                if(!(firstName.getText().equals("") && lastName.getText().equals("") && userNameText.getText().equals("") && password.getText().equals("") && retypePassword.getText().equals("") && selectUserType.getSelectionModel().getSelectedItem().equals(null))){
                    if(password.getText().equals(retypePassword.getText())){
                        ClientSocket clientSocket =ClientSocket.getClientSocket();

                        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                        System.out.println("<<<<<<<<<<<<<<<<");
                        UpdateUserSubmitRequest updateUserSubmitRequest = new UpdateUserSubmitRequest();
                        updateUserSubmitRequest.setHash(clientSocket.getHash());

                        updateUserSubmitRequest.setFirstName(firstName.getText());
                        updateUserSubmitRequest.setLastName(lastName.getText());
                        updateUserSubmitRequest.setUserName(userNameText.getText());
                        updateUserSubmitRequest.setPassword(password.getText());
                        updateUserSubmitRequest.setUserType(selectUserType.getSelectionModel().getSelectedItem());
                        updateUserSubmitRequest.setUserId(singleTon.getUpdateUserId());
                        
                        updateUserSubmitRequest.setMessageType("UpdateUserSubmitRequest");
                        queue.add(updateUserSubmitRequest);
                        
                        Stage stage = (Stage) btnUpdate.getScene().getWindow();
                        pageSwitch.nextScreen(stage, "Settings.fxml");
                    }
                }
            }
        }
    }

    @FXML
    private void closeErrorAction(ActionEvent event) {
    }

    @FXML
    private void errorYesAction(ActionEvent event) {
    }

    @FXML
    private void errorNoAction(ActionEvent event) {
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "Settings.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
