/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import com.sun.java.accessibility.util.AWTEventMonitor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.coretechies.Client.SingleTon;

/**
 *
 * @author user
 */
public class CDCClient extends Application {
    
    private SingleTon singleTon;
    
    @Override
    public void start(Stage primaryStage) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLocale(new Locale("en", "EN"));
        try {
            StackPane page = (StackPane) FXMLLoader.load(getClass().getResource("Loginip.fxml"), ResourceBundle.getBundle("org.coretechies.forms.language.bundle", singleTon.getLocale()));
            
            Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
            double maxX= primaryScreenBounds.getMaxX();
            double maxY= primaryScreenBounds.getMaxY();
            
            Scene scene = new Scene(page);
            
            primaryStage.setScene(scene);
//            primaryStage.fullScreenProperty();
            primaryStage.setMaximized(true);
            primaryStage.setHeight(maxY);
            primaryStage.setWidth(maxX);
            primaryStage.setTitle("CDC");
            primaryStage.show();
//            AWTEventMonitor.addWindowListener(new WindowAdapter() {
//
//                @Override
//                public void windowClosing(WindowEvent e) {
//                    super.windowClosing(e); //To change body of generated methods, choose Tools | Templates.
//                }
//            });
            
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        
    }

   public void start1(Stage primaryStage) {
      primaryStage.show();
    
}}
