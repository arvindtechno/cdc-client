/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SearchRequest;
import org.coretechies.MessageTransporter.SearchResponseData;
import org.coretechies.MessageTransporter.SearchSortedRequest;
import org.coretechies.forms.controller.java.SearchResultView;

/**
 * FXML Controller class
 *
 * @author Tiwari
 */
public class SearchController implements Initializable {
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private ComboBox<String> cboxCurriculum;
    @FXML
    private ComboBox<String> cboxSubject;
    @FXML
    private ComboBox<String> cboxGrade;
    @FXML
    private TableView<SearchResultView> tableSearchView;
    @FXML
    private TableColumn columnSubject;
    @FXML
    private TableColumn columnChapter;
    @FXML
    private TableColumn columnLesson;
    @FXML
    private TableColumn columnTopic;
    @FXML
    private TableColumn columnGrade;
    @FXML
    private TableColumn columnCurriculum;
    @FXML
    private TableColumn columnClickBelow;
    @FXML
    private Button search;
    @FXML
    private TextField txtSearch;
    ObservableList<SearchResultView> data;
    private SingleTon singleTon;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelFilterby;
    @FXML
    private Label labelCurriculum;
    @FXML
    private Label labelSubject;
    @FXML
    private Label labelGrade;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelCNG;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Text userName;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pageSwitch = new PageSwitch();
        singleTon = SingleTon.getSingleTon();
        singleTon.setSearch(false);
        
        columnSubject.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("subject")
        );
        columnChapter.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("chapter")
        );
        columnLesson.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("lesson")
        );
        columnTopic.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("topic")
        );
        columnGrade.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("grade")
        );
        columnCurriculum.setCellValueFactory(
            new PropertyValueFactory<SearchResultView,String>("curriculum")
        );
        
        columnClickBelow.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<SearchResultView, Boolean>, 
                ObservableValue<Boolean>>() {
 
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<SearchResultView, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        
        columnClickBelow.setCellFactory(
                new Callback<TableColumn<SearchResultView, Boolean>, TableCell<SearchResultView, Boolean>>() {          

            @Override
            public TableCell<SearchResultView, Boolean> call(TableColumn<SearchResultView, Boolean> param) {
                 return new SearchController.CreateButtonUpdate();
            }
        
        });
        
        data = FXCollections.observableArrayList();
        tableSearchView.setItems(data);
        System.err.println("1");
        setTable();
        System.err.println("2");
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void curriculum(ActionEvent event) {
        sortResult();
    }

    @FXML
    private void subject(ActionEvent event) {
        sortResult();
    }

    @FXML
    private void grade(ActionEvent event) {
        sortResult();
    }


    private void searchcontent(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            System.out.println("<<<<<<<<<<<<<<<<");
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            tableSearchView.getItems().clear();
            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            setTable();
        }
    }

    @FXML
    private void searchView(SortEvent event) {
    }

    private void sortResult(){
        if(!(cboxCurriculum.getSelectionModel().isEmpty() || cboxGrade.getSelectionModel().isEmpty() || cboxSubject.getSelectionModel().isEmpty())){
            tableSearchView.getItems().clear();
            
            for(SearchResponseData srd : singleTon.getSearchResponseDatas()){
                if(srd.getCurriculum().endsWith(cboxCurriculum.getSelectionModel().getSelectedItem())){
                    if(srd.getGrade().endsWith(cboxGrade.getSelectionModel().getSelectedItem())){
                        if(srd.getSubject().endsWith(cboxSubject.getSelectionModel().getSelectedItem())){
                            SearchResultView lessonScheduleView = new SearchResultView();

                            lessonScheduleView.subject.setValue(srd.getSubject());
                            lessonScheduleView.chapter.setValue(srd.getChapter());
                            lessonScheduleView.lesson.setValue(srd.getLesson());
                            lessonScheduleView.topic.setValue(srd.getTopic());
                            lessonScheduleView.grade.setValue(srd.getGrade());
                            lessonScheduleView.curriculum.setValue(srd.getCurriculum());

                            data.add(lessonScheduleView);
                        }
                    }
                }
            }
        }
    }
    
    private void setTable() {
        singleTon = SingleTon.getSingleTon();
        while(!singleTon.isLessonPlanResponse()){
            try {
                System.out.print("waiting");
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getSearchResponseDatas().size();
        for(int i=0; i<size; i++){
            System.out.println("DATA"+i);
            System.out.println("DATA"+size);
            SearchResultView lessonScheduleView = new SearchResultView();

            lessonScheduleView.subject.setValue(singleTon.getSearchResponseDatas().get(i).getSubject());
            lessonScheduleView.chapter.setValue(singleTon.getSearchResponseDatas().get(i).getChapter());
            lessonScheduleView.lesson.setValue(singleTon.getSearchResponseDatas().get(i).getLesson());
            lessonScheduleView.topic.setValue(singleTon.getSearchResponseDatas().get(i).getTopic());
            lessonScheduleView.grade.setValue(singleTon.getSearchResponseDatas().get(i).getGrade());
            lessonScheduleView.curriculum.setValue(singleTon.getSearchResponseDatas().get(i).getCurriculum());
            
            data.add(lessonScheduleView);
        }
        
        singleTon = SingleTon.getSingleTon();
        while(singleTon.getSearchSubjectList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        size = singleTon.getSearchSubjectList().size();
        for(int i=0;i<size;i++){
            cboxSubject.getItems().add(singleTon.getSearchSubjectList().get(i));
        }
        
        while(singleTon.getSearchCurriculumList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        size = singleTon.getSearchCurriculumList().size();
        for(int i=0;i<size;i++){
            cboxCurriculum.getItems().add(singleTon.getSearchCurriculumList().get(i));
        }
        
        while(singleTon.getSearchGradeList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        size = singleTon.getSearchGradeList().size();
        for(int i=0;i<size;i++){
            cboxGrade.getItems().add(singleTon.getSearchGradeList().get(i));
        }
        
//        singleTon.setSearchResponseDatas(null);
        singleTon.setSearchSubjectList(null);
        singleTon.setSearchGradeList(null);
        singleTon.setSearchCurriculumList(null);
        singleTon.setLessonPlanResponse(false);
    }

    @FXML
    private void searchContent(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            System.out.println("<<<<<<<<<<<<<<<<");
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            tableSearchView.getItems().clear();
            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            setTable();
            txtSearch.setText(null);
        }
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
    
    public class CreateButtonUpdate extends TableCell<SearchResultView, Boolean> {
        final Button updateButton = new Button("Start Now >>");
        public CreateButtonUpdate(){
            System.out.println(CreateButtonUpdate.this.getIndex());
            updateButton.setCursor(Cursor.HAND);
            updateButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    SearchResultView item = (SearchResultView) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    singleTon = SingleTon.getSingleTon();
                    singleTon.setSearchChapter(item.getChapter());
                    singleTon.setSearchLesson(item.getLesson());
                    singleTon.setSearchTopic(item.getTopic());
                    singleTon.setSearch(true);

                    Stage stage = (Stage) updateButton.getScene().getWindow();
                    pageSwitch.nextScreen(stage, "Subject.fxml");
                }
            });
        }
          
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
            }else{
                setGraphic(null);
            }
        }
    }

}
