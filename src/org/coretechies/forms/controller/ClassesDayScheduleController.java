/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.LessonPlanningRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdateClassDayScheduleRequest;
import org.coretechies.forms.controller.java.EditingCell;
import org.coretechies.forms.controller.java.classDayScheduleView;

/**
 * FXML Controller class
 *
 * @author user
 */
public class ClassesDayScheduleController implements Initializable {
    @FXML
    private Button btnUpdateSchedule;
    @FXML
    private Button btnHome;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnBack;
    @FXML
    private TableView<classDayScheduleView> classDayScheduleTable;
    @FXML
    private TableColumn colMonday;
    @FXML
    private TableColumn colTuesday;
    @FXML
    private TableColumn colWednesday;
    @FXML
    private TableColumn colThursday;
    @FXML
    private TableColumn colFriday;
    
    ObservableList<classDayScheduleView> data;

    SingleTon singleTon;
    
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private HBox ErrorHbox;
    @FXML
    private Button btnCloseError;
    @FXML
    private Button btnNavigateYes;
    @FXML
    private Button btnNavigateNo;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        ErrorHbox.setScaleX(0);
        ErrorHbox.setScaleY(0);
        pageSwitch = new PageSwitch();
        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };
        colMonday.setCellValueFactory(
            new PropertyValueFactory<classDayScheduleView,String>("monday")
        );
        colTuesday.setCellValueFactory(
            new PropertyValueFactory<classDayScheduleView,String>("tuesday")
        );
        colWednesday.setCellValueFactory(
            new PropertyValueFactory<classDayScheduleView,String>("wednesday")
        );
        colThursday.setCellValueFactory(
            new PropertyValueFactory<classDayScheduleView,String>("thursday")
        );
        colFriday.setCellValueFactory(
            new PropertyValueFactory<classDayScheduleView,String>("friday")
        );
        colMonday.setCellFactory(new Callback<TableColumn<classDayScheduleView, String>, TableCell<classDayScheduleView, String>>(){
            @Override
            public TableCell<classDayScheduleView, String> call(TableColumn<classDayScheduleView, String> p) {
                TableCell<classDayScheduleView, String> tc = new TableCell<classDayScheduleView, String>();
                tc.setAlignment(Pos.CENTER);
                return tc;
            }
        });

        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        while(singleTon.getScheduleList()==null){
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClassesDayScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        classDayScheduleView dsv = new classDayScheduleView(singleTon.getScheduleList().get(0)+"",singleTon.getScheduleList().get(1)+"",singleTon.getScheduleList().get(2)+"",singleTon.getScheduleList().get(3)+"",singleTon.getScheduleList().get(4)+"");
        classDayScheduleTable.setEditable(true);
        data = FXCollections.observableArrayList();
        classDayScheduleTable.setItems(data);
        colFriday.setCellFactory(cellFactory);
        colMonday.setCellFactory(cellFactory);
        colThursday.setCellFactory(cellFactory);
        colTuesday.setCellFactory(cellFactory);
        colWednesday.setCellFactory(cellFactory);
        System.out.println("asd");
        data.add(dsv);
        classDayScheduleTable.setEditable(true);
    }    

    @FXML
    private void updateSchedule(ActionEvent event) {

        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        UpdateClassDayScheduleRequest classDayScheduleRequest = new UpdateClassDayScheduleRequest();
        classDayScheduleRequest.setHash(clientSocket.getHash());
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(Integer.parseInt(colMonday.getCellData(0)+""));
        arrayList.add(Integer.parseInt(colTuesday.getCellData(0)+""));
        arrayList.add(Integer.parseInt(colWednesday.getCellData(0)+""));
        arrayList.add(Integer.parseInt(colThursday.getCellData(0)+""));
        arrayList.add(Integer.parseInt(colFriday.getCellData(0)+""));
        classDayScheduleRequest.setScheduleList(arrayList);
        classDayScheduleRequest.setMessageType("UpdateClassDayScheduleRequest");
        queue.add(classDayScheduleRequest);

        singleTon = SingleTon.getSingleTon();
        singleTon.setScheduleList(arrayList);
        while(!singleTon.isUpdateClassDayScheduleResponse()){
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClassesDayScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ErrorHbox.setScaleX(1);
        ErrorHbox.setScaleY(1);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void ActionCrose(ActionEvent event) {
        ErrorHbox.setScaleX(0);
        ErrorHbox.setScaleY(0);
    }

    @FXML
    private void ActionYes(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanningList(null);
        singleTon.setLessonPlanResponse(false);
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        
        LessonPlanningRequest lessonPlanningRequest = new LessonPlanningRequest();
        lessonPlanningRequest.setHash(clientSocket.getHash());
        lessonPlanningRequest.setMessageType("LessonPlanningRequest");
        queue.add(lessonPlanningRequest);
                
        singleTon = SingleTon.getSingleTon();
        while(singleTon.isLessonPlanResponse()==false){
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(LessonScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(singleTon.isErrorResponse()){
            Stage stage = (Stage) btnNavigateYes.getScene().getWindow();
            pageSwitch.nextScreen(stage, "LessonSchedule.fxml");
        }else{
            singleTon.setLessonPlanningErrorPane(true);
            Stage stage = (Stage) btnNavigateYes.getScene().getWindow();
            pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
        }
    }

    @FXML
    private void ActionNo(ActionEvent event) {
        Stage stage = (Stage) btnNavigateNo.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
    
}
