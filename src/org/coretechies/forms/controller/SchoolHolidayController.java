/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.AddSchoolHolidayRequest;
import org.coretechies.MessageTransporter.DeleteSchoolHolidayRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.UpdateSchoolHolidayRequest;
import org.coretechies.forms.controller.java.ItemHoliday;

/**
 * FXML Controller class
 *
 * @author user
 */
public class SchoolHolidayController implements Initializable {
   
    @FXML
    private Button btnAddSchoolHoliday;
    @FXML
    private Pane paneAddHoliday;
    @FXML
    private DatePicker holidayDate;
    @FXML
    private TextArea TextAreaComment;
    @FXML
    private Button btnSubmit;
    @FXML
    private TableColumn coloumnDate;
    @FXML
    private TableColumn coloumnComment;
    @FXML
    private TableColumn coloumnUpdate;
    @FXML
    private TableColumn coloumnDelete;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    @FXML
    private TableView<ItemHoliday> holidayTable;
    ObservableList<ItemHoliday> data;
    
    SingleTon singleTon;
    @FXML
    private Label labelAddHoliday;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnClosePaneAddLeave;
    @FXML
    private HBox hboxAddHoliday;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    
    String updateId;
    private ItemHoliday updateItem;
    private PageSwitch pageSwitch;
    @FXML
    private BorderPane borderPaneMain;
    @FXML
    private HBox ERRORHbox;
    @FXML
    private Text textErrorInfo;
    @FXML
    private Text textMessage;
    @FXML
    private Button btnCloseError;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
        hboxAddHoliday.setScaleX(0);
        hboxAddHoliday.setScaleY(0);
        pageSwitch = new PageSwitch();
        paneAddHoliday.setScaleX(0);
        paneAddHoliday.setScaleY(0);
        coloumnDate.setCellValueFactory(
            new PropertyValueFactory<ItemHoliday,String>("date")
        );
        coloumnComment.setCellValueFactory(
            new PropertyValueFactory<ItemHoliday,String>("comment")
        );
        coloumnUpdate.setCellFactory(
                new Callback<TableColumn<ItemHoliday, Boolean>, TableCell<ItemHoliday, Boolean>>() {
 
           

            @Override
            public TableCell<ItemHoliday, Boolean> call(TableColumn<ItemHoliday, Boolean> param) {
                 return new DateAllign();
            }
        
        });
        
        coloumnUpdate.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<ItemHoliday, Boolean>, 
                ObservableValue<Boolean>>() {
 
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ItemHoliday, Boolean> p) {
               
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
         
        coloumnUpdate.setCellFactory(
            new Callback<TableColumn<ItemHoliday, Boolean>, TableCell<ItemHoliday, Boolean>>() {
 
            @Override
            public TableCell<ItemHoliday, Boolean> call(TableColumn<ItemHoliday, Boolean> param) {
                 return new CreateButtonUpdate();
            }
        });
        coloumnDelete.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<ItemHoliday, Boolean>, 
                ObservableValue<Boolean>>() {
 
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ItemHoliday, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        coloumnDelete.setCellFactory(
                new Callback<TableColumn<ItemHoliday, Boolean>, TableCell<ItemHoliday, Boolean>>() {
 
           

            @Override
            public TableCell<ItemHoliday, Boolean> call(TableColumn<ItemHoliday, Boolean> param) {
                 return new CreateButtonDelete();
            }
        
        });
        data = FXCollections.observableArrayList();
        holidayTable.setItems(data);
        
        singleTon = SingleTon.getSingleTon();
        while(singleTon.getSchoolHolidayList()==null){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(SchoolHolidayController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        int size = singleTon.getSchoolHolidayList().size();
        
        for(int i=0;i<size;i++) {
            System.out.println("DATA");
            ItemHoliday item = new ItemHoliday();
            item.date.setValue(singleTon.getSchoolHolidayList().get(i).getDate().toString());
            item.comment.setValue(singleTon.getSchoolHolidayList().get(i).getComment());
            item.setId(singleTon.getSchoolHolidayList().get(i).getId());
            data.add(item);
        }
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
    }

    @FXML
    private void addSchoolholiday(ActionEvent event) {
        labelAddHoliday.setText("Add New Holiday Date Below");
        btnSubmit.setScaleX(1);
        btnSubmit.setScaleY(1);
        hboxAddHoliday.setScaleX(1);
        hboxAddHoliday.setScaleY(1);
        
        paneAddHoliday.setScaleX(1);
        paneAddHoliday.setScaleY(1);
        btnUpdate.setScaleX(0);
        btnUpdate.setScaleY(0);
        
//        leaveDate.setValue();
        TextAreaComment.setText("");
        
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    
                    @Override 
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (item.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                            setDisable(true);
                        }if (item.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                            setDisable(true);
                        }if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                        }
                        
                        int size = data.size();
                        for(int i = 0; size>i; i++){
                            String date = data.get(i).date.getValue();
                            if (item.getDayOfMonth()==Integer.parseInt(date.substring(8, 10)) && item.getMonthValue()==Integer.parseInt(date.substring(5, 7)) && item.getYear()==Integer.parseInt(date.substring(0, 4))) {
                                if(updateItem!=null){
                                    String updateDate = updateItem.date.getValue();
                                    if(!(item.getDayOfMonth()==Integer.parseInt(updateDate.substring(8, 10)) && item.getMonthValue()==Integer.parseInt(updateDate.substring(5, 7)) && item.getYear()==Integer.parseInt(updateDate.substring(0, 4))))
                                        setDisable(true);
                                }else{
                                    setDisable(true);
                                }
                            }
                        }
                    }
                };
            }
        };
        holidayDate.setDayCellFactory(dayCellFactory);        
    }

    @FXML
    private void getHolidayDate(ActionEvent event) {
    }

    @FXML
    private void submit(ActionEvent event) {
        
        System.out.println(holidayDate.getValue());
        System.out.println(TextAreaComment.getText());
        String nextId = holidayDate.getValue().toString();

        singleTon = SingleTon.getSingleTon();
        singleTon.setSchoolHolidayList(null);
        singleTon.setResponse(false);
        ClientSocket clientSocket =ClientSocket.getClientSocket();

        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        String date = holidayDate.getValue().getYear()+"/"+holidayDate.getValue().getMonthValue()+"/"+nextId.substring(8, 10);
        AddSchoolHolidayRequest addSchoolHolidayRequest = new AddSchoolHolidayRequest();
        addSchoolHolidayRequest.setHash(clientSocket.getHash());
        addSchoolHolidayRequest.setMessageType("AddHolidayrequest");
        addSchoolHolidayRequest.setComment(TextAreaComment.getText());
        addSchoolHolidayRequest.setHolidayDate(date);
        queue.add(addSchoolHolidayRequest);
        
        while(!singleTon.isResponse()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(singleTon.isInturrept()){
            textErrorInfo.setFill(new Color(1, 0, 0, 1));
            textErrorInfo.setText("ERROR");
            textMessage.setText("A lesson is already scheduled for this date "+date+" for  "+singleTon.getSubject()+": "+singleTon.getChapter()+": "+singleTon.getLesson()+": "+singleTon.getTopic()+". Please update user lesson planning first and try again.");
            ERRORHbox.setScaleX(1);
            ERRORHbox.setScaleY(1);
        }else{
            hboxAddHoliday.setScaleX(0);
            hboxAddHoliday.setScaleY(0);
            paneAddHoliday.setScaleX(0);
            paneAddHoliday.setScaleY(0);

            ItemHoliday item = new ItemHoliday();
            item.setId(singleTon.getHolidayId());
            item.date.setValue(nextId);
            item.comment.setValue(TextAreaComment.getText());
            data.add(item);
            singleTon.setHolidayId(null);
        }
    }


    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void update(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setUpdateResponse(false);
        
        String nextId = holidayDate.getValue().toString();
        String date = holidayDate.getValue().getYear()+"/"+holidayDate.getValue().getMonthValue()+"/"+nextId.substring(8, 10);
        
        singleTon.setResponse(false);
        ClientSocket clientSocket =ClientSocket.getClientSocket();
        Queue<MessageRequest> queue = clientSocket.getRequestQueue();
        UpdateSchoolHolidayRequest updateSchoolHolidayRequest = new UpdateSchoolHolidayRequest();
        updateSchoolHolidayRequest.setHash(clientSocket.getHash());
        updateSchoolHolidayRequest.setMessageType("UpdateSchoolHolidayRequest");
        updateSchoolHolidayRequest.setComment(TextAreaComment.getText());
        updateSchoolHolidayRequest.setHolidayDate(date);
        updateSchoolHolidayRequest.setId(updateId);

        queue.add(updateSchoolHolidayRequest);

        singleTon.setSchoolHolidayList(null);
        while(!singleTon.isResponse()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyLeavesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(singleTon.isInturrept()){
            textErrorInfo.setFill(new Color(1, 0, 0, 1));
            textErrorInfo.setText("ERROR ");
            textMessage.setText("A lesson is already scheduled for this date "+date+" for  "+singleTon.getSubject()+": "+singleTon.getChapter()+": "+singleTon.getLesson()+": "+singleTon.getTopic()+". Please update user lesson planning first and try again.");
            ERRORHbox.setScaleX(1);
            ERRORHbox.setScaleY(1);
        }else{
            hboxAddHoliday.setScaleX(0);
            hboxAddHoliday.setScaleY(0);
            paneAddHoliday.setScaleX(0);
            paneAddHoliday.setScaleY(0);

            ItemHoliday itemHoliday = new ItemHoliday();
            itemHoliday.setId(updateId);
            itemHoliday.date.setValue(date);
            itemHoliday.comment.setValue(TextAreaComment.getText());
            data.remove(updateItem);
            data.add(itemHoliday);
            singleTon.setUpdateResponse(false);
            updateId = null;
        }
    }

    @FXML
    private void closePaneAddLeave(ActionEvent event) {
        hboxAddHoliday.setScaleX(0);
        hboxAddHoliday.setScaleY(0);
        paneAddHoliday.setScaleX(0);
        paneAddHoliday.setScaleY(0);
    }

    @FXML
    private void closeErrorAction(ActionEvent event) {
        ERRORHbox.setScaleX(0);
        ERRORHbox.setScaleY(0);
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.nextScreen(stage, "LessonPlanning.fxml");
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    private static class DateAllign extends TableCell<ItemHoliday, Boolean> {

        public DateAllign() {
            setAlignment(Pos.CENTER);
        }
    }
    public class CreateButtonUpdate extends TableCell<ItemHoliday, Boolean> {
        final Button updateButton = new Button("Update");
        
        public CreateButtonUpdate(){
            System.out.println(CreateButtonUpdate.this.getIndex());
            updateButton.setCursor(Cursor.HAND);
            updateButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    hboxAddHoliday.setScaleX(1);
                    hboxAddHoliday.setScaleY(1);
                    paneAddHoliday.setScaleX(1);
                    paneAddHoliday.setScaleY(1);
                    btnUpdate.setScaleX(1);
                    btnUpdate.setScaleY(1);
                    btnSubmit.setScaleX(0);
                    btnSubmit.setScaleY(0);
                    labelAddHoliday.setText("Update Holiday date below");

                    updateItem = (ItemHoliday) CreateButtonUpdate.this.getTableView().getItems().get(CreateButtonUpdate.this.getIndex());
                    updateId = updateItem.getId();
                    TextAreaComment.setText(updateItem.getComment());
                    System.out.println("Update ID : "+updateId);
                }
            });
        }
 
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(updateButton);
                updateButton.setStyle("-fx-background-color:#008cba;-fx-text-fill: white;");
//                updateButton.setGraphic(new ImageView("file:///C:/Users/user/Downloads/check_ffffff_16.png"));
            }
            else{
                setGraphic(null);
            }
        }
    }

    public class CreateButtonDelete extends TableCell<ItemHoliday, Boolean> {
        final Button deleteButton = new Button("Delete");
        
        public CreateButtonDelete(){
            System.out.println(CreateButtonDelete.this.getIndex());
            deleteButton.setCursor(Cursor.HAND);
            deleteButton.setOnAction(new EventHandler<ActionEvent>(){
 
                @Override
                public void handle(ActionEvent t) {
                    ItemHoliday item = (ItemHoliday) CreateButtonDelete.this.getTableView().getItems().get(CreateButtonDelete.this.getIndex());
                    System.out.println("ID : "+item.getId());
                    ClientSocket clientSocket =ClientSocket.getClientSocket();
                    Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                    DeleteSchoolHolidayRequest deleteSchoolHolidayRequest = new DeleteSchoolHolidayRequest();
                    deleteSchoolHolidayRequest.setHash(clientSocket.getHash());
                    deleteSchoolHolidayRequest.setMessageType("DeleteschoolHolidayRequest");
                    deleteSchoolHolidayRequest.setId(item.getId());
                    
                    queue.add(deleteSchoolHolidayRequest);
                    data.remove(item);
                    singleTon.setSchoolHolidayList(null);
                    textErrorInfo.setFill(new Color(0.3216, 0.502, 0.2039, 1));
                    textErrorInfo.setText("INFO:");
                    String date = item.date.getValue();
                    date = date.substring(8, 10) + "/" + date.substring(5, 7) + "/" + date.substring(0, 4);
                    textMessage.setText(" All users can plan their classes now on "+date+"  since you specified that school is not closed on that day.");
                    ERRORHbox.setScaleX(1);
                    ERRORHbox.setScaleY(1);
                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(deleteButton);
                deleteButton.setStyle("-fx-background-color:#f04124;-fx-text-fill: white;");
//                deleteButton.setGraphic(new ImageView("file:///C:/Users/user/Downloads/times_ffffff_16.png"));
            }
            else{
                setGraphic(null);
            }
        }
    }    
}
