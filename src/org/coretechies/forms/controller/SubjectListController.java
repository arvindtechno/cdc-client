/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.forms.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.coretechies.Client.ClientSocket;
import org.coretechies.Client.SingleTon;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.SearchRequest;
import org.coretechies.MessageTransporter.SubjectDataRequest;

/**
 * FXML Controller class
 *
 * @author user
 */
public class SubjectListController implements Initializable {
    @FXML
    private GridPane subjectListGrid;

    private SingleTon singleTon;
    
    int i,j,k;
    @FXML
    private TextField txtSearch;
    private Button btnSearch;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnLogout;
    @FXML
    private Button btnHome;
    private PageSwitch pageSwitch;
    @FXML
    private Label labelHome;
    @FXML
    private Label labelBack;
    @FXML
    private Label labelLogout;
    @FXML
    private Button search;
    @FXML
    private Label labelWelcomeL;
    @FXML
    private ImageView iviewHeader;
    @FXML
    private Label labelWelcomeR;
    @FXML
    private Label labelSubject;
    @FXML
    private ImageView iviewIcon;
    @FXML
    private Text userName;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pageSwitch = new PageSwitch();
        singleTon = SingleTon.getSingleTon();
        userName.setText(singleTon.getLastName()+", "+singleTon.getFirstName());
        while(singleTon.getSubjectList()==null){
            try {
                System.out.println("YAHOO");
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(SubjectListController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        int size = singleTon.getSubjectList().size();
//        for(int i=0;i<size;i++){
//            selectUnit.getItems().add(singleTon.getSubjectList().get(i));
//        }
        for(i=j=k=0; k<size; i++, k++){
            Pane pane = new Pane();
            Pane pane1 = new Pane();
            pane.setStyle("-fx-background-color: rgba(0, 100, 100, 0);");
            pane1.setStyle("-fx-background-color:black;-fx-border-radius:0 0 13 13;-fx-background-radius:0 0 13 13;-fx-border-width:1;-fx-border-color:#3ab7cf;-fx-baxkground-radius:0 0 13 13;");
//            ImageView imageViewSI = new ImageView(new Image("file:///C:/Users/Tiwari/Desktop/UPDATED/New%20folder/CDCClient/src/org/coretechies/forms/images/icon_BiologyLrg.png"));
            ImageView imageViewSI = new ImageView(new Image(getClass().getResourceAsStream("icon_BiologyLrg.png")));
//            ImageView imageViewPH =new ImageView(new Image("/src/org/coretechies/forms/images/button/panehead.png"));
            ImageView imageViewPH =new ImageView(new Image(getClass().getResourceAsStream("panehead.png")));
//            imageViewSI.setStyle("-fx-image:url('../images/icon_BiologyLrg.png');");
//            imageViewPH.setStyle("-fx-image:url('@../images/button/panehead.png');");
            Button buttonEnter =new Button("Enter");
            buttonEnter.setCursor(Cursor.HAND);

            buttonEnter.setStyle("-fx-background-color: orange;");
            buttonEnter.setPrefSize(64, 31);
            buttonEnter.setId(singleTon.getSubjectList().get(k));
            buttonEnter.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    ClientSocket clientSocket =ClientSocket.getClientSocket();
            
                    Queue<MessageRequest> queue = clientSocket.getRequestQueue();
                    SubjectDataRequest subjectDataRequest = new SubjectDataRequest();
                    
                    subjectDataRequest.setHash(clientSocket.getHash());
                    subjectDataRequest.setSubjectName(buttonEnter.getId());
                    subjectDataRequest.setMessageType("UnitData");
                    queue.add(subjectDataRequest);

                    Stage stage = (Stage) buttonEnter.getScene().getWindow();
                    pageSwitch.nextScreen(stage, "Subjectinit.fxml");
                }
            });
       
            
            
//            Label subjectName = new Label("Google");
            Label subjectName = new Label(singleTon.getSubjectList().get(k));
            subjectName.setStyle("-fx-text-fill: #3ab7cf; -fx-font-weight: bold; -fx-font-size: 18;");
            pane.getChildren().add(pane1);
            pane.getChildren().add(imageViewPH);
            pane.getChildren().add(imageViewSI);
            pane.getChildren().add(buttonEnter);
            pane.getChildren().add(subjectName);
            pane1.setPrefSize(290, 97);
            pane1.setLayoutX(1);
            pane1.setLayoutY(58);
            imageViewSI.setLayoutX(110);
            imageViewSI.setLayoutY(20);

            imageViewSI.setFitWidth(175);
            imageViewSI.setFitHeight(168);

            imageViewPH.setLayoutX(0);
            imageViewPH.setLayoutY(0);
            imageViewPH.setFitHeight(60);
            imageViewPH.setFitWidth(292);

            buttonEnter.setLayoutX(10);
            buttonEnter.setLayoutY(80);

            subjectName.setLayoutX(10);
            subjectName.setLayoutY(25);
            if(i==3){
                i=0;
                j++;
                subjectListGrid.add(pane, i, j);

            }
            else{
            subjectListGrid.add(pane, i, j);
            }
        }       
    }    
    
   public void subjectListElement(){
      Pane pane = new Pane();
        Pane pane1 = new Pane();
        pane.setStyle("-fx-background-color: rgba(0, 100, 100, 0);");
       pane1.setStyle("-fx-background-color:black;-fx-border-radius:0 0 13 13;-fx-background-radius:0 0 13 13;-fx-border-width:1;-fx-border-color:#3ab7cf;-fx-baxkground-radius:0 0 13 13;");
       ImageView imageViewSI = new ImageView(new Image("file:///C:/Users/user/Desktop/updatingCDC/15oct%205pm%20all%20set%20problem%20with%20other%20function%20due%20to%20stack%20pane/CDCClient/src/org/coretechies/forms/images/icon_BiologyLrg.png"));
       ImageView imageViewPH =new ImageView(new Image("file:///C:/Users/user/Desktop/updatingCDC/15oct%205pm%20all%20set%20problem%20with%20other%20function%20due%20to%20stack%20pane/CDCClient/src/org/coretechies/forms/images/button/panehead.png"));

       Button buttonEnter =new Button("Enter");
       
       buttonEnter.setStyle("-fx-background-color: orange;");
       buttonEnter.setPrefSize(64, 31);
       Label subjectName = new Label("Google");
       subjectName.setStyle("-fx-text-fill: #3ab7cf; -fx-font-weight: bold; -fx-font-size: 18;");
       pane.getChildren().add(pane1);
       pane.getChildren().add(imageViewPH);
       pane.getChildren().add(imageViewSI);
       pane.getChildren().add(buttonEnter);
       pane.getChildren().add(subjectName);
       pane1.setPrefSize(290, 97);
       pane1.setLayoutX(0);
       pane1.setLayoutY(60);
       imageViewSI.setLayoutX(110);
       imageViewSI.setLayoutY(20);
       
       imageViewSI.setFitWidth(175);
       imageViewSI.setFitHeight(168);
       
       imageViewPH.setLayoutX(0);
       imageViewPH.setLayoutY(0);
       imageViewPH.setFitHeight(60);
       imageViewPH.setFitWidth(290);
       
       buttonEnter.setLayoutX(10);
       buttonEnter.setLayoutY(80);
       
       subjectName.setLayoutX(10);
       subjectName.setLayoutY(25);
       subjectListGrid.add(pane, 0, 0);
       
       
        
   }

    private void searchAction(ActionEvent event) {
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            System.out.println("<<<<<<<<<<<<<<<<");
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Stage stage = (Stage) btnSearch.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    @FXML
    private void back(ActionEvent event) {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage stage = (Stage) btnLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }

    @FXML
    private void home(ActionEvent event) {
        Stage stage = (Stage) btnHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void searchContent(ActionEvent event) {
        
        singleTon = SingleTon.getSingleTon();
        singleTon.setLessonPlanResponse(false);
        
        if(txtSearch.getText().length()!=0){
            singleTon.setSearchText(txtSearch.getText());
            ClientSocket clientSocket =ClientSocket.getClientSocket();
            Queue<MessageRequest> queue = clientSocket.getRequestQueue();
            System.out.println("<<<<<<<<<<<<<<<<");
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.setHash(clientSocket.getHash());
            searchRequest.setKeyword(txtSearch.getText());
            searchRequest.setMessageType("SearchDataRequest");
            queue.add(searchRequest);

            while(singleTon.isLessonPlanResponse()==false){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CurriculumAndGradeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Stage stage = (Stage) search.getScene().getWindow();
            pageSwitch.nextScreen(stage, "Search.fxml");
        }
    }

    @FXML
    private void LabelHomeAction(MouseEvent event) {
        Stage stage = (Stage) labelHome.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void labelBackAction(MouseEvent event) {
        Stage stage = (Stage) labelBack.getScene().getWindow();
        pageSwitch.home(stage);
    }

    @FXML
    private void LabelLogoutAction(MouseEvent event) {
        Stage stage = (Stage) labelLogout.getScene().getWindow();
        pageSwitch.logout(stage);
    }
}
