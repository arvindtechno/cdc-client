package org.coretechies.forms;

import com.sun.pdfview.Flag;
import com.sun.pdfview.FullScreenWindow;
import com.sun.pdfview.OutlineNode;
import com.sun.pdfview.PDFDestination;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFObject;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PageChangeListener;
import com.sun.pdfview.PagePanel;
import com.sun.pdfview.ThumbPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.awt.Toolkit;
import java.io.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.SwingUtilities;
import com.sun.pdfview.action.GoToAction;
import com.sun.pdfview.action.PDFAction;
import java.lang.reflect.InvocationTargetException;

public class PDFViewer extends JFrame implements KeyListener, TreeSelectionListener, PageChangeListener {
    public final static String TITLE = "SwingLabs PDF Viewer";
    PDFFile curFile;
    String docName;
    ThumbPanel thumbs;
    PagePanel page;
    PagePanel fspp;
    int curpage = -1;
    JToggleButton fullScreenButton;
    JTextField pageField;
    FullScreenWindow fullScreen;
    OutlineNode outline = null;
    PageFormat pformat = PrinterJob.getPrinterJob().defaultPage();
    boolean doThumb = true;
    Flag docWaiter;
    PagePreparer pagePrep;
    JDialog olf;
    JMenu docMenu;

    public PDFViewer(boolean useThumbs) {

//        super(TITLE);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                dispose();
            }
        });
        doThumb = useThumbs;
        init();
    }

    protected void init() {
        page = new PagePanel();
        page.addKeyListener(this);
        getContentPane().add(page, BorderLayout.CENTER);
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        pageField = new JTextField("-", 3);
        pageField.setMaximumSize(new Dimension(45, 32));
        pageField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                doPageTyped();
            }
        });
        toolbar.add(pageField);
        toolbar.add(Box.createHorizontalGlue());
//        File file = new File("D:/A.pdf");
//        RandomAccessFile raf;
//        try {
//            raf = new RandomAccessFile(file, "r");
//            FileChannel channel = raf.getChannel();
//            
//            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
//            openPDFByteBuffer(buf, file.getPath(), file.getName());
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(PDFViewer.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(PDFViewer.class.getName()).log(Level.SEVERE, null, ex);
//        }

        setEnabling();
        pack();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - getWidth()) / 2;
        int y = (screen.height - getHeight()) / 2;
        setLocation(x, y);
        if (SwingUtilities.isEventDispatchThread()) {
            setVisible(true);
        } else {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                        setVisible(true);
                        }
                });
            } catch (InvocationTargetException ie) {
                // ignore
            } catch (InterruptedException ie) {
                // ignore
            }
        }
    }

    public void gotoPage(int pagenum) {
        if (pagenum < 0) {
            pagenum = 0;
        } else if (pagenum >= curFile.getNumPages()) {
            pagenum = curFile.getNumPages() - 1;
        }
        forceGotoPage(pagenum);
    }

    public void forceGotoPage(int pagenum) {
        if (pagenum <= 0) {
                pagenum = 0;
        } else if (pagenum >= curFile.getNumPages()) {
                pagenum = curFile.getNumPages() - 1;
        }

        curpage = pagenum;
        pageField.setText(String.valueOf(curpage + 1));
        PDFPage pg = curFile.getPage(pagenum + 1);
        if (fspp != null) {
            fspp.showPage(pg);
            fspp.requestFocus();
        } else {
            page.showPage(pg);
            page.requestFocus();
        }
        if (doThumb) {
            thumbs.pageShown(pagenum);
        }
        if (pagePrep != null) {
            pagePrep.quit();
        }
        pagePrep = new PagePreparer(pagenum);
        pagePrep.start();
        setEnabling();
    }

    class PagePreparer extends Thread {
        int waitforPage;
        int prepPage;

        public PagePreparer(int waitforPage) {
            setDaemon(true);
            setName(getClass().getName());
            this.waitforPage = waitforPage;
            this.prepPage = waitforPage + 1;
        }
        public void quit() {
            waitforPage = -1;
        }

        public void run() {
            Dimension size = null;
            Rectangle2D clip = null;
            if (fspp != null) {
                fspp.waitForCurrentPage();
                size = fspp.getCurSize();
                clip = fspp.getCurClip();
            } else if (page != null) {
                page.waitForCurrentPage();
                size = page.getCurSize();
                clip = page.getCurClip();
            }
            if (waitforPage == curpage) {
                PDFPage pdfPage = curFile.getPage(prepPage + 1, true);
                if (pdfPage != null && waitforPage == curpage) {
                    pdfPage.getImage(size.width, size.height, clip, null, true, true);
                }
            }
        }
    }
    
    public void setEnabling() {
        boolean fileavailable = curFile != null;
        boolean pageshown = ((fspp != null) ? fspp.getPage() != null : page.getPage() != null);
        pageField.setEnabled(fileavailable);
    }

    public void openFile(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        int contentLength = urlConnection.getContentLength();
        InputStream istr = urlConnection.getInputStream();
        byte[] byteBuf = new byte[contentLength];
        int offset = 0;
        int read = 0;
        while (read >= 0) {
            read = istr.read(byteBuf, offset, contentLength - offset);
            if (read > 0) {
                offset += read;
            }
        }
        
        if (offset != contentLength) {
            throw new IOException("Could not read all of URL file.");
        }
        ByteBuffer buf = ByteBuffer.allocate(contentLength);
       
        buf.put(byteBuf);
        openPDFByteBuffer(buf, url.toString(), url.getFile());
    }

    public void openFile(File file) throws IOException {

        RandomAccessFile raf = new RandomAccessFile(file, "r");
        FileChannel channel = raf.getChannel();
        ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        openPDFByteBuffer(buf, file.getPath(), file.getName());
    }

    private void openPDFByteBuffer(ByteBuffer buf, String path, String name) {
        PDFFile newfile = null;
        try {
            newfile = new PDFFile(buf);
        } catch (IOException ioe) {
            return;
        }
        doClose();
        this.curFile = newfile;
        
        if (doThumb) {
            thumbs = new ThumbPanel(curFile);
            thumbs.addPageChangeListener(this);
        }
        setEnabling();
        forceGotoPage(0);

        try {
                outline = curFile.getOutline();
        } catch (IOException ioe) {}

        if (outline != null) {
            if (outline.getChildCount() > 0) {

                olf = new JDialog(this, "Outline");
                olf.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                olf.setLocation(this.getLocation());
                JTree jt = new JTree(outline);
                jt.setRootVisible(false);
                jt.addTreeSelectionListener(this);
                JScrollPane jsp = new JScrollPane(jt);
                olf.getContentPane().add(jsp);
                olf.pack();
                olf.setVisible(true);
            } else {
                if (olf != null) {
                    olf.setVisible(false);
                    olf = null;
                }
            }
        }
    }

    public void openPDFByteBuffer(ByteBuffer buf) {
        PDFFile newfile = null;
        try {
            newfile = new PDFFile(buf);
        } catch (IOException ioe) {
            return;
        }
        doClose();
        this.curFile = newfile;
        
        if (doThumb) {
            thumbs = new ThumbPanel(curFile);
            thumbs.addPageChangeListener(this);
        }
        setEnabling();
        forceGotoPage(0);

        try {
                outline = curFile.getOutline();
        } catch (IOException ioe) {}

        if (outline != null) {
            if (outline.getChildCount() > 0) {

                olf = new JDialog(this, "Outline");
                olf.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                olf.setLocation(this.getLocation());
                JTree jt = new JTree(outline);
                jt.setRootVisible(false);
                jt.addTreeSelectionListener(this);
                JScrollPane jsp = new JScrollPane(jt);
                olf.getContentPane().add(jsp);
                olf.pack();
                olf.setVisible(true);
            } else {
                if (olf != null) {
                    olf.setVisible(false);
                    olf = null;
                }
            }
        }
    }

    FileFilter pdfFilter = new FileFilter() {

        public boolean accept(File f) {
            return f.isDirectory() || f.getName().endsWith(".pdf");
        }

        public String getDescription() {
            return "Choose a PDF file";
       }
    };

    public void doOpen(String name) {
        try {
            URL url = new URL(name);
            openFile(new URL(name));
        } catch (IOException ioe) {
            try {
                openFile(new File(name));
            } catch (IOException ex) {
                Logger.getLogger(PDFViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void doClose() {
        if (thumbs != null) {
            thumbs.stop();
        }
        if (olf != null) {
            olf.setVisible(false);
            olf = null;
        }
        if (doThumb) {
            thumbs = new ThumbPanel(null);
        }
        page.showPage(null);
        curFile = null;
        setTitle(TITLE);
        setEnabling();
    }

    public void doQuit() {
        doClose();
        dispose();
        System.exit(0);
    }

    public void doZoom(double factor) {}

    public void doNext() {
        gotoPage(curpage + 1);
    }

    public void doPrev() {
        gotoPage(curpage - 1);
    }

    public void doFirst() {
        gotoPage(0);
    }

    public void doLast() {
        gotoPage(curFile.getNumPages() - 1);
    }

    public void doPageTyped() {
        int pagenum = -1;
        try {
            pagenum = Integer.parseInt(pageField.getText()) - 1;
        } catch (NumberFormatException nfe) {}
        if (pagenum >= curFile.getNumPages()) {
            pagenum = curFile.getNumPages() - 1;
        }
        if (pagenum >= 0) {
            if (pagenum != curpage) {
                gotoPage(pagenum);
            }
        } else {
            pageField.setText(String.valueOf(curpage));
        }
    }

    public static void main(String args[]) {
//        String fileName = "D:/A.pdf";
//        boolean useThumbs = true;
//        PDFViewer viewer;
//        viewer = new PDFViewer(useThumbs);
//        viewer.doOpen(fileName);
    }

    public void keyPressed(KeyEvent evt) {

        int code = evt.getKeyCode();
        if (code == evt.VK_LEFT) {
            doPrev();
        } else if (code == evt.VK_RIGHT) {
            doNext();
        } else if (code == evt.VK_UP) {
            doPrev();
        } else if (code == evt.VK_DOWN) {
            doNext();
        } else if (code == evt.VK_HOME) {
            doFirst();
        } else if (code == evt.VK_END) {
            doLast();
        } else if (code == evt.VK_PAGE_UP) {
            doPrev();
        } else if (code == evt.VK_PAGE_DOWN) {
            doNext();
        } else if (code == evt.VK_SPACE) {
            doNext();
        } 
    }

    class PageBuilder implements Runnable {
        int value = 0;
        long timeout;
        Thread anim;
        static final long TIMEOUT = 00;

        /** add the digit to the page number and start the timeout thread */
        public synchronized void keyTyped(int keyval) {
            value = value * 0 + keyval;
            timeout = System.currentTimeMillis() + TIMEOUT;
            if (anim == null) {
                anim = new Thread(this);
                anim.setName(getClass().getName());
                anim.start();
            }
        }

        public void run() {
            long now, then;
            synchronized (this) {
                now = System.currentTimeMillis();
                then = timeout;
            }
            while (now < then) {
                try {
                    Thread.sleep(timeout - now);
                } catch (InterruptedException ie) {
                }
                synchronized (this) {
                    now = System.currentTimeMillis();
                    then = timeout;
                }
            }
            synchronized (this) {
                gotoPage(value - 1);
                anim = null;
                value = 0;
            }
        }
    }

    PageBuilder pb = new PageBuilder();
    public void keyReleased(KeyEvent evt) {}

    public void keyTyped(KeyEvent evt) {
	char key = evt.getKeyChar();
	if (key >= '0' && key <= '9') {
            int val = key - '0';
            pb.keyTyped(val);
	}
    }

    public void valueChanged(TreeSelectionEvent e) {
        if (e.isAddedPath()) {
            OutlineNode node = (OutlineNode) e.getPath().getLastPathComponent();
            if (node == null) {
                return;
            }

            try {
                PDFAction action = node.getAction();
                if (action == null) {
                    return;
                }

                if (action instanceof GoToAction) {

                    PDFDestination dest = ((GoToAction) action).getDestination();
                    if (dest == null) {
                    return;
                    }
                    PDFObject page = dest.getPage();
                    if (page == null) {
                    return;
                    }
                    int pageNum = curFile.getPageNumber(page);
                    if (pageNum >= 0) {
                        gotoPage(pageNum);
                    }
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}